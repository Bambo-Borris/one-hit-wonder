# One Hit Wonder

## Premise
Game where a brave bowman goes to do battle against a set of evil overlords of darkenss. Take one hit, and you'll pass away!

## Tech
SFML

CMake

C++

Volcanic Toolkit (vtk)

(Thanks to Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de) for Animation/AnimatedSprite)