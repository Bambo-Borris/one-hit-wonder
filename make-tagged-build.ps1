#Requires -RunAsAdministrator

####################################################
#                 Make Tagged Build                #
####################################################
# This script is used to generate a test or release 
# build, tag it in git and then push the tag to the
# remote. Once this is done this script will then
# generate a release on Gitlab. The build can then 
# be uploaded.

# Verify we got enough arguments
if ($args.Length -lt 3) {
    Write-Output("Expected format: ./make-tagged-build <version> <type> <access_token>")
    Write-Output("<version>: major.minor.patch")
    Write-Output("<build_type>: test or release")
    Write-Output("<access_token>: Gitlab access token")
    Exit
}

# Using regex let's verify that the version number is SemVer
$matchResult = $args[0] -match "^((([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)$"
if (!$matchResult) {
    Write-Error("Invalid version number passed, please use SemVer form major.minor.patch")
    Exit
}

$versionNumber = $Matches.0

# Verify a tag of that value doesn't already exist
$mostRecentTag = git describe --tags --abbrev=0 

if ($mostRecentTag -eq $versionNumber) { 
    Write-Error("Version number exists already!")
    Exit
}

$mode = $args[1]
$folderName = "one_hit_wonder_$($versionNumber)"

$configType = ""
# Verify that the 'mode' variable (passed as an argument) is correct
# and set our config type accordingly
if ($mode.ToString().ToLower() -eq "release") {
    $configType = "Release"
}
elseif ($mode.ToString() -eq "test") {
    $configType = "RelWithDebInfo"
    Write-Output($configType)
}
else {
    Write-Error("Invalid build type supplied, options are: release, test")
    Exit
}

$accessToken = $args[2]

# Remove all previous .zip builds (local cleanup)
Remove-Item *.zip | Out-Null

# Generate the project files, if we fail we terminate
if ($mode.ToString().ToLower() -eq "release") { 
    cmake -B onehitwonder-build -G "Ninja Multi-Config" "-DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=$($configType) -DSYMLINK_PATHS=OFF"
} else {
    cmake -B onehitwonder-build -G `"Ninja Multi-Config`" "-DBUILD_SHARED_LIBS=OFF -DCMAKE_BUILD_TYPE=$($configType) -DSYMLINK_PATHS=OFF -DSKIP_RUBBISH=ON"
}

if (!$?) {
    Write-Error("Unable to configure project, exiting")
    Exit
}

# We'll setup our staging folder
$folderName = "one_hit_wonder_$($versionNumber)"
mkdir "onehitwonder-build/$($folderName)" -ErrorAction Stop

# Let's try compile, if we fail we'll exit
cmake --build onehitwonder-build --config "$($configType)"
if (!$?) {
    Write-Error("Unable to build, exiting")
    Exit
}

Copy-Item "onehitwonder-build/$($configType)/one-hit-wonder.exe" "./onehitwonder-build/$($folderName)/." -ErrorAction Stop
Copy-Item openal32.dll "./onehitwonder-build/$($folderName)" -ErrorAction Stop
Copy-Item ./copyright_info "./onehitwonder-build/$($folderName)/." -r -ErrorAction Stop

# We only generate these on test builds
if ($mode -eq "test") {
    Copy-Item "onehitwonder-build/$($configType)/one-hit-wonder.pdb" "./onehitwonder-build/$($folderName)/." -ErrorAction Stop
    Copy-Item "onehitwonder-build/nce/nce.dir/$($configType)/nce.pdb" "./onehitwonder-build/$($folderName)/." -ErrorAction Stop
}

Copy-Item bin/ "./onehitwonder-build/$($folderName)" -Recurse -Force -ErrorAction Stop
$outputPath = "./$($folderName).zip"

Compress-Archive -Path "./onehitwonder-build/$($folderName)" -DestinationPath $outputPath -ErrorAction Stop
Remove-Item -Recurse -Force ./onehitwonder-build/ -ErrorAction Stop

# All input has been validated, let's make the tag and push it to the remote 
git tag $versionNumber

if (!$?) { 
    Write-Error("Unable to create git tag")
    Exit
}

git push -u origin $versionNumber
if (!$?) { 
    Write-Error("Unable to push git tag to remote")
    Exit
}

$releaseName = "One Hit Wonder v$($versionNumber)"
$json = @"
{
    "name": "One Hit Wonder - v$($versionNumber)",
    "tag_name": "$versionNumber",
    "id": 39077700
}
"@
$json > out.json

curl.exe --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $($accessToken)" `
    -d "@out.json"`
    --request POST "https://gitlab.com/api/v4/projects/39077700/releases"

Remove-Item out.json -Force

