#pragma once

#include "../Objects/RadialProjectile.hpp"
#include <nce/TweeningAnimation.hpp>

// Needs a pointer to appstate, passed in constructor by boss
// This is so it can use add and remove GameObject

class RadialAbility {
public:
    RadialAbility(nce::AppState* currentState, std::string_view name, int numProj, const sf::Vector2f& pos, float rad);
    ~RadialAbility();

    void update(const sf::Time& dt);

    void lerpToRadius(const float& rad);
    void lerpToSpeed(const float& spd);

    void setPosition(const sf::Vector2f& pos) { m_position = pos; }

private:
    enum class Phase : uint32_t { Gathering, Moving, Attacking, Done };
    Phase m_phase { Phase::Gathering };

    // Returns offset vector from the centre of the ability
    sf::Vector2f getCMVector(const float& radius, const sf::Angle& angle);

    void updateGathering();
    void updateMoving();
    void updateAttacking();

    nce::AppState* const m_state;
    std::vector<RadialProjectile*> m_projectiles;

    sf::Vector2f m_position { 0.f, 0.f };
    sf::Vector2f m_gatherPosition { 0.f, 0.f };
    sf::Vector2f m_movementNormal { 0.f, 0.f };
    float m_speed { 0.f };
    float m_radius { 0.f };
    float m_numProjectiles { 0.f };

    std::optional<nce::TAFloat> m_angleTween;
    std::optional<nce::TAFloat> m_speedTween;
};

/*
    - Initial Test -
    1) Add angle spacing to projectiles (save as var)
    2) Set projectiles at correct position
    3) Rotate projectiles to angle + 90 degrees
    4) Start Tweening Animation
    5) Repeat: Get angle from tween + offset, then steps 2 and 3 (clamp angle if necessary)

    // Need phase management and multiple minor functions to operate each phase
    // 1) Gathering projectiles
    // 2) Moving into position
    // 3) Wheel tears across the screen at a fast pace, just slow enough to double dodge through it

    - Gathering Projectiles Phase -
    1) Set angular speed to ensure gap is a multiple of 1/60th of a second (for smooth transition)
    2) Fire projectiles at circle start position (1 per X frames depending on gap)
    2) When projectile is at the start position, activate the inCircle bool

    - Projectiles In Circle Loop -
    1) Get angle from tween + offset
    2) Set projectiles at correct position using angle and radius
    3) Rotate projectiles to angle + 90 degrees

    - Moving Into Position Phase -
    Description: Tweened motion to bottom right of the play area

    - Attack Phase -
    Description: Wheel moves from right to left to right to left and off screen,
    tweened speed at the edge of the screen to look like it's rubber banding back
*/
