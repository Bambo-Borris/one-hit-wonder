#pragma once

#include "RadialAbility.hpp"
#include "../GameplayVars.hpp"
#include <nce/AppState.hpp>
#include <nce/MathsUtils.hpp>

RadialAbility::RadialAbility(nce::AppState* currentState, std::string_view name, int numProj, const sf::Vector2f& pos, float rad)
    : m_state(currentState)
    , m_numProjectiles(float(numProj))
    , m_position(pos)
    , m_radius(rad)
{
    m_projectiles.resize(numProj);

    for (auto& proj : m_projectiles) {
        proj = new RadialProjectile(currentState, sf::Vector2f { 0.f, 0.f });
        if (!currentState->addGameObject(proj)) {
            assert(false);
            throw std::runtime_error("Cannot add radial projctile game object");
        }
    }

    // Temporarily spawning in projectiles where they need to be
    auto counter = 0;
    auto spacing = 2.f * nce::PI / m_numProjectiles;
    for (auto& proj : m_projectiles) {
        auto angle = sf::radians(counter * spacing);
        proj->setAngle(angle);
        proj->setAngleOffset(angle);
        proj->setActive(true);
        proj->setInCirlce(true);
        counter++;
    }
    auto timerController = &currentState->getTimerController();
    m_angleTween.emplace(nce::AnimationCurve::Lerp, name, 0.f, nce::PI, sf::seconds(getBossesUnnamedFloatVar("ra_gather_tween_time")), timerController);
    m_angleTween->play();
}

RadialAbility::~RadialAbility()
{
    for (auto& proj : m_projectiles)
        m_state->removeGameObject(proj);
}

void RadialAbility::update(const sf::Time& dt)
{
    bool tweenReset = false;
    if (m_angleTween->isComplete()) {
        m_angleTween->stop();
        m_angleTween->setStart(m_angleTween->getEnd() * dt.asSeconds() / getBossesUnnamedFloatVar("ra_gather_tween_time"));
        m_angleTween->play();
        tweenReset = true;
    }

    for (auto& proj : m_projectiles) {
        if (proj->getInCirlce()) {
            if (tweenReset) {
                auto offset = proj->getAngleOffset();
                if (offset.asRadians() + nce::PI > 2.f * nce::PI)
                    proj->setAngleOffset(offset - sf::radians(nce::PI));
                else
                    proj->setAngleOffset(offset + sf::radians(nce::PI));
            }

            proj->setAngle(proj->getAngleOffset() + sf::radians(m_angleTween->getTweenedValue()));
            proj->setPosition(m_position + getCMVector(m_radius, proj->getAngle()));
            proj->setRotation(proj->getAngle() + sf::radians(nce::PI * 0.5f));
        }
    }

    switch (m_phase) {
    case Phase::Gathering:
        updateGathering();
        break;
    case Phase::Moving:
        updateMoving();
        break;
    case Phase::Attacking:
        updateAttacking();
        break;
    case Phase::Done:
        // reset and deactivate
        break;
    default:
        assert(false);
        std::runtime_error("RadialAbility update() enum error!");
        break;
    }
}

void RadialAbility::lerpToRadius(const float& rad) { NCE_UNUSED(rad); }

void RadialAbility::lerpToSpeed(const float& spd) { NCE_UNUSED(spd); }

sf::Vector2f RadialAbility::getCMVector(const float& radius, const sf::Angle& angle)
{
    return { radius * std::cos(angle.asRadians()), radius * std::sin(angle.asRadians()) };
}

void RadialAbility::updateGathering()
{

    //    - Gathering Projectiles Phase -
    //    1) Set angular speed to ensure gap is a multiple of 1/60th of a second (for smooth transition)
    //    2) Fire projectiles at circle start position (1 per X frames depending on gap)
    //    2) When projectile is at the start position, activate the inCircle bool
}

void RadialAbility::updateMoving() { }

void RadialAbility::updateAttacking() { }
