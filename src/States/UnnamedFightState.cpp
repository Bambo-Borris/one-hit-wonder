#include "UnnamedFightState.hpp"
#include "../GameplayConstants.hpp"
#include "../Objects/Unnamed.hpp"
#include "../Utils/GameplayUtility.hpp"

constexpr std::size_t NAIL_PROJECTILE_COUNT { 60 }; // Boss cached projectile

UnnamedFightState::UnnamedFightState(nce::App* app, const sf::Vector2f& playAreaSize)
    : BossStateBase(gc::UNNAMED_STATE_NAME, app, playAreaSize, gc::UNNAMED_INDEX)
    , m_cachedView(app->getRenderTexture()->getView())
{
    auto zoomedView = m_cachedView;
    zoomedView.zoom(gc::ZOOM_OUT_SCALE);
    zoomedView.setCenter({ 512.f, 154.f });
    app->getRenderTexture()->setView(zoomedView);

    const auto& v { app->getRenderTexture()->getView() };
    const sf::FloatRect bounds { v.getCenter() - v.getSize() * 0.5f, v.getSize() };
    const auto initData { GenerateSolverFromViewBounds(bounds) };
    getSolver().reconstructGrid(initData.cellSize, initData.gridSize, initData.position);

    for (size_t i { 0 }; i < NAIL_PROJECTILE_COUNT; ++i) {
        m_bossProjectiles.push_back(new BossProjectile(this));
        m_bossProjectiles.back()->setActive(false);
        m_bossProjectiles.back()->setProjectileType(BossProjectile::ProjectileType::FlameBarrage);
        if (!addGameObject(m_bossProjectiles.back()))
            throw std::runtime_error("Unable to create nail projectile");
    }

    if (!addGameObject(new Unnamed(this, getPlayer(), playAreaSize, m_bossProjectiles)))
        throw std::runtime_error("Unable to create demonic hammer");
    setBossDeathText(fmt::format("{} has been slain!", gc::BOSS_NAMES[gc::UNNAMED_INDEX]));
    addBossHealthBar(getObjectOfType<Unnamed>());

    getSfxManager().playMusic("bin/music/menu_music.mp3");
}

UnnamedFightState::~UnnamedFightState()
{
    getSfxManager().stopMusic("bin/music/menu_music.mp3");
    getApp()->getRenderTexture()->setView(m_cachedView);
}

void UnnamedFightState::update(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    BossStateBase::update(dt);

    if (getGameplayStage() != GameplayStage::PlayerDied) {
        auto player { getPlayer() };
        if (player->wasPlayerHit())
            setGameplayStage(GameplayStage::PlayerDied);
    }

    if (nce::InputHandler::KeyPressed(sf::Keyboard::F))
        if (!m_radAbility) {
            auto pos = getApp()->getRenderTexture()->getView().getCenter();
            pos.x += getApp()->getRenderTexture()->getView().getSize().x * 0.35f;
            pos.y -= getApp()->getRenderTexture()->getView().getSize().y * 0.2f;
            m_radAbility.emplace(this, "wheel", 30, pos, 200.f);
        }

    if (nce::InputHandler::KeyPressed(sf::Keyboard::T)) {
        if (m_radAbility) {
            if (moveWithMouse) {
                auto pos = getApp()->getRenderTexture()->getView().getCenter();
                pos.x += getApp()->getRenderTexture()->getView().getSize().x * 0.35f;
                pos.y -= getApp()->getRenderTexture()->getView().getSize().y * 0.2f;
                m_radAbility->setPosition(pos);
                moveWithMouse = false;
            } else
                moveWithMouse = true;
        }
    }

    if (m_radAbility) {
        m_radAbility->update(dt);

        if (moveWithMouse) {
            auto pos = -getApp()->getRenderTexture()->getView().getSize() * 0.25f;
            m_radAbility->setPosition(GetMouseRTWorldPos(getApp()));
            // m_radAbility->setPosition(sf::Vector2f(getWindow()->mapPixelToCoords(sf::Mouse::getPosition())) + pos);
        }
    }
}

void UnnamedFightState::debugKillBoss() { }

int32_t UnnamedFightState::getBossHealth() const { return getObjectOfType<Unnamed>()->getHealth(); }

void UnnamedFightState::setGameplayStage(GameplayStage stage)
{
    switch (stage) {
    case GameplayStage::FightingBoss: {
        auto boss = getObjectOfType<Unnamed>();
        assert(boss);
        boss->reset();
    } break;
    case GameplayStage::BossDied:
        setNextStateID(gc::MENU_STATE_NAME);
        break;
    case GameplayStage::PlayerDied:
        break;
    default:
        assert(false);
        break;
    }

    BossStateBase::setGameplayStage(stage);
}

void UnnamedFightState::updateDeveloperUI(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    generateGenericDevMenu(NodeIdentifiers::BossesUnnamed, dt);
}
