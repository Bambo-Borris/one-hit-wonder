#include "BossStateBase.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../HUD/BossHealthBar.hpp"
#include "../HUD/DarkenHUDAnimated.hpp"
#include "../HUD/GUIHelpers.hpp"
#include "../HUD/PadAimIndicator.hpp"
#include "../Objects/BossBase.hpp"
#include "../Objects/Floor.hpp"
#include "../Utils/GameMetrics.hpp"
#include "../Utils/GameplayUtility.hpp"

#include <SFML/Window/Cursor.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/InputHandler.hpp>
#include <nce/SoundEventManager.hpp>
#include <nce/TimerController.hpp>

#include <imgui.h>

constexpr std::size_t PROJECTILE_COUNT { 100 }; // Player cached projectile count
constexpr auto HUD_FADE_IN_TIME { sf::seconds(1.f) }; // When the player either dies or kills the boss, how long should it take for the HUD to fade in

constexpr auto HealthBarLayer { 0 }; // the lowest so it can also be darkend when player dies
constexpr auto DarkenHUDLayer { 1 };
constexpr auto NormalHUDLayer { 2 };

bool BossStateBase::m_showDeveloperUI { false };

BossStateBase::BossStateBase(std::string_view identifier, nce::App* app, const sf::Vector2f& playAreaSize, size_t bossIndex)
    : nce::AppState(identifier, app, playAreaSize)
    , m_bossIndex(bossIndex)
    , m_pauseMenu(&getButtonActionChecker())
{
    auto initData = GenerateSolverFromViewBounds(sf::FloatRect { { 0.f, 0.f }, playAreaSize });
    m_solver = nce::CollisionSolver(initData.cellSize, initData.gridSize, initData.position);
    bindControls();

    m_arrowProjectiles.resize(PROJECTILE_COUNT);
    for (auto& p : m_arrowProjectiles) {
        p = new ArrowProjectile(this, sf::Vector2f { getApp()->getRenderTexture()->getSize() });
        assert(p);
        p->setActive(false);
        if (!addGameObject(p))
            throw std::runtime_error("Unable to create arrow projectiles");
    }

    if (!addGameObject(new Floor(this)))
        throw std::runtime_error("Unable to create floor");

    if (!addGameObject(new PlayerCharacter(this, m_arrowProjectiles)))
        throw std::runtime_error("Unable to create player");

    setupHUD();

    const auto backgroundTexture { nce::AssetHolder::get().getTexture("bin/textures/background.png") };
    m_background.emplace(*backgroundTexture);
    m_background->setOrigin(sf::Vector2f(backgroundTexture->getSize()) * 0.5f);
    m_background->setScale((getApp()->getRenderTexture()->getView().getSize() * 1.5f).cwiseDiv(m_background->getLocalBounds().getSize()));
    m_background->setPosition(getApp()->getRenderTexture()->getView().getCenter());

    registerSoundEvents();
    changeCursor(false);
    nce::AssetHolder::get().getTexture("bin/textures/particle.png");
}

BossStateBase::~BossStateBase() { GameMetrics::WriteToDisk(); }

void BossStateBase::update(const sf::Time& dt)
{
    m_background->setPosition(getApp()->getRenderTexture()->getView().getCenter());

    if (getButtonActionChecker().checkAction("pause") && !m_isPaused) {
        m_isPaused = true;
        changeCursor(true);
    }

    if (!getApp()->getRenderWindow()->hasFocus() && !m_isPaused) {
        // m_isPaused = true;
        changeCursor(true);
    }

    if (m_isPaused) {
        getApp()->getRenderWindow()->setMouseCursorGrabbed(false);
        bool exitToMainMenu = false;
        m_pauseMenu.update(getApp()->getRenderWindow(), m_isPaused, exitToMainMenu);

        if (exitToMainMenu) {
            setStateComplete();
            setNextStateID(gc::MENU_STATE_NAME);
        }

        if (!m_isPaused) {
            changeCursor(false);
        }

        return;
    } else {
        // If we're not paused, the game should hook the users mouse to prevent them
        // clicking outside of the window and tabbing themselves out (TODO: migrate
        // this to app since this should just be a core feature)
        if (getApp()->getRenderWindow()->hasFocus())
            getApp()->getRenderWindow()->setMouseCursorGrabbed(true);
        else
            getApp()->getRenderWindow()->setMouseCursorGrabbed(false);
    }

    AppState::update(dt);

    if (m_fightTimerName.empty()) {
        const auto bossId { getObjectOfType<BossBase>()->getIdentifier() };
        m_fightTimerName = fmt::format("{}_fight_timer", bossId);
        getTimerController().addTimer(m_fightTimerName);
    }

    getTimerController().update(dt);

    if (getTimerController().exists("directionChangeTimer")) {
        if (getTimerController().getElapsed("directionChangeTimer").asSeconds() > 2.f) {
            getTimerController().resetTimer("directionChangeTimer");
            getObjectOfType<PlayerCharacter>()->resetDirectionChangeCounter();
        }
    } else {
        getTimerController().addTimer("directionChangeTimer");
    }

    switch (m_currentStage) {
    case GameplayStage::FightingBoss:
        // We only need to update the game objects
        // during the gameplay stage
        updateBossFight(dt);
        break;
    case GameplayStage::PlayerDied:
        updatePlayerDeath();
        break;
    case GameplayStage::BossDied:
        updateBossDeath();
        break;
    default:
        assert(false);
        break;
    }

    // Once we've updated the game objects we can invoke the solver to update
    m_solver.update();

#ifdef ONE_HIT_WONDER_DEBUG
    if (nce::InputHandler::KeyPressed(sf::Keyboard::F2))
        m_showDeveloperUI = !m_showDeveloperUI;

    if (m_showDeveloperUI) {
        updateDeveloperUI(dt);
    }
#endif
}

void BossStateBase::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    target.draw(*m_background, states);
    AppState::draw(target, states);
    m_solver.draw(target);
}

void BossStateBase::addBossHealthBar(BossBase* boss)
{
    if (!addHUDElement(new BossHealthBar(this, boss)))
        throw std::runtime_error("Unable to create boss HP Bar HUD");

    getHUDElementByType<BossHealthBar>()->setLayer(HealthBarLayer);
}

void BossStateBase::generateGenericDevMenu(NodeIdentifiers nodeId, const sf::Time& dt)
{
    enum class MenuMode { Stats, Tools, Variables, Timers };
    static auto menuMode { MenuMode::Stats };

    PushDetailFont();
    ImGui::Begin("Debug UI");
    if (ImGui::BeginTabBar("Menu Mode")) {
        if (ImGui::TabItemButton("Stats"))
            menuMode = MenuMode::Stats;

        if (ImGui::TabItemButton("Tools"))
            menuMode = MenuMode::Tools;

        if (ImGui::TabItemButton("Variables"))
            menuMode = MenuMode::Variables;

        if (ImGui::TabItemButton("Timers"))
            menuMode = MenuMode::Timers;

        ImGui::EndTabBar();
    }

    switch (menuMode) {
    case MenuMode::Stats: {
        static sf::Time dtAverage;
        static unsigned int counter { 0 };

        if (counter < 10) {
            dtAverage += dt;
            ++counter;
        } else {

            dtAverage = sf::Time::Zero;
            counter = 0;
        }

        static std::string dtAdjusted;
        static std::string fpsAdjusted;

        if (dtAverage != sf::Time::Zero && counter != 0) {
            const auto avg { dtAverage.asSeconds() / (float)(counter) };
            dtAdjusted = fmt::format("{:4f}", avg);
            fpsAdjusted = fmt::format("{:2f}", 1.f / avg);
        }

        ImGui::Text("%s", fmt::format("Delta Time {}", dtAdjusted).c_str());
        ImGui::Text("%s", fmt::format("FPS {}", fpsAdjusted).c_str());
        break;
    }
    case MenuMode::Tools: {
        auto player = getObjectOfType<PlayerCharacter>();

        if (ImGui::Button(fmt::format("{} Invincibility", player->isDebugInvincible() ? "Disable" : "Enable").data()))
            player->setDebugInvincibility(!player->isDebugInvincible());

        const auto debugDrawSolver = m_solver.debugDrawEnabled();
        if (ImGui::Button(fmt::format("{} Collision Debug Drawing", debugDrawSolver ? "Disable" : "Enable").data()))
            m_solver.shouldDebugDraw(!debugDrawSolver);

        if (ImGui::Button("Slay Boss!"))
            debugKillBoss();
    } break;
    case MenuMode::Variables:
        if (ImGui::CollapsingHeader("World Variables")) {
            const auto worldFloatKeys = GameVars().getAllFloatKeys(NodeIdentifiers::World);
            const auto worldVectorKeys = GameVars().getAllVectorKeys(NodeIdentifiers::World);
            ImGui::Text("Decimal Variables");
            for (const auto& k : worldFloatKeys) {
                auto value { getWorldFloatVar(k) };
                if (ImGui::InputFloat(k.c_str(), &value))
                    setWorldFloatVar(k, value);
            }
            ImGui::Text("Vector Variables");
            for (const auto& k : worldVectorKeys) {
                std::array<float, 2> copy { getWorldVectorVar(k).x, getWorldVectorVar(k).y };
                if (ImGui::InputFloat2(k.c_str(), copy.data()))
                    setWorldVectorVar(k, { copy[0], copy[1] });
            }
        }

        if (ImGui::CollapsingHeader("Player Variables")) {
            const auto playerFloatKeys = GameVars().getAllFloatKeys(NodeIdentifiers::Player);
            ImGui::Text("Decimal Variables");
            for (const auto& k : playerFloatKeys) {
                auto value { getPlayerFloatVar(k) };
                if (ImGui::InputFloat(k.c_str(), &value))
                    setPlayerFloatVar(k, value);
            }
            ImGui::Text("Vector Variables");
            // TODO: Add vector variables when required
        }

        if (ImGui::CollapsingHeader("Shared Boss Variables")) {
            const auto worldFloatKeys = GameVars().getAllFloatKeys(NodeIdentifiers::BossesShared);
            ImGui::Text("Decimal Variables");
            for (const auto& k : worldFloatKeys) {
                auto value { getBossesSharedFloatVar(k) };
                if (ImGui::InputFloat(k.c_str(), &value))
                    setBossesSharedFloatVar(k, value);
            }
            ImGui::Text("Vector Variables");
            // TODO: Add vector variables when required
        }

        if (ImGui::CollapsingHeader("Current Boss Variables")) {
            const auto currentBossFloatKeys = GameVars().getAllFloatKeys(nodeId);
            const auto currentBossVectorKeys = GameVars().getAllVectorKeys(nodeId);
            ImGui::Text("Decimal Variables");
            for (const auto& k : currentBossFloatKeys) {
                auto value { GameVars().getFloat(nodeId, k) };
                if (ImGui::InputFloat(k.c_str(), &value))
                    GameVars().setFloat(nodeId, k, value);
            }
            ImGui::Text("Vector Variables");
            for (const auto& k : currentBossVectorKeys) {
                switch (nodeId) {
                case NodeIdentifiers::BossesDancingPrinces: {
                    std::array<float, 2> copy { getBossesDancingPrincesVectorVar(k).x, getBossesDancingPrincesVectorVar(k).y };
                    if (ImGui::InputFloat2(k.c_str(), copy.data()))
                        setWorldVectorVar(k, { copy[0], copy[1] });
                } break;
                default:
                    assert(false);
                    break;
                }
                // TODO: Add other boss vector variables when required
            }
        }
        if (ImGui::Button("Save to file"))
            GameVars().writeToFile();
        break;
    case MenuMode::Timers: {
        const auto allTimerKeys { getTimerController().getKeys() };
        for (const auto& key : allTimerKeys) {
            const auto isPaused { getTimerController().isPaused(key) };
            const auto time { getTimerController().getElapsed(key).asSeconds() };
            const auto outputStr { fmt::format("[Name: {} - Paused Status: {} - Elapsed: {} seconds", key, isPaused, time) };
            ImGui::Text("%s", outputStr.data());
        }
    } break;
    default:
        assert(false);
        break;
    }

    ImGui::End();
    ImGui::PopFont();
}

void BossStateBase::updateBossFight(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    if (getBossHealth() <= 0) {
        setGameplayStage(GameplayStage::BossDied);
    }
}

void BossStateBase::setGameplayStage(GameplayStage stage)
{
    switch (stage) {
    case GameplayStage::FightingBoss: {
        ++m_attemptCount;
        auto player { getObjectOfType<PlayerCharacter>() };
        player->reset();
        player->unlockPlayerControls();

        for (auto& p : m_arrowProjectiles) {
            p->setActive(false);
        }

        for (auto& bp : m_bossProjectiles) {
            bp->reset();
        }
        getTimerController().resetTimer("directionChangeTimer");
        m_playerDeathText->setActive(false);
        getTimerController().resetTimer(m_fightTimerName);
        getHUDElementByType<DarkenHUDAnimated>()->setActive(false);
    } break;
    case GameplayStage::PlayerDied: {
        m_fadeInTimer.restart();
        const auto fill { m_playerDeathText->getFillColour() };
        m_playerDeathText->setFillColour(sf::Color { fill.r, fill.g, fill.b, 0 });
        m_playerDeathText->setActive(true);
        auto player { getObjectOfType<PlayerCharacter>() };
        player->lockPlayerControls();

        GameMetrics::AddPlayerDeath();

        if (!getHUDElementByType<DarkenHUDAnimated>()->isActive()) {
            getHUDElementByType<DarkenHUDAnimated>()->playDarken();
            getHUDElementByType<DarkenHUDAnimated>()->setActive(true);
        }

    } break;
    case GameplayStage::BossDied: {
        m_fadeInTimer.restart();
        auto player = getObjectOfType<PlayerCharacter>();
        assert(player);
        if (!player)
            throw std::runtime_error(std::format("No PlayerCharacter found in {}", __LINE__));

        player->lockPlayerControls();

        const auto deathFill { m_bossDeathText->getFillColour() };
        const auto continueFill { m_bossContinueText->getFillColour() };

        m_bossDeathText->setFillColour(sf::Color { deathFill.r, deathFill.g, deathFill.b, 0 });
        m_bossContinueText->setFillColour(sf::Color { continueFill.r, continueFill.g, continueFill.b, 0 });
        if (getTimerController().exists(m_fightTimerName)) {
            const auto elapsed { getTimerController().getElapsed(m_fightTimerName) };
            getTimerController().removeTimer(m_fightTimerName);

            // Set metrics
            GameMetrics::SetBossCompleted(m_bossIndex);
            GameMetrics::SetBossKillTime(m_bossIndex, elapsed);
            GameMetrics::SetBossAttemptsCount(m_bossIndex, m_attemptCount);

            const auto fired = player->getTotalPlayerShotsFired();
            if (fired) {
                const auto accuracy { (float(m_shotsOnTarget) / float(fired)) * 100.f };
                GameMetrics::SetPlayerAccuracyOnBoss(m_bossIndex, accuracy);
            }
        }

        if (!getHUDElementByType<DarkenHUDAnimated>()->isActive()) {
            getHUDElementByType<DarkenHUDAnimated>()->playDarken();
            getHUDElementByType<DarkenHUDAnimated>()->setActive(true);
        }

        m_bossDeathText->setActive(true);
        m_bossContinueText->setActive(true);
        break;
    }
    default:
        assert(false);
        break;
    }

    m_currentStage = stage;
}

void BossStateBase::setBossDeathText(std::string_view str) { m_bossDeathText->setString(str); }

void BossStateBase::bindControls()
{
    SetupImGuiPadBinds(getButtonActionChecker());

    // Add keyboard actions
    getButtonActionChecker().addKeyboardAction("move_left", nce::ButtonActionChecker::ActionType::Held, sf::Keyboard::A);
    getButtonActionChecker().addKeyboardAction("move_left", nce::ButtonActionChecker::ActionType::Held, sf::Keyboard::Left);

    getButtonActionChecker().addKeyboardAction("move_right", nce::ButtonActionChecker::ActionType::Held, sf::Keyboard::D);
    getButtonActionChecker().addKeyboardAction("move_right", nce::ButtonActionChecker::ActionType::Held, sf::Keyboard::Right);
    getButtonActionChecker().addKeyboardAction("dodge", nce::ButtonActionChecker::ActionType::Pressed, sf::Keyboard::Space);

    getButtonActionChecker().addKeyboardAction("pause", nce::ButtonActionChecker::ActionType::Pressed, sf::Keyboard::Escape);

    getButtonActionChecker().addMouseAction("shoot_hold", nce::ButtonActionChecker::ActionType::Held, sf::Mouse::Button::Left);
    getButtonActionChecker().addMouseAction("shoot_release", nce::ButtonActionChecker::ActionType::Released, sf::Mouse::Button::Left);

    // Add gamepad actions
    getButtonActionChecker().addGamePadAction("move_left", nce::ButtonActionChecker::ActionType::Held, nce::GamePad::Buttons::DirectionLeft);
    getButtonActionChecker().addGamePadAction("move_right", nce::ButtonActionChecker::ActionType::Held, nce::GamePad::Buttons::DirectionRight);

    getButtonActionChecker().addGamePadAxisAsAction("move_left", nce::ButtonActionChecker::ActionAxisRange::Negative, nce::GamePad::Axis::LeftStickX);
    getButtonActionChecker().addGamePadAxisAsAction("move_right", nce::ButtonActionChecker::ActionAxisRange::Positive, nce::GamePad::Axis::LeftStickX);

    getButtonActionChecker().addGamePadAction("dodge", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::LeftShoulder);
    getButtonActionChecker().addGamePadAction("dodge", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::RightShoulder);

    getButtonActionChecker().addGamePadAction("shoot_hold", nce::ButtonActionChecker::ActionType::Held, nce::GamePad::Buttons::LeftTrigger);
    getButtonActionChecker().addGamePadAction("shoot_release", nce::ButtonActionChecker::ActionType::Released, nce::GamePad::Buttons::LeftTrigger);

    getButtonActionChecker().addGamePadAction("shoot_hold", nce::ButtonActionChecker::ActionType::Held, nce::GamePad::Buttons::RightTrigger);
    getButtonActionChecker().addGamePadAction("shoot_release", nce::ButtonActionChecker::ActionType::Released, nce::GamePad::Buttons::RightTrigger);

    getButtonActionChecker().addGamePadAction("pause", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::Start);
}

void BossStateBase::registerSoundEvents()
{
    nce::SoundEvent dodgeEvent;
    dodgeEvent.assetName = "bin/sounds/dodge.wav";
    getSfxManager().registerEvent("dodge", dodgeEvent);

    nce::SoundEvent dodgePingEvent;
    dodgeEvent.assetName = "bin/sounds/dodge_ping.wav";
    dodgeEvent.type = nce::SoundEventType::SingleInstance;
    getSfxManager().registerEvent("dodge_ping", dodgeEvent);

    nce::SoundEvent bowDraw;
    bowDraw.assetName = "bin/sounds/bow_draw.wav";
    bowDraw.type = nce::SoundEventType::SingleInstance;
    getSfxManager().registerEvent("bow_draw", bowDraw);

    nce::SoundEvent bowFire;
    bowFire.assetName = "bin/sounds/bow_fire.wav";
    bowFire.type = nce::SoundEventType::SingleInstance;
    getSfxManager().registerEvent("bow_fire", bowFire);

    nce::SoundEvent arrowImpactBoss;
    arrowImpactBoss.assetName = "bin/sounds/arrow_impact_boss.wav";
    arrowImpactBoss.type = nce::SoundEventType::ManyInstance;
    getSfxManager().registerEvent("arrow_impact_boss", arrowImpactBoss);

    nce::SoundEvent arrowImpactFloor;
    arrowImpactFloor.assetName = "bin/sounds/arrow_impact_floor.wav";
    arrowImpactFloor.type = nce::SoundEventType::ManyInstance;
    getSfxManager().registerEvent("arrow_impact_floor", arrowImpactFloor);
}

void BossStateBase::setupHUD()
{
    constexpr auto fontStr { "bin/fonts/OldWizard.ttf" };
    constexpr auto TextFillColor { sf::Color { sf::Color::Yellow } };

    if (!addHUDElement(new PadAimIndicator(this, getObjectOfType<PlayerCharacter>())))
        throw std::runtime_error("Unable to create pad aim indicator");

    getHUDElementByType<PadAimIndicator>()->setLayer(NormalHUDLayer);

    m_playerDeathText = new nce::HUDText(this, fontStr, "       You were slain!\n\n\n(Dodge Key) to try again!", 64);
    if (!addHUDElement(m_playerDeathText))
        throw std::runtime_error("Unable to create player death text");

    m_playerDeathText->setAnchorPosition({ 0.5f, 0.5f });
    m_playerDeathText->setOriginLocation(nce::HUDElement::OriginLocation::Centre);
    m_playerDeathText->setActive(false);
    m_playerDeathText->setFillColour(TextFillColor);
    m_playerDeathText->setLayer(NormalHUDLayer);

    m_bossDeathText = new nce::HUDText(this, fontStr, "", 64);
    if (!addHUDElement(m_bossDeathText))
        throw std::runtime_error("Unable to create boss death text");

    m_bossDeathText->setAnchorPosition({ 0.5f, 0.5f });
    m_bossDeathText->setOriginLocation(nce::HUDElement::OriginLocation::Centre);
    m_bossDeathText->setActive(false);
    m_bossDeathText->setFillColour(TextFillColor);
    m_bossDeathText->setLayer(NormalHUDLayer);

    m_bossContinueText = new nce::HUDText(this, fontStr, "(Space) to fight next boss!", 64);
    if (!addHUDElement(m_bossContinueText))
        throw std::runtime_error("Unable to create boss continue text");

    m_bossContinueText->setAnchorPosition({ 0.5f, 0.65f });
    m_bossContinueText->setOriginLocation(nce::HUDElement::OriginLocation::Centre);
    m_bossContinueText->setActive(false);
    m_bossContinueText->setFillColour(TextFillColor);
    m_bossContinueText->setLayer(NormalHUDLayer);

    m_bowPullbackHUD = new BowPullbackHUD(this, getObjectOfType<PlayerCharacter>());
    if (!addHUDElement(m_bowPullbackHUD))
        throw std::runtime_error("Unable to create bow pullback error");

    m_bowPullbackHUD->setLayer(NormalHUDLayer);

    if (!addHUDElement(new DarkenHUDAnimated(this, HUD_FADE_IN_TIME, sf::Color { 10, 10, 10 }, sf::Vector2f { getApp()->getRenderTexture()->getSize() })))
        throw std::runtime_error("Unable to create darken HUD element");

    getHUDElementByType<DarkenHUDAnimated>()->setLayer(DarkenHUDLayer);
    getHUDElementByType<DarkenHUDAnimated>()->setActive(false);
}

void BossStateBase::updatePlayerDeath()
{
    const auto fill { m_playerDeathText->getFillColour() };
    const auto elapsedInSeconds = std::clamp(m_fadeInTimer.getElapsedTime().asSeconds(), 0.f, 1.f);
    const auto colour = nce::LerpColour(sf::Color { fill.r, fill.g, fill.b, 0 }, sf::Color { fill.r, fill.g, fill.b, 255 }, elapsedInSeconds);
    m_playerDeathText->setFillColour(colour);
    if (m_fadeInTimer.getElapsedTime() >= HUD_FADE_IN_TIME) {
        if (getButtonActionChecker().checkAction("dodge"))
            setGameplayStage(GameplayStage::FightingBoss);
    }
}

void BossStateBase::updateBossDeath()
{
    const auto deathFill { m_bossDeathText->getFillColour() };
    const auto continueFill { m_bossContinueText->getFillColour() };

    const auto elapsedInSeconds = std::clamp(m_fadeInTimer.getElapsedTime().asSeconds(), 0.f, 1.f);
    const auto deathTextColour
        = nce::LerpColour(sf::Color { deathFill.r, deathFill.g, deathFill.b, 0 }, sf::Color { deathFill.r, deathFill.g, deathFill.b, 255 }, elapsedInSeconds);
    const auto continueTextColour = nce::LerpColour(
        sf::Color { continueFill.r, continueFill.g, continueFill.b, 0 }, sf::Color { continueFill.r, continueFill.g, continueFill.b, 255 }, elapsedInSeconds);
    m_bossDeathText->setFillColour(deathTextColour);
    m_bossContinueText->setFillColour(continueTextColour);

    if (m_fadeInTimer.getElapsedTime() >= HUD_FADE_IN_TIME) {
        if (getButtonActionChecker().checkAction("dodge"))
            setStateComplete();
    }
}

void BossStateBase::changeCursor(bool useDefault)
{
    if (m_crosshairCursor.getPixelsPtr() == nullptr) {
        auto cursorTexture { nce::AssetHolder::get().getTexture("bin/textures/Crosshair.png") };
        if (!cursorTexture) {
            spdlog::warn("Unable to set cursor from system");
            return;
        }
        m_crosshairCursor = cursorTexture->copyToImage();
    }

    if (useDefault) {
        if (m_cursor.loadFromSystem(sf::Cursor::Arrow))
            getApp()->getRenderWindow()->setMouseCursor(m_cursor);
    } else {
        if (m_crosshairCursor.getPixelsPtr() == nullptr)
            return;

        if (!m_cursor.loadFromPixels(m_crosshairCursor.getPixelsPtr(), m_crosshairCursor.getSize(), m_crosshairCursor.getSize() / 2u)) {
            spdlog::warn("Unabe to load cursor from ");
        } else {
            getApp()->getRenderWindow()->setMouseCursor(m_cursor);
        }
    }
}
