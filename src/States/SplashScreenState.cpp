#include "SplashScreenState.hpp"
#include "../GameplayConstants.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/RNG.hpp>
#include <nce/TextUtils.hpp>
#include <nce/TimerController.hpp>
#include <spdlog/spdlog.h>

constexpr std::array<char, 25> ENGINE_NAME { "Nebulous Creation Engine" };
constexpr auto CHARACTER_APPEND_TIME { sf::seconds(0.25f) };
constexpr auto SPLASH_TIMER_LENGTH { sf::seconds(float(ENGINE_NAME.size()) * CHARACTER_APPEND_TIME.asSeconds()) + sf::seconds(1.5f) };

SplashScreenState::SplashScreenState(nce::App* app, const sf::Vector2f& playAreaSize)
    : AppState(gc::ENGINE_SPLASH_STATE_NAME, app, playAreaSize)
{
    for (auto& c : ENGINE_NAME) {
        m_engineName.push_back(c);
        spdlog::warn("Is Empty?", m_engineName.empty());
    }

    m_backgroundFill.setSize(playAreaSize);
    m_backgroundFill.setFillColor(sf::Color::Black);

    const auto font = nce::AssetHolder::get().getFont("bin/fonts/OldWizard.ttf");
    font->setSmooth(false);
    m_engineSplashText.emplace(*font);
    m_engineSplashText->setCharacterSize(48);
    m_engineSplashText->setString("");
    nce::CentreTextOrigin(*m_engineSplashText);

    m_engineSplashText->setPosition(sf::Vector2f { playAreaSize } * 0.5f);
    spdlog::warn("Text Pos {} {}", m_engineSplashText->getPosition().x, m_engineSplashText->getPosition().y);
    getTimerController().addTimer("engine_splash_timer");
    getTimerController().addTimer("splash_append_timer");

    m_keyStrikeSounds[0] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_1.wav"));
    m_keyStrikeSounds[1] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_2.wav"));
    m_keyStrikeSounds[2] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_3.wav"));
    m_keyStrikeSounds[3] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_4.wav"));
    m_keyStrikeSounds[4] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_5.wav"));
    m_keyStrikeSounds[5] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_6.wav"));
    m_keyStrikeSounds[6] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_7.wav"));
    m_keyStrikeSounds[7] = sf::Sound(*nce::AssetHolder::get().getSoundBuffer("bin/sounds/keystrike_8.wav"));
}

SplashScreenState::~SplashScreenState()
{
    getTimerController().removeTimer("engine_splash_timer");
    getTimerController().removeTimer("splash_append_timer");
}

void SplashScreenState::update(const sf::Time& dt)
{
    getTimerController().update(dt);
    const auto elapsed { getTimerController().getElapsed("engine_splash_timer") };
    if (elapsed > SPLASH_TIMER_LENGTH) {
        setStateComplete();
        setNextStateID(gc::MENU_STATE_NAME);
    }

    if (getTimerController().getElapsed("splash_append_timer") > CHARACTER_APPEND_TIME && !m_engineName.empty()) {
        getTimerController().resetTimer("splash_append_timer");
        auto c = m_engineName.front();
        if (c != '\0')
            m_engineSplashText->setString(m_engineSplashText->getString() + c);
        nce::CentreTextOrigin(*m_engineSplashText);
        m_engineName.pop_front();

        auto randIndex = m_keystrikeIndex;
        while (randIndex == m_keystrikeIndex) {
            randIndex = size_t(nce::RNG::intWithinRange(0, int(m_keyStrikeSounds.size() - 1)));
        }
        m_keystrikeIndex = randIndex;
        m_keyStrikeSounds[m_keystrikeIndex].play();
        m_keystrikeIndex = (m_keystrikeIndex + 1 >= m_keyStrikeSounds.size() ? 0 : m_keystrikeIndex + 1);
    }
}

void SplashScreenState::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    AppState::draw(target, states);
    target.draw(m_backgroundFill, states);
    target.draw(*m_engineSplashText, states);
}
