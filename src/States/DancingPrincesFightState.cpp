#include "DancingPrincesFightState.hpp"
#include "../Effects/CameraShake.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../HUD/GUIHelpers.hpp"
#include "../Objects/AcidFloor.hpp"
#include "../Objects/DancingPrinces.hpp"
#include "../Objects/PlayerCharacter.hpp"
#include "../Objects/UndergroundSpike.hpp"
#include "../Utils/GameplayUtility.hpp"

#include <imgui.h>
#include <nce/InputHandler.hpp>
#include <spdlog/spdlog.h>

constexpr auto SPIKE_START_WAVE { sf::seconds(0.75f) };

// This is a workaround to allow us to draw the room segments
// when required
class HighlightedSegment : public nce::GameObject {
public:
    HighlightedSegment(nce::AppState* currentState, sf::FloatRect region)
        : nce::GameObject(currentState)
        , m_shape(region.getSize())
    {
        m_shape.setPosition(region.getPosition());
        auto col = nce::GenerateRandomColour();
        col.a = 100;
        m_shape.setFillColor(col);
    }

private:
    sf::RectangleShape m_shape;

    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override { target.draw(m_shape, states); }
    virtual void update(const sf::Time& dt) override { NCE_UNUSED(dt); }
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }
};

DancingPrinceFightState::DancingPrinceFightState(nce::App* app, const sf::Vector2f& playAreaSize)
    : BossStateBase(gc::DANCING_PRINCES_STATE_NAME, app, playAreaSize, gc::DANCING_PRINCES_INDEX)
    , m_viewMax(sf::Vector2f { app->getRenderTexture()->getView().getSize() } * gc::ZOOM_OUT_SCALE)
    , m_cachedView(app->getRenderTexture()->getView())
    , m_zoomOutTween(
          std::make_unique<nce::TweeningAnimation<float>>(nce::AnimationCurve::Lerp, "pan_zoom_out", 1.f, 1.5f, sf::seconds(2.5f), &getTimerController()))
{
    m_zoomOutTween->play();
    m_bossProjectiles.resize(gc::BOSS_PROJECTILES_COUNT_DANCING_PRINCES);
    for (size_t i { 0 }; i < gc::BOSS_PROJECTILES_COUNT_DANCING_PRINCES; ++i) {
        m_bossProjectiles[i] = new BossProjectile(this);
        m_bossProjectiles[i]->setActive(false);
        if (!addGameObject(m_bossProjectiles[i]))
            throw std::runtime_error("Unable to allocate boss projectile");
    }

    // We need to calculate the regions of the level that exist, this level has 3 zones
    // Zone 0 & 2 are for the bosses, zone 1 is the middle segment which is traversable
    // to swap between bosses. Since the view will be zoomed out, the calculations
    // of these zones is done here
    sf::View v { m_cachedView };
    v.zoom(gc::ZOOM_OUT_SCALE);
    const auto floorBounds { getFloor()->getBoundsRect() };
    v.setCenter({ v.getCenter().x, (floorBounds.top + floorBounds.height) - (v.getSize().y / 2.f) });
    const auto viewTopLeft { v.getCenter() - (v.getSize() / 2.f) };
    const auto quarterWidth { v.getSize().x / 4.f };

    m_levelRegions[0] = sf::FloatRect { viewTopLeft, { quarterWidth, m_viewMax.y } };
    m_levelRegions[1] = sf::FloatRect { sf::Vector2f { viewTopLeft.x + quarterWidth, viewTopLeft.y }, sf::Vector2f { 2 * quarterWidth, m_viewMax.y } };
    m_levelRegions[2] = sf::FloatRect { sf::Vector2f { viewTopLeft.x + quarterWidth * 3.f, viewTopLeft.y }, sf::Vector2f { quarterWidth, m_viewMax.y } };

    // Add boss game object
    if (!addGameObject(new DancingPrinces(this, getPlayer(), m_bossProjectiles, m_levelRegions)))
        throw std::runtime_error("Unable to create dancing princes");

    // This is a debug feature so we can see the boss zones & the centre zone visually
    if (!addGameObject(new HighlightedSegment(this, m_levelRegions[0])))
        throw std::runtime_error("Unable to create highlighted segment");

    if (!addGameObject(new HighlightedSegment(this, m_levelRegions[1])))
        throw std::runtime_error("Unable to create highlighted segment");

    if (!addGameObject(new HighlightedSegment(this, m_levelRegions[2])))
        throw std::runtime_error("Unable to create highlighted segment");

    auto list { getAllObjectsOfType<HighlightedSegment>() };
    for (auto& li : list) {
        li->setActive(false);
    }

    constexpr auto SpikeCount = 14;
    constexpr auto totalBounds { (UNDERGROUND_SPIKE_SIZE.x * SpikeCount) + (SPIKE_SPACING * (SpikeCount - 1)) };
    const auto padding(m_levelRegions[1].width - totalBounds);

    for (size_t i { 0 }; i < size_t(SpikeCount); ++i) {
        const auto xPos { (m_levelRegions[1].left + (i * (UNDERGROUND_SPIKE_SIZE.x + SPIKE_SPACING))) + padding / 2.f };
        auto ptr = new UndergroundSpike(this, m_levelRegions[1], xPos);
        ptr->setActive(false);
        if (!addGameObject(ptr))
            throw std::runtime_error("Unable to create underground spike");
        m_undergroundSpikes.push_back(ptr);
    }

    getPlayer()->lockPlayerControls();
    getTimerController().addTimer("play_spike_raise_fight_start");
    addBossHealthBar(getObjectOfType<DancingPrinces>());

    getSfxManager().playMusic("bin/music/menu_music.mp3");
    setBossDeathText(fmt::format("{} has been slain!", gc::BOSS_NAMES[gc::DANCING_PRINCES_INDEX]));
}

DancingPrinceFightState::~DancingPrinceFightState()
{
    // We need to reset the view back to the one we cached at the start
    // but first we have to destroy the camera shake to prevent a
    // bad view position being applied. Don't re-order these.
    m_cameraShake.reset();
    getApp()->getRenderTexture()->setView(m_cachedView);
    getSfxManager().stopMusic("bin/music/menu_music.mp3");
}

void DancingPrinceFightState::update(const sf::Time& dt)
{
    BossStateBase::update(dt);
    if (BossStateBase::isPaused())
        return;

    if (!m_zoomOutTween)
        updateState(dt);
    else
        updateZoomOut();
}

void DancingPrinceFightState::updateDeveloperUI(const sf::Time& dt)
{
    generateGenericDevMenu(NodeIdentifiers::BossesDancingPrinces, dt);

    static bool showHighlights { false };
    PushDetailFont();
    ImGui::Begin("Dancing Princes Debug Tools");
    if (ImGui::Button(fmt::format("{} Highlights", !showHighlights ? "Show" : "Hide").c_str())) {
        showHighlights = !showHighlights;
        auto list { getAllObjectsOfType<HighlightedSegment>() };
        for (auto& li : list)
            li->setActive(showHighlights);
    }

    auto boss = getObjectOfType<DancingPrinces>();
    assert(boss);

    if (ImGui::Button("Force Phase 1 Swap"))
        boss->forcePhase1Swap();

    ImGui::End();
    ImGui::PopFont();
}

void DancingPrinceFightState::debugKillBoss() { getObjectOfType<DancingPrinces>()->debugSlay(); }

int32_t DancingPrinceFightState::getBossHealth() const { return getObjectOfType<DancingPrinces>()->getHealth(); }

void DancingPrinceFightState::setGameplayStage(GameplayStage stage)
{
    switch (stage) {
    case GameplayStage::FightingBoss:
        for (auto& spike : m_undergroundSpikes)
            spike->reset();
        for (auto& pool : getAllObjectsOfType<AcidFloor>())
            removeGameObject(pool);

        getObjectOfType<DancingPrinces>()->reset();
        getTimerController().addTimer("play_spike_raise_fight_start");
        break;
    case GameplayStage::PlayerDied:
        m_cameraShake.reset();
        break;
    case GameplayStage::BossDied:
        setNextStateID(gc::MENU_STATE_NAME);
        break;
    default:
        assert(false);
        break;
    }

    BossStateBase::setGameplayStage(stage);
}

void DancingPrinceFightState::updateState(const sf::Time& dt)
{
    NCE_UNUSED(dt);

    if (getGameplayStage() != GameplayStage::PlayerDied) {
        auto player { getPlayer() };
        if (player->wasPlayerHit())
            setGameplayStage(GameplayStage::PlayerDied);
    }

    const auto playerPos { getPlayer()->getPosition() };
    auto boss { getObjectOfType<DancingPrinces>() };

    // If we're in phase 1 of the boss, and the player is inside the centre region, and we're not currently playing a spike wave
    // let's start a spike wave.
    if (boss->getBossPhase() == DancingPrinces::BossPhase::Phase1) {
        // Play raise
        if (getTimerController().exists("play_spike_raise_fight_start")) {
            if (getTimerController().getElapsed("play_spike_raise_fight_start") > SPIKE_START_WAVE) {
                if (!getTimerController().isPaused("play_spike_raise_fight_start")) {
                    playSpikeWave(true, !boss->isLeftBossActive());
                } else {
                    if (!m_lastSpike->isAnimating()) {
                        getTimerController().removeTimer("play_spike_raise_fight_start");
                        m_isPlayingSpikeWave = false;
                        m_cameraShake.reset(nullptr);
                        spdlog::debug("Removing play spike raise fight start timer");
                    }
                }
            }
        } else {
            if (boss->readyToShieldSwap() && m_shieldSwapStages == BossShieldSwapStages::Default) {
                m_shieldSwapStages = BossShieldSwapStages::LowerAndSwap;
                spdlog::debug("Shield swap sequence starting");
            }

            switch (m_shieldSwapStages) {
                using enum BossShieldSwapStages;
            case LowerAndSwap:
                if (!m_isPlayingSpikeWave) {
                    if (!m_levelRegions[1].contains(playerPos))
                        playSpikeWave(false, !boss->isLeftBossActive());
                    spdlog::debug("Triggering spike lower");
                } else {
                    if (!m_lastSpike->isAnimating() && !m_lastSpike->isAnimationQueued()) {
                        m_cameraShake.reset();
                        m_isPlayingSpikeWave = false;
                        boss->swapShields();
                        m_shieldSwapStages = BossShieldSwapStages::Wait;
                        spdlog::debug("Entering wait sequence");
                    }
                }
                break;
            case Wait: {
                const size_t index { boss->isLeftBossActive() ? size_t(2) : size_t(0) };
                if (!m_isPlayingSpikeWave) {
                    // We need to give the player just enough room to escape before starting the raise sequence
                    const auto centreX { m_levelRegions[index].getPosition().x + (m_levelRegions[index].getSize().x / 2.f) };
                    const auto adjustedDistance { (m_levelRegions[index].getSize().x / 2.f) * 1.5f };
                    if (std::abs(centreX - playerPos.x) > adjustedDistance) {
                        playSpikeWave(true, !boss->isLeftBossActive());
                        spdlog::debug("Player has left their region, it's safe to play the spike wave");
                    }
                } else {
                    m_cameraShake.reset();
                    m_isPlayingSpikeWave = false;
                    m_shieldSwapStages = BossShieldSwapStages::Default;
                    spdlog::debug("Returning to default state");
                }
            } break;
            case Default: {
                auto acidPools = getAllObjectsOfType<AcidFloor>();
                for (auto& pool : acidPools) {
                    if (pool->hasTimedOut())
                        removeGameObject(pool);
                }
            } break;
            default:
                break;
            }
        }
    }

    if (m_cameraShake) {
        m_cameraShake->update();
    }

    if (nce::InputHandler::KeyPressed(sf::Keyboard::P)) {
        playSpikeWave(false, boss->isLeftBossActive());
    }
}

void DancingPrinceFightState::updateZoomOut()
{
    // Zoom out anim here
    if (m_zoomOutTween) {
        if (!m_zoomOutTween->isComplete()) {
            sf::View temp { m_cachedView };
            temp.zoom(m_zoomOutTween->getTweenedValue());

            // We need to keep the floor and the player in the same place relatively on the screen
            // which means as we zoom out we want to adjust the centre so the floor is visible
            const auto floorBounds { getFloor()->getBoundsRect() };
            temp.setCenter({ temp.getCenter().x, (floorBounds.top + floorBounds.height) - (temp.getSize().y / 2.f) });
            getApp()->getRenderTexture()->setView(temp);

        } else {
            m_zoomOutTween.reset(nullptr);
            const auto& v { getApp()->getRenderTexture()->getView() };
            const sf::FloatRect bounds { v.getCenter() - v.getSize() * 0.5f, v.getSize() };
            const auto initData { GenerateSolverFromViewBounds(bounds) };
            getSolver().reconstructGrid(initData.cellSize, initData.gridSize, initData.position);
            getObjectOfType<DancingPrinces>()->setZoomOutFinished();
            resetSpikes();
            getPlayer()->unlockPlayerControls();

            getTimerController().playTimer("play_spike_raise_fight_start");
        }
    }
}

void DancingPrinceFightState::playSpikeWave(bool isRaise, bool leftToRight)
{
    size_t i { 0 };
    const auto performPlayLogic = [this, isRaise](size_t i, float delayTime, UndergroundSpike* spike, bool lastSpike) -> void {
        spike->setActive(true);
        if (isRaise)
            spike->startRaise(sf::seconds(float(i) * delayTime));
        else
            spike->startLower(sf::seconds(float(i) * delayTime));
        if (lastSpike) {
            m_lastSpike = spike;
        }
    };

    const auto delayTime { GameVars().getFloat(NodeIdentifiers::BossesDancingPrinces, "spike_delay_time") };
    const auto spikeHoldTime { GameVars().getFloat(NodeIdentifiers::BossesDancingPrinces, "spike_delay_time") };

    if (leftToRight) {
        for (auto it = m_undergroundSpikes.begin(); it != m_undergroundSpikes.end(); ++it) {
            const auto lastSpike { it + 1 == m_undergroundSpikes.end() };
            performPlayLogic(i, delayTime, *it, lastSpike);
            ++i;
        }
    } else {
        for (auto it = m_undergroundSpikes.rbegin(); it != m_undergroundSpikes.rend(); ++it) {
            const auto lastSpike { it + 1 == m_undergroundSpikes.rend() };
            performPlayLogic(i, delayTime, *it, lastSpike);
            ++i;
        }
    }

    if (getTimerController().exists("play_spike_raise_fight_start"))
        getTimerController().pauseTimer("play_spike_raise_fight_start");

    m_isPlayingSpikeWave = true;

    const auto maxTime { sf::seconds((delayTime * m_undergroundSpikes.size()) + (spikeHoldTime * m_undergroundSpikes.size())) };
    m_cameraShake
        = std::make_unique<CameraShake>(getApp()->getRenderTexture(), getTimerController(), maxTime, getBossesDancingPrincesFloatVar("shake_effect_intensiy"));
}

void DancingPrinceFightState::resetSpikes()
{
    for (auto& spike : m_undergroundSpikes) {
        spike->reset();
        spike->setActive(false);
    }
    m_cameraShake.reset(nullptr);
}
