#include "MenuState.hpp"
#include "../GameplayConstants.hpp"
#include "../HUD/GUIHelpers.hpp"
#include "../HUD/MenuConstants.hpp"
#include "../Utils/GameMetrics.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/InputHandler.hpp>
#include <nce/NCE.hpp>

#include <SFML/Window/VideoMode.hpp>
#include <imgui-SFML.h>
#include <imgui.h>
#include <spdlog/spdlog.h>

MenuState::MenuState(nce::App* app, const sf::Vector2f& playAreaSize)
    : nce::AppState(gc::MENU_STATE_NAME, app, playAreaSize)
    , m_app(app)
{
    assert(app);
    getSfxManager().playMusic("bin/music/menu_music.mp3");
    SetupImGuiPadBinds(getButtonActionChecker());
    const auto backgroundTexture { nce::AssetHolder::get().getTexture("bin/textures/background.png") };
    if (backgroundTexture) {
        m_background.emplace(*backgroundTexture);
        m_background->setOrigin(sf::Vector2f(backgroundTexture->getSize()) * 0.5f);
        m_background->setScale((getApp()->getRenderTexture()->getView().getSize() * 1.5f).cwiseDiv(m_background->getLocalBounds().getSize()));
        m_background->setPosition(getApp()->getRenderTexture()->getView().getCenter());
    }
}

MenuState::~MenuState() { getSfxManager().stopMusic("bin/music/Menu Music.mp3"); }

void MenuState::update(const sf::Time& dt)
{
    nce::AppState::update(dt);

    CheckImGuiPadBinds(getButtonActionChecker());

    ImGui::Begin("MenuButtons",
                 nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBackground
                     | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);

    if (m_mode == MenuMode::Default) {
        PushFancyFontBig();
        TextCentered("One Hit Wonder");
        ImGui::PopFont();
    }

    const auto position = (sf::Vector2f(getApp()->getRenderWindow()->getSize()) * 0.5f) - (mc::MENU_PANEL_SIZE * 0.5f);
    ImGui::SetWindowSize(mc::MENU_PANEL_SIZE);
    ImGui::SetWindowPos(position);

    PushFancyFont();
    switch (m_mode) {
    case MenuMode::Default:
        updateDefaultPage();
        break;
    case MenuMode::Play:
        updatePlayPage();
        break;
    case MenuMode::Options:
        updateOptionsPage();
        break;
    case MenuMode::Controls:
        updateControlsPage();
        break;
    case MenuMode::Stats:
        updateStatsPage();
        break;
    case MenuMode::Credits:
        updateCreditsPage();
        break;
    default:
        assert(false);
        break;
    }

    ImGui::PopFont();
    ImGui::End();
}

void MenuState::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (m_background)
        target.draw(*m_background, states);

    AppState::draw(target, states);
}

void MenuState::updateDefaultPage()
{
    CentreImGuiButton();
    if (ImGui::Button("Play", mc::MENU_BUTTON_SIZE)) {
#ifndef ONE_HIT_WONDER_DEBUG
        // If we've never killed boss 1, we don't enter the sub menu
        if (!GameMetrics::GetBossFightMetrics(gc::EVIL_EYE_INDEX).didKill) {
            setStateComplete();
            setNextStateID(gc::EVIL_EYE_STATE_NAME);
        } else {
            m_mode = MenuMode::Play;
        }
#else
        m_mode = MenuMode::Play;
#endif
    }

    CentreImGuiButton();
    if (ImGui::Button("Options", mc::MENU_BUTTON_SIZE))
        m_mode = MenuMode::Options;

    CentreImGuiButton();
    if (ImGui::Button("Controls", mc::MENU_BUTTON_SIZE))
        m_mode = MenuMode::Controls;

    CentreImGuiButton();
    ImGui::BeginDisabled(!GameMetrics::HasFoundExistingSave());
    if (ImGui::Button("Stats", mc::MENU_BUTTON_SIZE))
        m_mode = MenuMode::Stats;
    ImGui::EndDisabled();

    CentreImGuiButton();
    if (ImGui::Button("Credits", mc::MENU_BUTTON_SIZE))
        m_mode = MenuMode::Credits;

    CentreImGuiButton();
    if (ImGui::Button("Exit", mc::MENU_BUTTON_SIZE))
        getApp()->getRenderWindow()->close();
}

void MenuState::updatePlayPage()
{
    PushDetailFont();

    CentreImGuiButton();
    if (ImGui::Button(gc::BOSS_NAMES[0], mc::MENU_BUTTON_SIZE)) {
        setStateComplete();
        setNextStateID(gc::EVIL_EYE_STATE_NAME);
    }

    CentreImGuiButton();
    if (ImGui::Button(gc::BOSS_NAMES[1], mc::MENU_BUTTON_SIZE)) {
        setStateComplete();
        setNextStateID(gc::DEMONIC_HAMMER_STATE_NAME);
    }
#ifdef ONE_HIT_WONDER_DEBUG

    CentreImGuiButton();
    if (ImGui::Button(gc::BOSS_NAMES[2], mc::MENU_BUTTON_SIZE)) {
        setStateComplete();
        setNextStateID(gc::DANCING_PRINCES_STATE_NAME);
    }

    CentreImGuiButton();
    if (ImGui::Button(gc::BOSS_NAMES[3], mc::MENU_BUTTON_SIZE)) {
        setStateComplete();
        setNextStateID(gc::UNNAMED_STATE_NAME);
    }
#else
    if (GameMetrics::GetBossFightMetrics(gc::DEMONIC_HAMMER_INDEX).didKill) {
        CentreImGuiButton();
        if (ImGui::Button("Dancing Princes", mc::MENU_BUTTON_SIZE)) {
            setStateComplete();
            setNextStateID(gc::DANCING_PRINCES_STATE_NAME);
        }
    }
#endif
    ImGui::Separator();
    addBackButtonAndCheckEscape();
    ImGui::PopFont();
}

void MenuState::updateOptionsPage()
{
    PushDetailFont();

    // Display Mode
    const auto currentDisplayMode = m_app->getDisplayMode();

    if (ImGui::BeginCombo("Display Mode", nce::WindowDisplayModesStrings[size_t(currentDisplayMode)].c_str())) {
        for (size_t i { 0 }; i < size_t(nce::WindowDisplayModes::MAX); ++i) {
            if (ImGui::Selectable(nce::WindowDisplayModesStrings[i].c_str())) {
                m_app->setDisplayModeAndReconstruct(nce::WindowDisplayModes(i));
            }
        }
        ImGui::EndCombo();
    }

    ImGui::Separator();

    // Window resolution selection
    // TODO disable options when on display mode => windowed borderless
    const auto currentWindowSize { getApp()->getRenderWindow()->getSize() };
    static auto currentlySelected = currentWindowSize;
    std::vector<sf::Vector2u> resolutionOptions;

    const auto& supportedFullscreenModes = sf::VideoMode::getFullscreenModes();
    for (auto& res : supportedFullscreenModes) {
        const auto findResult { std::find(resolutionOptions.begin(), resolutionOptions.end(), res.size) };
        if (findResult != resolutionOptions.end())
            continue;

        if (res.size.x < uint32_t(getPlayAreaSize().x) || res.size.y < uint32_t(getPlayAreaSize().y))
            continue;

        resolutionOptions.push_back(res.size);
    }

    if (ImGui::BeginCombo("Resolution", fmt::format("{} x {}", currentlySelected.x, currentlySelected.y).c_str())) {
        for (const auto& res : resolutionOptions) {
            const auto asStr = fmt::format("{} x {}", res.x, res.y);
            if (ImGui::Selectable(asStr.c_str())) {
                currentlySelected = res;
                m_app->setDisplayModeAndReconstruct(m_app->getDisplayMode(), res);
            }
        }
        ImGui::EndCombo();
    }

    ImGui::Separator();

    int32_t volume { nce::SoundEventManager::GetMasterVolume() };
    if (ImGui::SliderInt("Master Volume", &volume, 0, 100)) {
        nce::SoundEventManager::SetMasterVolume(volume);
        m_app->setMasterVolumeConfig(volume);
    }

    ImGui::Separator();

    addBackButtonAndCheckEscape();

    ImGui::PopFont();
}

void MenuState::updateControlsPage()
{
    PushDetailFont();
    displayControlsMenu();

    ImGui::Separator();

    addBackButtonAndCheckEscape();
    ImGui::PopFont();
}

void MenuState::updateStatsPage()
{
    PushDetailFont();
    if (addBackButtonAndCheckEscape())
        m_statsMode = StatsMode::Player;

    if (ImGui::BeginTabBar("Sections")) {
        if (ImGui::TabItemButton("Player"))
            m_statsMode = StatsMode::Player;

        ImGui::BeginDisabled(!GameMetrics::GetBossFightMetrics(gc::EVIL_EYE_INDEX).didKill);
        if (ImGui::TabItemButton(gc::BOSS_NAMES[gc::EVIL_EYE_INDEX]))
            m_statsMode = StatsMode::EvilEye;
        ImGui::EndDisabled();

        ImGui::BeginDisabled(!GameMetrics::GetBossFightMetrics(gc::DEMONIC_HAMMER_INDEX).didKill);
        if (ImGui::TabItemButton(gc::BOSS_NAMES[gc::DEMONIC_HAMMER_INDEX]))
            m_statsMode = StatsMode::DemonicHammer;
        ImGui::EndDisabled();

        ImGui::BeginDisabled(!GameMetrics::GetBossFightMetrics(gc::DANCING_PRINCES_INDEX).didKill);
        if (ImGui::TabItemButton(gc::BOSS_NAMES[gc::DANCING_PRINCES_INDEX]))
            m_statsMode = StatsMode::DancingPrinces;
        ImGui::EndDisabled();

        // TODO: When we have added the fourth boss
        //  we need to also add a subpage here for it

        ImGui::EndTabBar();
    }

    // All bosses display the same info, we only differ on their index
    // this lambda allows us to simplify the code for this
    constexpr auto DisplayBossPage = [](size_t index) -> void {
        ImGui::Text(fmt::format("Attempts: ").data());
        ImGui::Text(fmt::format("{}", GameMetrics::GetBossFightMetrics(index).attemptsCount).data());
        ImGui::Separator();

        ImGui::Text(fmt::format("Kill Time: ").data());
        ImGui::Text(fmt::format("{} Seconds", GameMetrics::GetBossFightMetrics(index).killTime.asSeconds()).data());
        ImGui::Separator();

        ImGui::Text(fmt::format("Accuracy: ").data());
        ImGui::Text(fmt::format("{}%%", GameMetrics::GetBossFightMetrics(index).accuracy).data());
        ImGui::Separator();
    };

    switch (m_statsMode) {
    case StatsMode::Player: {
        float accuracy { 0.f };
        if (GameMetrics::GetPlayerMetrics().totalShotsFired != 0.f)
            accuracy = float(GameMetrics::GetPlayerMetrics().totalShotsOnTarget) / float(GameMetrics::GetPlayerMetrics().totalShotsFired);
        ImGui::Text(fmt::format("Total Shots Fired:").data());
        ImGui::Text(fmt::format("{}", GameMetrics::GetPlayerMetrics().totalShotsFired).data());
        ImGui::Separator();

        ImGui::Text(fmt::format("Total Shots On Target:").data());
        ImGui::Text(fmt::format("{}", GameMetrics::GetPlayerMetrics().totalShotsOnTarget).data());
        ImGui::Separator();

        ImGui::Text(fmt::format("Total Accuracy:").data());
        ImGui::Text(fmt::format("{}%%", accuracy * 100.f).data());
        ImGui::Separator();

        ImGui::Text(fmt::format("Total Deaths:").data());
        ImGui::Text(fmt::format("{}", GameMetrics::GetPlayerMetrics().totalDeaths).data());
        ImGui::Text("%");

    } break;
    case StatsMode::EvilEye:
        DisplayBossPage(gc::EVIL_EYE_INDEX);
        break;
    case StatsMode::DemonicHammer:
        DisplayBossPage(gc::DEMONIC_HAMMER_INDEX);
        break;
    case StatsMode::DancingPrinces:
        DisplayBossPage(gc::DANCING_PRINCES_INDEX);
        break;
    default:
        assert(false);
    }

    ImGui::PopFont();
}

void MenuState::updateCreditsPage()
{
    PushDetailFont();

    addBackButtonAndCheckEscape();
    TextCentered(
        R"(
       Programming
----------------------------
Mace (Lead)
Tom

          Design
----------------------------
Henrik (Lead)
Tom
Zack

          Audio
----------------------------
Danny Walker (Lead)

         Testing
----------------------------
Zack
        )");
    ImGui::PopFont();
}

bool MenuState::addBackButtonAndCheckEscape()
{
    CentreImGuiButton(true);
    if (ImGui::Button("Back", mc::MENU_BUTTON_SIZE_SMALL) || nce::InputHandler::KeyPressed(sf::Keyboard::Escape)) {
        m_mode = MenuMode::Default;
        return true;
    }

    return false;
}
