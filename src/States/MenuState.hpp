#pragma once

#include <nce/App.hpp>
#include <nce/AppState.hpp>

class MenuState : public nce::AppState {
public:
    MenuState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~MenuState();

    virtual void update(const sf::Time& dt) override;

private:
    /* Menu pages that can be displayed */
    enum class MenuMode : uint32_t { Default, Play, Options, Controls, Stats, Credits };

    /* SUbpages for the stats page */
    enum class StatsMode : uint32_t { Player, EvilEye, DemonicHammer, DancingPrinces };

    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    /* Update methods for each of the pages*/
    void updateDefaultPage();
    void updatePlayPage();
    void updateOptionsPage();
    void updateControlsPage();
    void updateStatsPage();
    void updateCreditsPage();

    /// <summary>
    /// Generates calls to create a pane centred "Back" button in
    /// ImGui.
    /// Used pages which require a return route to the 'Default' page.
    /// Also checks if escape was pressed, if it was it will return
    /// to the default page.
    /// </summary>
    /// <returns>bool - Was the back button was pressed</returns>
    [[maybe_unused]] bool addBackButtonAndCheckEscape();

    nce::App* m_app;
    MenuMode m_mode { MenuMode::Default };
    StatsMode m_statsMode { StatsMode::Player };

    std::optional<sf::Sprite> m_background;
};
