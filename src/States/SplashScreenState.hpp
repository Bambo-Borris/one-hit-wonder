#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <nce/AppState.hpp>

#include <deque>

class SplashScreenState : public nce::AppState {
public:
    SplashScreenState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~SplashScreenState();
    virtual void update(const sf::Time& dt) override;

protected:
    void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    std::optional<sf::Text> m_engineSplashText;
    sf::RectangleShape m_backgroundFill;
    std::deque<char> m_engineName;
    std::array<sf::Sound, 8> m_keyStrikeSounds;
    size_t m_keystrikeIndex { 0 };
};
