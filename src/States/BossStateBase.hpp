#pragma once

#include "../GameplayVars.hpp"
#include "../HUD/BowPullbackHUD.hpp"
#include "../HUD/PauseMenu.hpp"
#include "../Objects/ArrowProjectile.hpp"
#include "../Objects/BossProjectile.hpp"
#include "../Objects/Floor.hpp"
#include "../Objects/PlayerCharacter.hpp"

#include <nce/App.hpp>
#include <nce/AppState.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/HUDText.hpp>

#include <SFML/Window/Cursor.hpp>

class BossBase;

class BossStateBase : public nce::AppState {
public:
    enum class GameplayStage { FightingBoss, PlayerDied, BossDied };

    BossStateBase(std::string_view identifier, nce::App* app, const sf::Vector2f& playAreaSize, size_t bossIndex);
    virtual ~BossStateBase();

    virtual void update(const sf::Time& dt) override;

    [[nodiscard]] auto getSolver() -> nce::CollisionSolver& { return m_solver; }
    [[nodiscard]] auto getPlayAreaSize() -> sf::FloatRect
    {
        const auto& view = getApp()->getRenderTexture()->getView();
        return { view.getCenter() - view.getSize() / 2.f, view.getSize() };
    }

    [[nodiscard]] auto isPaused() const { return m_isPaused; }
    [[nodiscard]] auto getBossIndex() const { return m_bossIndex; }
    void addOnTargetShot() { ++m_shotsOnTarget; }

protected:
    // Special ui menu feautres here
    virtual void updateDeveloperUI(const sf::Time& dt) = 0;
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    void addBossHealthBar(BossBase* boss);
    void generateGenericDevMenu(NodeIdentifiers nodeId, const sf::Time& dt);

    // Fight specific update sequence logic here
    virtual void updateBossFight(const sf::Time& dt);
    virtual void debugKillBoss() = 0;
    virtual void setGameplayStage(GameplayStage stage);

    void setBossDeathText(std::string_view str);

    [[nodiscard]] virtual int32_t getBossHealth() const = 0;
    [[nodiscard]] auto getPlayer() -> PlayerCharacter* { return getObjectOfType<PlayerCharacter>(); }
    [[nodiscard]] auto getFloor() -> Floor* { return getObjectOfType<Floor>(); }
    [[nodiscard]] auto isInvincible() const -> bool { return m_invincibilityMode; }
    [[nodiscard]] auto getGameplayStage() const -> GameplayStage { return m_currentStage; }

    std::vector<ArrowProjectile*> m_arrowProjectiles;
    std::vector<BossProjectile*> m_bossProjectiles;

private:
    void bindControls();
    void registerSoundEvents();
    void setupHUD();
    // Stage specific update sequences that are currently
    // not required to be customisable (this may change in
    // future)
    void updatePlayerDeath();
    void updateBossDeath();

    /// <summary>
    /// We need to be able to hop between the standard mouse icon
    /// for menus, and a crosshair cursor when in a boss fight.
    /// This function handles this transition for the user.
    /// </summary>
    /// <param name="useDefault">Whether we should set the cursor to the default mouse</param>
    void changeCursor(bool useDefault);

    std::vector<sf::RectangleShape> m_debugColliderShapes;
    sf::Image m_crosshairCursor;
    sf::Cursor m_cursor;
    PauseMenu m_pauseMenu;

    nce::HUDText* m_playerDeathText;
    nce::HUDText* m_bossDeathText;
    nce::HUDText* m_bossContinueText;
    BowPullbackHUD* m_bowPullbackHUD;

    std::optional<sf::Text> m_continueBossSuccessText;
    std::optional<sf::Sprite> m_background;

    std::string m_fightTimerName;
    nce::CollisionSolver m_solver;

    GameplayStage m_currentStage { GameplayStage::FightingBoss };

    sf::Clock m_fadeInTimer;

    size_t m_bossIndex;
    unsigned int m_attemptCount { 1 };
    unsigned int m_shotsOnTarget { 0 };

    bool m_invincibilityMode { false }; // Developer menu option only
    bool m_isPaused { false };
    static bool m_showDeveloperUI;
};
