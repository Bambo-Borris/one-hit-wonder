#pragma once

#include "BossStateBase.hpp"

class EvilEyeFightState : public BossStateBase {
public:
    EvilEyeFightState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~EvilEyeFightState();

    virtual void update(const sf::Time& dt) override;

protected:
    virtual void debugKillBoss() override;
    virtual int32_t getBossHealth() const override;
    virtual void setGameplayStage(GameplayStage stage) override;
    virtual void updateDeveloperUI(const sf::Time& dt) override;

private:
};
