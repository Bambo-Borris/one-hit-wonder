#include "DemonicHammerFightState.hpp"
#include "../GameplayConstants.hpp"
#include "../Objects/BossProjectile.hpp"
#include "../Objects/Explosion.hpp"

#include <spdlog/spdlog.h>

constexpr std::size_t NAIL_PROJECTILE_COUNT { 60 }; // Boss cached projectile
constexpr std::size_t FALLING_RAIN_PROJECTILE_COUNT { 50 }; // Boss cached projectile

DemonicHammerFightState::DemonicHammerFightState(nce::App* app, const sf::Vector2f& playAreaSize)
    : BossStateBase(gc::DEMONIC_HAMMER_STATE_NAME, app, playAreaSize, gc::DEMONIC_HAMMER_INDEX)
{
    for (size_t i { 0 }; i < NAIL_PROJECTILE_COUNT; ++i) {
        m_bossProjectiles.push_back(new BossProjectile(this));
        m_bossProjectiles.back()->setActive(false);
        m_bossProjectiles.back()->setProjectileType(BossProjectile::ProjectileType::FlameBarrage);
        if (!addGameObject(m_bossProjectiles.back()))
            throw std::runtime_error("Unable to create nail projectile");
    }

    for (size_t i { 0 }; i < FALLING_RAIN_PROJECTILE_COUNT; ++i) {
        m_bossProjectiles.push_back(new BossProjectile(this));
        m_bossProjectiles.back()->setActive(false);
        m_bossProjectiles.back()->setProjectileType(BossProjectile::ProjectileType::Bomb);
        if (!addGameObject(m_bossProjectiles.back()))
            throw std::runtime_error("Unable to create falling rain projectile");
    }

    if (!addGameObject(new DemonicHammer(this, getPlayer(), playAreaSize, m_bossProjectiles)))
        throw std::runtime_error("Unable to create demonic hammer");
    setBossDeathText(fmt::format("{} has been slain!", gc::BOSS_NAMES[gc::DEMONIC_HAMMER_INDEX]));
    addBossHealthBar(getObjectOfType<DemonicHammer>());

    getSfxManager().playMusic("bin/music/menu_music.mp3");
}

DemonicHammerFightState::~DemonicHammerFightState() { getSfxManager().stopMusic("bin/music/menu_music.mp3"); }

void DemonicHammerFightState::update(const sf::Time& dt)
{
    BossStateBase::update(dt);
    if (BossStateBase::isPaused())
        return;

    if (getGameplayStage() != GameplayStage::PlayerDied) {
        auto player { getPlayer() };
        if (player->wasPlayerHit()) {
            setGameplayStage(GameplayStage::PlayerDied);
        }
    }

    // Check for completed explosions
    auto explosions { getAllObjectsOfType<Explosion>() };

    for (auto& e : explosions) {
        if (e->isActive())
            continue;

        removeGameObject(e);
    }
}

void DemonicHammerFightState::debugKillBoss() { getObjectOfType<DemonicHammer>()->debugSlay(); }

int32_t DemonicHammerFightState::getBossHealth() const { return getObjectOfType<DemonicHammer>()->getHealth(); }

void DemonicHammerFightState::setGameplayStage(GameplayStage stage)
{
    switch (stage) {
    case GameplayStage::FightingBoss: {
        auto boss = getObjectOfType<DemonicHammer>();
        auto explosions { getAllObjectsOfType<Explosion>() };
        for (auto& e : explosions)
            removeGameObject(e);
        assert(boss);
        boss->reset();
    } break;
    case GameplayStage::BossDied:
        setNextStateID(gc::DANCING_PRINCES_STATE_NAME);
        break;
    case GameplayStage::PlayerDied:
        break;
    default:
        assert(false);
        break;
    }

    BossStateBase::setGameplayStage(stage);
}

void DemonicHammerFightState::updateDeveloperUI(const sf::Time& dt) { generateGenericDevMenu(NodeIdentifiers::BossesDemonicHammer, dt); }
