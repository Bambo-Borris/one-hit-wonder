#pragma once

#include "BossStateBase.hpp"

#include <array>
#include <nce/TweeningAnimation.hpp>

class UndergroundSpike;
class CameraShake;

class DancingPrinceFightState : public BossStateBase {
public:
    DancingPrinceFightState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~DancingPrinceFightState();

    virtual void update(const sf::Time& dt) override;

protected:
    // Inherited via BossStateBase
    virtual void updateDeveloperUI(const sf::Time& dt) override;
    virtual void debugKillBoss() override;
    virtual int32_t getBossHealth() const override;
    virtual void setGameplayStage(GameplayStage stage) override;

private:
    enum BossShieldSwapStages { Default, LowerAndSwap, Wait, Raise };
    void updateState(const sf::Time& dt);
    void updateZoomOut();
    void playSpikeWave(bool isRaise, bool leftToRight = true);

    const sf::Vector2f m_viewMax;
    sf::View m_cachedView;
    std::unique_ptr<nce::TweeningAnimation<float>> m_zoomOutTween;

    void resetSpikes();

    std::array<sf::FloatRect, 3> m_levelRegions;
    std::vector<UndergroundSpike*> m_undergroundSpikes;
    std::unique_ptr<CameraShake> m_cameraShake;
    UndergroundSpike* m_lastSpike { nullptr };
    BossShieldSwapStages m_shieldSwapStages { BossShieldSwapStages::Default };
    bool m_isPlayingSpikeWave { false };
};
