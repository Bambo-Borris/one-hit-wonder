#include "EvilEyeFightState.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../Objects/ArcLaser.hpp"
#include "../Objects/EvilEye.hpp"
#include "../Objects/PlayerCharacter.hpp"

constexpr std::size_t BOSS_PROJECTILE_COUNT { 20 }; // Boss cached projectile

EvilEyeFightState::EvilEyeFightState(nce::App* app, const sf::Vector2f& playAreaSize)
    : BossStateBase(gc::EVIL_EYE_STATE_NAME, app, playAreaSize, gc::EVIL_EYE_INDEX)
{
    m_bossProjectiles.resize(BOSS_PROJECTILE_COUNT);
    for (auto& b : m_bossProjectiles) {
        b = new BossProjectile(this);
        b->setActive(false);
        if (!addGameObject(b))
            throw std::runtime_error("Unable to create game object");
    }

    if (!addGameObject(new ArcLaser(this)))
        throw std::runtime_error("Unable to create arc laser");

    if (!addGameObject(new EvilEye(this, getPlayer(), getObjectOfType<ArcLaser>(), m_bossProjectiles, m_arrowProjectiles, playAreaSize)))
        throw std::runtime_error("Unable to create evil eye");

    getObjectOfType<ArcLaser>()->setActive(false);
    setBossDeathText("The Evil Eye has been slain!");
    addBossHealthBar(getObjectOfType<EvilEye>());

    getSfxManager().playMusic("bin/music/menu_music.mp3");
}

EvilEyeFightState::~EvilEyeFightState() { getSfxManager().stopMusic("bin/music/menu_music.mp3"); }

void EvilEyeFightState::update(const sf::Time& dt)
{
    BossStateBase::update(dt);
    if (BossStateBase::isPaused())
        return;

    if (getGameplayStage() != GameplayStage::PlayerDied) {
        auto player { getPlayer() };
        if (player->wasPlayerHit())
            setGameplayStage(GameplayStage::PlayerDied);
    }
}

void EvilEyeFightState::debugKillBoss() { getObjectOfType<EvilEye>()->debugSlay(); }

int32_t EvilEyeFightState::getBossHealth() const { return getObjectOfType<EvilEye>()->getHealth(); }

void EvilEyeFightState::setGameplayStage(GameplayStage stage)
{
    switch (stage) {
    case GameplayStage::FightingBoss: {
        auto eye { getObjectOfType<EvilEye>() };
        eye->reset();

        auto arc { getObjectOfType<ArcLaser>() };
        arc->reset();
        arc->setActive(false);
    } break;
    case GameplayStage::PlayerDied:
        break;
    case GameplayStage::BossDied:
        setNextStateID(gc::DEMONIC_HAMMER_STATE_NAME);
    default:
        break;
    }

    BossStateBase::setGameplayStage(stage);
}

void EvilEyeFightState::updateDeveloperUI(const sf::Time& dt) { BossStateBase::generateGenericDevMenu(NodeIdentifiers::BossesEvilEye, dt); }
