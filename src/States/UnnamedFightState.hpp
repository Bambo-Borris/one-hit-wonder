#pragma once

#include "../Abilities/RadialAbility.hpp"
#include "BossStateBase.hpp"

class UnnamedFightState : public BossStateBase {
public:
    UnnamedFightState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~UnnamedFightState();

    virtual void update(const sf::Time& dt) override;

protected:
    // Inherited via BossStateBase
    virtual void debugKillBoss() override;
    virtual int32_t getBossHealth() const override;
    virtual void setGameplayStage(GameplayStage stage) override;
    virtual void updateDeveloperUI(const sf::Time& dt) override;

private:
    sf::View m_cachedView;
    std::optional<RadialAbility> m_radAbility;
    bool moveWithMouse { false };
};