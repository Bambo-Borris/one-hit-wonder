#pragma once

#include "../Objects/DemonicHammer.hpp"
#include "BossStateBase.hpp"

class DemonicHammerFightState : public BossStateBase {
public:
    DemonicHammerFightState(nce::App* app, const sf::Vector2f& playAreaSize);
    ~DemonicHammerFightState();

    virtual void update(const sf::Time& dt) override;

protected:
    // Inherited via BossStateBase
    virtual void debugKillBoss() override;
    virtual int32_t getBossHealth() const override;
    virtual void setGameplayStage(GameplayStage stage) override;
    virtual void updateDeveloperUI(const sf::Time& dt) override;

private:
};
