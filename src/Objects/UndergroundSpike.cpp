#include "UndergroundSpike.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/App.hpp>
#include <nce/AppState.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/NCE.hpp>
#include <nce/TimerController.hpp>

UndergroundSpike::UndergroundSpike(nce::AppState* currentState, const sf::FloatRect& region, float xPosition)
    : nce::GameObject(currentState)
    , m_region(region)
    , m_uuid(nce::GenerateUUID())
    , m_delayRaiseTimerName(fmt::format("spike_raise_delay_{}", m_uuid))
    , m_delayLowerTimerName(fmt::format("spike_lower_delay_{}", m_uuid))
    , m_holdTimerName(fmt::format("spike_hold_{}", m_uuid))
    , m_raiseTween(nce::AnimationCurve::Lerp, fmt::format("spike_raise_{}", m_uuid), {}, {}, sf::seconds(0.25f), &getAppState()->getTimerController())
    , m_lowerTween(nce::AnimationCurve::Lerp, fmt::format("spike_lower_{}", m_uuid), {}, {}, sf::seconds(0.25f), &getAppState()->getTimerController())
    , m_xPosition(xPosition)
{
    setIdentifier("underground_spike");
    m_shape.setSize(UNDERGROUND_SPIKE_SIZE);
    m_shape.setOrigin(UNDERGROUND_SPIKE_SIZE * 0.5f);
    m_shape.setPosition({ xPosition + UNDERGROUND_SPIKE_SIZE.x / 2.f, 100.f });

    auto bossState = dynamic_cast<BossStateBase*>(currentState);
    assert(bossState);
    if (!bossState)
        throw std::runtime_error("Unable to access BossState");

    auto& solver = bossState->getSolver();
    auto collider { nce::MakeOBBCollider(UNDERGROUND_SPIKE_SIZE * 0.5f, &m_shape, this) };
    if (!solver.addCollider(collider))
        throw std::runtime_error("Unable to add collider to solver");

    auto texture { nce::AssetHolder::get().getTexture("bin/textures/underground_spike.png") };
    assert(texture);
    if (!texture)
        throw std::runtime_error("Unable to load underground spike texture");

    m_shape.setTexture(texture);
    setRenderLayer(4);
    reset();
}

UndergroundSpike::~UndergroundSpike() { }

void UndergroundSpike::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }

void UndergroundSpike::setupRaise()
{
    const auto& v { getAppState()->getApp()->getRenderTexture()->getView() };
    m_phase = AnimationPhase::Raise;
    // Setup raise tween
    m_raiseTween.stop();
    m_raiseTween.setStart(m_shape.getPosition());
    m_raiseTween.setEnd({ m_shape.getPosition().x, (v.getCenter().y + v.getSize().y / 2.f) - (m_shape.getSize().y / 2.f) });

    m_shape.setPosition(m_raiseTween.getTweenedValue());
    m_raiseTween.play();
}

void UndergroundSpike::setupLower()
{
    m_phase = AnimationPhase::Lower;

    // We must've gone through a raise to get to a lower
    // so we'll rely on the start/end of the raise tween
    // to determine our start/end!
    m_lowerTween.stop();
    m_lowerTween.setStart(m_raiseTween.getEnd());
    m_lowerTween.setEnd(m_raiseTween.getStart());

    m_shape.setPosition(m_lowerTween.getTweenedValue());
    m_lowerTween.play();
}

void UndergroundSpike::update(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    auto& timerController { getAppState()->getTimerController() };
    switch (m_phase) {
    case AnimationPhase::Idle:
        if (timerController.exists(m_delayRaiseTimerName)) {
            if (timerController.getElapsed(m_delayRaiseTimerName) > m_raiseDelay) {
                setupRaise();
            }
        } else if (timerController.exists(m_delayLowerTimerName)) {
            if (timerController.getElapsed(m_delayLowerTimerName) > m_lowerDelay) {
                setupLower();
            }
        }
        break;
    case AnimationPhase::Raise:
        if (!m_raiseTween.isComplete()) {
            m_shape.setPosition(m_raiseTween.getTweenedValue());
        } else {
            m_lowerTween.stop();
            m_phase = AnimationPhase::Idle;
            if (timerController.exists(m_delayRaiseTimerName))
                timerController.removeTimer(m_delayRaiseTimerName);
            m_raiseDelay = sf::Time::Zero;
        }
        break;
    case AnimationPhase::Lower:
        if (!m_lowerTween.isComplete()) {
            m_shape.setPosition(m_lowerTween.getTweenedValue());
        } else {
            m_lowerTween.stop();
            m_phase = AnimationPhase::Idle;
            if (timerController.exists(m_delayLowerTimerName))
                timerController.removeTimer(m_delayLowerTimerName);
            m_lowerDelay = sf::Time::Zero;
        }
        break;
    default:
        assert(false);
        break;
    }
}

void UndergroundSpike::reset()
{
    auto appState = getAppState();
    const auto& v = appState->getApp()->getRenderTexture()->getView();
    const auto offset = (v.getCenter().y + v.getSize().y / 2.f) + (UNDERGROUND_SPIKE_SIZE.y / 2.f);
    m_shape.setPosition({ m_xPosition + (UNDERGROUND_SPIKE_SIZE.x / 2.f), offset });
    m_phase = AnimationPhase::Idle;
    m_raiseDelay = sf::Time::Zero;

    auto& timerController { getAppState()->getTimerController() };
    if (timerController.exists(m_delayRaiseTimerName))
        timerController.removeTimer(m_delayRaiseTimerName);

    if (timerController.exists(m_holdTimerName))
        timerController.removeTimer(m_holdTimerName);
}

void UndergroundSpike::startRaise(const sf::Time& delay)
{
    // If there's no delay, we'll begin the sequence straight away
    // otherwise we'll construct & start a timer and store the delay
    m_raiseDelay = delay;
    if (delay != sf::Time::Zero) {
        getAppState()->getTimerController().addTimer(m_delayRaiseTimerName);
    } else {
        setupRaise();
    }
}

void UndergroundSpike::startLower(const sf::Time& delay)
{
    m_lowerDelay = delay;
    if (delay != sf::Time::Zero) {
        getAppState()->getTimerController().addTimer(m_delayLowerTimerName);
    } else {
        setupLower();
    }
}
