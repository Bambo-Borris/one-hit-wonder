#pragma once

#include <nce/CollisionSolver.hpp>
#include <nce/GameObject.hpp>

class SplittableProjectile : public nce::GameObject {
public:
    SplittableProjectile(nce::AppState* currentState);

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    void fire(const sf::Vector2f& direction, const sf::Vector2f& position);

private:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go);
    void handleSplitCollisions(const nce::Manifold& manifold, nce::GameObject* const go);

    std::array<sf::CircleShape, 2> m_splitShapes;
    std::array<sf::Vector2f, 2> m_splitDirections;
    std::array<bool, 2> m_splitActivity;

    sf::CircleShape m_shape;
    nce::CollisionSolver::Callback m_cbWrapper;
    nce::CollisionSolver::Callback m_cbWrapperSplit;
    std::weak_ptr<nce::ColliderInfo> m_colliderInfo;
    sf::Vector2f m_direction;
    bool m_split { false };
};
