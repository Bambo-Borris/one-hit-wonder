#pragma once

#include <nce/CollisionSolver.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/GameObject.hpp>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/System/Time.hpp>

class ArrowProjectile : public nce::GameObject {
public:
    ArrowProjectile(nce::AppState* currentState, const sf::Vector2f& playArea);

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    void fire(const sf::Vector2f& position, const sf::Vector2f& direction, float speed, int32_t damage);
    [[nodiscard]] auto getAliveTime() const -> sf::Time { return m_aliveTimer.getElapsedTime(); }
    [[nodiscard]] auto getPosition() const -> sf::Vector2f { return m_shape.getPosition(); }
    [[nodiscard]] auto getArrowDamage() const -> int32_t { return m_arrowDamage; }

protected:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    struct TrailQuad {
        sf::Vector2f position { 0.f, 0.f };
        sf::Time aliveTime { sf::Time::Zero };
    };

    void updateTrail(const sf::Time& dt);
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go);
    nce::CollisionSolver::Callback m_cbWrapper;

    sf::Vector2f m_playAreaSize;
    sf::RectangleShape m_shape;
    sf::Vector2f m_velocity;
    sf::Clock m_aliveTimer;

    sf::VertexArray m_trailParticles;
    std::vector<std::optional<TrailQuad>> m_trailQuads;

    std::size_t m_trailIndex { 0 };
    sf::Clock m_trailEmitClock;

    sf::Colour m_trailColour;
    int32_t m_arrowDamage { 1 };
};
