#include "ArrowProjectile.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "../Utils/GameMetrics.hpp"
#include "BossBase.hpp"

#include <nce/AppState.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/RNG.hpp>
#include <spdlog/spdlog.h>

constexpr auto PROJECTILE_SIZE { sf::Vector2f { 16.f, 8.f } };
constexpr auto TRAIL_PARTICLE_QUAD_SIZE { sf::Vector2f { 2.f, 2.f } };
constexpr auto MAX_TRAIL_QUADS { 12 };
constexpr auto TRAIL_QUAD_LIFETIME { sf::seconds(1.f) };
constexpr sf::Color PROJECTILE_COLOUR { 224, 219, 209 };
constexpr sf::Time TRAIL_EMIT_TIME { sf::seconds(0.1f) };

ArrowProjectile::ArrowProjectile(nce::AppState* currentState, const sf::Vector2f& playArea)
    : GameObject(currentState)
    , m_cbWrapper(std::bind(&ArrowProjectile::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
    , m_playAreaSize(playArea)
    , m_shape(PROJECTILE_SIZE)
{
    setIdentifier("arrow_projectile");

    m_shape.setOrigin(PROJECTILE_SIZE * 0.5f);
    m_shape.setFillColor(PROJECTILE_COLOUR);
    m_shape.setPosition({ 100, 100 });

    auto texture = nce::AssetHolder::get().getTexture("bin/textures/arrow.png");
    if (!texture->generateMipmap())
        throw std::runtime_error("Unable to generate mip map for arrow texture");
    m_shape.setTexture(texture, true);

    m_trailColour = nce::GenerateRandomColour();

    auto colliderInfo { nce::MakeOBBCollider(PROJECTILE_SIZE * 0.5f, &m_shape, this) };

    auto bossState { dynamic_cast<BossStateBase*>(currentState) };
    if (bossState) {
        auto& solver = bossState->getSolver();
        if (!solver.addCollider(colliderInfo))
            throw std::runtime_error("Unable to add arrow collider");

        if (!solver.subscribeNotification({ colliderInfo, &m_cbWrapper }))
            throw std::runtime_error("Unable to subscribe for collision callback");
    }

    m_trailQuads.resize(MAX_TRAIL_QUADS);
    m_trailParticles.setPrimitiveType(sf::PrimitiveType::Triangles);
}

void ArrowProjectile::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    target.draw(m_shape, states);
    target.draw(m_trailParticles, states);
}

void ArrowProjectile::updateTrail(const sf::Time& dt)
{
    // Update the trail quads that are currently active (tick the alive time
    // and reset ones that are too old)
    for (auto& q : m_trailQuads) {
        if (!q)
            continue;

        if (q->aliveTime > TRAIL_QUAD_LIFETIME)
            q.reset();
        else
            q->aliveTime += dt;
    }

    // Now we check if we should emit a new trail quad
    const auto getFreeTrailQuad = [](std::vector<std::optional<TrailQuad>>& quads) -> size_t {
        for (size_t i { 0 }; i < quads.size(); ++i) {
            auto& q = quads[i];
            if (!q) {
                q = TrailQuad {};
                return i;
            }

            if (q->aliveTime > TRAIL_QUAD_LIFETIME)
                return i;
        }
        return quads.size();
    };

    if (m_trailEmitClock.getElapsedTime() >= TRAIL_EMIT_TIME) {
        if (auto quadIdx = getFreeTrailQuad(m_trailQuads); quadIdx != m_trailQuads.size()) {
            assert(m_trailQuads[quadIdx]);
            m_trailQuads[quadIdx]->position = m_shape.getPosition();
            m_trailEmitClock.restart();
        } else {
            assert(false);
        }
    }

    // We've done all updating & checking so we'll setup the
    // vertices to draw the trail
    m_trailParticles.clear();
    for (const auto& trailQuad : m_trailQuads) {
        if (!trailQuad)
            continue;
        const auto oldSize { m_trailParticles.getVertexCount() };
        m_trailParticles.resize(oldSize + 6);

        auto const quad = &m_trailParticles[oldSize];
        const auto halfSize = TRAIL_PARTICLE_QUAD_SIZE * 0.5f;
        const auto& centre = trailQuad->position;

        quad[0].position = centre - halfSize;
        quad[1].position = centre + sf::Vector2f { halfSize.x, -halfSize.y };
        quad[2].position = centre + halfSize;

        quad[3].position = quad[2].position;
        quad[4].position = centre + sf::Vector2f { -halfSize.x, halfSize.y };
        quad[5].position = quad[0].position;

        for (std::size_t i { 0 }; i < 6; ++i)
            quad[1].color = m_trailColour;
    }
}

void ArrowProjectile::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    NCE_UNUSED(manifold);
    auto const boss = dynamic_cast<BossBase*>(go);
    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    if (!bossState)
        throw std::runtime_error("Unable to access boss state for ArrowProjectile::handleCollisions");

    if (go->getIdentifier() == "floor") {
        setActive(false);
        bossState->getSfxManager().triggerSoundEvent("arrow_impact_floor");
    } else if (boss) {
        setActive(false);
        bossState->getSfxManager().triggerSoundEvent("arrow_impact_boss");
        bossState->addOnTargetShot();
        GameMetrics::AddPlayerTotalShotsOnTarget();
    }
}

void ArrowProjectile::update(const sf::Time& dt)
{
    auto bossState = dynamic_cast<BossStateBase*>(getAppState());
    assert(bossState);
    if (!bossState->getPlayAreaSize().findIntersection(m_shape.getGlobalBounds())) {
        setActive(false);
    }

    const auto gravity = getWorldVectorVar("gravity");

    m_velocity += gravity * dt.asSeconds();
    m_shape.move(m_velocity * dt.asSeconds());
    m_shape.setRotation(m_velocity.angle());

    updateTrail(dt);
}

void ArrowProjectile::fire(const sf::Vector2f& position, const sf::Vector2f& direction, float speed, int32_t damage)
{
    m_shape.setPosition(position);
    if (speed == 0.f)
        speed = 1.f;

    m_velocity = direction * speed;
    setActive(true);
    m_aliveTimer.restart();
    m_trailEmitClock.restart();
    m_trailParticles.clear();
    for (auto& q : m_trailQuads)
        q.reset();
    m_trailColour = nce::GenerateRandomColour();
    m_arrowDamage = damage;
    m_shape.setRotation(m_velocity.angle());
}
