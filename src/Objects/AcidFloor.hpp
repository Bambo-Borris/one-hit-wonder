#pragma once

#include <nce/AppState.hpp>
#include <nce/Collisions.hpp>
#include <nce/GameObject.hpp>
#include <nce/TimerController.hpp>

class AcidFloor : public nce::GameObject {
public:
    AcidFloor(nce::AppState* currentState, sf::Vector2f position);
    ~AcidFloor();

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    [[nodiscard]] auto hasTimedOut() const -> bool { return getAppState()->getTimerController().getElapsed(m_timerName) > sf::seconds(5.f); }

private:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    sf::RectangleShape m_shape;
    std::string m_timerName;
    std::shared_ptr<nce::ColliderInfo> m_colliderInfo;
};
