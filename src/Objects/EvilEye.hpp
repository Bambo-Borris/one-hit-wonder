#pragma once

#include "BossBase.hpp"

#include <nce/AnimatedSprite.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/GameObject.hpp>
#include <nce/TweeningAnimation.hpp>

#include <SFML/Graphics/RectangleShape.hpp>

class BossProjectile;
class ArrowProjectile;
class ArcLaser;

class EvilEye : public BossBase {
public:
    EvilEye(nce::AppState* currentState,
            PlayerCharacter* player,
            ArcLaser* arc,
            const std::vector<BossProjectile*>& projectiles,
            const std::vector<ArrowProjectile*>& arrows,
            const sf::Vector2f& playAreaSize);
    ~EvilEye();

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT)
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    virtual void reset() override;

protected:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;
    virtual sf::Transformable* getTransformable() override { return &m_sprite; }

private:
    enum class ActivityState { Hovering, Dodging, Shooting, ArcLaserFiring };

    void setupAnimations();
    void updateHovering(const sf::Time& dt);
    void updateDodging(const sf::Time& dt);
    void updateShooting(const sf::Time& dt);
    void updateArcLaser(const sf::Time& dt);
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go) override;

    nce::CollisionSolver::Callback m_cbWrapper;
    std::unique_ptr<nce::TAFloat> m_dodgeAnimation { nullptr };
    std::weak_ptr<nce::ColliderInfo> m_colliderInfo;

    nce::AnimatedSprite m_sprite;
    nce::Animation m_hoverAnimation;

    ArcLaser* m_arcLaser;
    sf::Vector2f m_playAreaSize;
    sf::Time m_fireCooldownMax;

    std::vector<ArrowProjectile*> m_arrows;

    float m_verticalOffset { 0.f };
    float m_dodgeDirection { 0.f };
    float m_dodgeTargetX { 0.f };
    std::uint32_t m_framesOnLeft { 0 };
    std::uint32_t m_framesOnRight { 0 };
    bool m_hasFiredFirstShot { false };
    bool m_reachedMaxDodge { false };
    ActivityState m_currentState { ActivityState::Hovering };
};
