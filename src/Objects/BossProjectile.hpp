#pragma once

#include <nce/AnimatedSprite.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/GameObject.hpp>

#include <memory>

namespace sf {
class Shape;
}

class PlayerCharacter;

class BossProjectile : public nce::GameObject {
public:
    enum class ProjectileType : uint32_t { Flame, FlameBarrage, SeekingArrow, Bomb, LeafFall, Acid };

    BossProjectile(nce::AppState* currentState);

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    void fire(const sf::Vector2f& start, const sf::Vector2f& end, PlayerCharacter* player = nullptr);
    void reset();

    void setProjectileType(ProjectileType type);
    auto getSpeed() const -> float { return m_speed; }
    auto getType() const -> ProjectileType { return m_projectileType; }

protected:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    void constructAnimations();
    void setupProjectile();

    void fireStraightProjectile(const sf::Vector2f& start, const sf::Vector2f& end);
    void fireSeekingArrow(const sf::Vector2f& start, const sf::Vector2f& end, PlayerCharacter* player);
    void fireBomb(const sf::Vector2f& start, const sf::Vector2f& end);
    void fireLeafFall(const sf::Vector2f& start, const sf::Vector2f& end);
    void fireAcid(const sf::Vector2f& start, const sf::Vector2f& end);

    void updateFlame(const sf::Time& dt);
    void updateSeekingArrow(const sf::Time& dt);
    void updateBomb(const sf::Time& dt);
    void updateLeafFall(const sf::Time& dt);
    void updateAcid(const sf::Time& dt);
    void updateRotationFromVelocity();
    void handleCollisions(const nce::Manifold& manifold, GameObject* const go);

    void terminate();

    std::unordered_map<ProjectileType, nce::Animation> m_animations;
    std::unordered_map<ProjectileType, sf::Time> m_animationFrameTimes;

    std::shared_ptr<sf::Shape> m_shape;
    std::unique_ptr<nce::AnimatedSprite> m_sprite;

    nce::CollisionSolver::Callback m_cbWrapper;
    sf::Clock m_swayClock;
    PlayerCharacter* m_player { nullptr };
    sf::Vector2f m_velocity;
    sf::Vector2f m_direction;
    float m_speed { 0.f };
    ProjectileType m_projectileType { ProjectileType::Flame };
    std::shared_ptr<nce::ColliderInfo> m_colliderInfo;
};
