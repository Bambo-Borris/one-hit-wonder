#include "DancingPrinces.hpp"
#include "../GameplayConstants.hpp"
#include "../States/BossStateBase.hpp"
#include "../Utils/AIUtility.hpp"
#include "BossProjectile.hpp"
#include "PlayerCharacter.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/RNG.hpp>

constexpr auto DANCING_PRINCE_SPRITE_SIZE { sf::Vector2f { 128.f, 159.f } };
constexpr auto OFFSET_TOP_OF_SCREEN { 10.f };
// How long does it take us to move from point to point in our diamond formation
constexpr auto DANCING_PRINCES_MOVE_SPEED { 120.f };
constexpr auto MIN_DIST_FROM_TARGET_SQ { 0.9f };

constexpr auto LEAF_DROP_CD_TIMER { "prince_leaf_cd" };
constexpr auto LASER_FIRE_CD_TIMER { "prince_laser_cd" };

constexpr auto ACTIVE_BOSS_LEAF_CD { "active_leaf_cd" };
constexpr auto SHIELDED_BOSS_LEAF_CD { "shielded_leaf_cd" };

constexpr auto ACTIVE_BOSS_LASER_CD { "active_laser_cd" };

constexpr auto ACID_FIRE_CD { "acid_fire_cd" };

DancingPrinces::DancingPrinces(nce::AppState* currentState,
                               PlayerCharacter* player,
                               const std::vector<BossProjectile*>& projectiles,
                               const std::array<sf::FloatRect, 3>& regions)
    : BossBase(currentState, player, 300, projectiles, gc::BOSS_NAMES[gc::DANCING_PRINCES_INDEX])
    , m_regions(regions)
    , m_cbWrapperLeft(std::bind(&DancingPrinces::handleCollisionsLeftBoss, this, std::placeholders::_1, std::placeholders::_2))
    , m_cbWrapperRight(std::bind(&DancingPrinces::handleCollisionsRightBoss, this, std::placeholders::_1, std::placeholders::_2))
{
    setIdentifier("dancing_princes");

    auto texture { nce::AssetHolder::get().getTexture("bin/textures/1Vampiric Creatures Giant Mosquito.png") };
    assert(texture);
    if (!texture)
        throw std::runtime_error("Unable to load dancing princes boss texture");

    for (auto& s : m_bossShapes) {
        s = sf::RectangleShape(DANCING_PRINCE_SPRITE_SIZE);
        s.setOrigin(DANCING_PRINCE_SPRITE_SIZE / 2.f);
        s.setTexture(texture);
    }

    // We need three different types of projectile on this boss, laser & leaf fall & acid
    constexpr size_t leafProjCount { 25 };
    constexpr size_t laserProjCount { 65 };
    // constexpr size_t acidProjCount { 10 };
    for (size_t i { 0 }; i < leafProjCount; ++i)
        projectiles[i]->setProjectileType(BossProjectile::ProjectileType::LeafFall);

    for (size_t i { leafProjCount }; i < leafProjCount + laserProjCount; ++i)
        projectiles[i]->setProjectileType(BossProjectile::ProjectileType::Flame);

    for (size_t i { leafProjCount + laserProjCount }; i < projectiles.size(); ++i)
        projectiles[i]->setProjectileType(BossProjectile::ProjectileType::Acid);

    m_bossShield.setSize(DANCING_PRINCE_SPRITE_SIZE * 1.25f);
    m_bossShield.setOrigin(m_bossShield.getSize() * 0.5f);
    m_bossShield.setOutlineThickness(5.f);
    m_bossShield.setOutlineColor(sf::Color::Cyan);
    m_bossShield.setFillColor(sf::Color::Transparent);

    setupMoveableRegions();

    auto leftBossCollider { nce::MakeAABBCollider(DANCING_PRINCE_SPRITE_SIZE * 0.45f, &m_bossShapes[LEFT_BOSS_INDEX], this) };
    m_colliderInfo[LEFT_BOSS_INDEX] = leftBossCollider;
    auto rightBossCollider { nce::MakeAABBCollider(DANCING_PRINCE_SPRITE_SIZE * 0.45f, &m_bossShapes[RIGHT_BOSS_INDEX], this) };
    m_colliderInfo[RIGHT_BOSS_INDEX] = rightBossCollider;

    auto bossState = dynamic_cast<BossStateBase*>(currentState);
    assert(bossState);

    if (!bossState->getSolver().addCollider(leftBossCollider))
        throw std::runtime_error("Unable to add collider for dancing prince boss");

    if (!bossState->getSolver().subscribeNotification({ leftBossCollider, &m_cbWrapperLeft }))
        throw std::runtime_error("Unable to subscribe to collision events for dancing prince boss");

    if (!bossState->getSolver().addCollider(rightBossCollider))
        throw std::runtime_error("Unable to add collider for dancing prince boss");

    if (!bossState->getSolver().subscribeNotification({ rightBossCollider, &m_cbWrapperRight }))
        throw std::runtime_error("Unable to subscribe to collision events for dancing prince boss");

    m_debugLine.setPrimitiveType(sf::PrimitiveType::Lines);
    m_debugLine.resize(12);
    m_debugLine[0].color = sf::Color::Yellow;
    m_debugLine[1].color = sf::Color::Yellow;
    m_shotsOnTargetCount = 0;

    reset();

    m_leafSpawnPoints[0] = { -(DANCING_PRINCE_SPRITE_SIZE.x / 2.f), DANCING_PRINCE_SPRITE_SIZE.y / 2.f };
    m_leafSpawnPoints[1] = { 0.f, DANCING_PRINCE_SPRITE_SIZE.y / 2.f };
    m_leafSpawnPoints[2] = { DANCING_PRINCE_SPRITE_SIZE.x / 2.f, DANCING_PRINCE_SPRITE_SIZE.y / 2.f };
}

void DancingPrinces::swapShields()
{
    m_shotsOnTargetCount = 0;
    auto idx { getActiveBossIndex() };
    if (!idx)
        throw std::runtime_error("No active prince boss");

    m_bossStates[*idx] = BossState::Acid;
    m_bossStates[getOtherBossIndex(*idx)] = BossState::Active;

    // This will be used for the actual release version so we're not computing on every update.
    // gamevar - 1 because a projectile will be fired into the spike region to push the player
    // far enough into the middle that they trigger the spikes, although this could still be
    // avoided if the projectiles and area vars are high and low enough, so I'll cap the
    // projectile limit as well when I find where that is.
    /*if (m_acidSpacing == 0.f)
        m_acidSpacing = m_regions[0].width / (getBossesDancingPrincesFloatVar("acid_projectiles") - 1);*/

    m_acidSpacing = m_regions[0].width * 1.25f / getBossesDancingPrincesFloatVar("acid_projectiles");

    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    const auto bounds { bossState->getPlayAreaSize() };

    if ((*idx) == LEFT_BOSS_INDEX) {
        m_acidTarget = { bounds.left, bounds.top + bounds.height };
        m_acidDirection = 1.f;

    } else {
        m_acidTarget = { bounds.getPosition() + bounds.getSize() };
        m_acidDirection = -1.f;
    }
    m_acidTarget.y -= 1.f;

    m_firstAcidBomb = true;
    m_acidCounter = 0;
    getAppState()->getTimerController().resetTimer(ACID_FIRE_CD);
    getAppState()->getTimerController().playTimer(ACID_FIRE_CD);
}

void DancingPrinces::forcePhase1Swap() { m_shotsOnTargetCount = 8; }

void DancingPrinces::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    target.draw(m_bossShield, states);

    for (const auto& s : m_bossShapes)
        target.draw(s, states);

    target.draw(m_debugLine, states);
    BossBase::draw(target, states);
}

void DancingPrinces::update(const sf::Time& dt)
{
    BossBase::update(dt);

    if (!m_zoomOutFinished)
        return;

    switch (m_currentPhase) {
    case BossPhase::Phase1:
        updatePhase1(dt);
        break;
    case BossPhase::Phase2:
        updatePhase2(dt);
        break;
    }
}

void DancingPrinces::reset()
{
    BossBase::reset();

    m_currentPhase = BossPhase::Phase1;

    for (size_t i { 0 }; i < PRINCES_ENTITY_COUNT; ++i) {
        generateTargetPosition(i);
        m_bossStates[i] = BossState::Active;
    }

    const auto shieldedIndex { size_t(nce::RNG::intWithinRange(0, 1)) };
    m_bossStates[shieldedIndex] = BossState::Shielded;
    m_bossShield.setPosition(m_bossShapes[shieldedIndex].getPosition());

    // sort out timers
    constructOrResetTimer(ACTIVE_BOSS_LEAF_CD);
    constructOrResetTimer(SHIELDED_BOSS_LEAF_CD);
    constructOrResetTimer(ACTIVE_BOSS_LASER_CD);
    constructOrResetTimer(ACID_FIRE_CD);

    m_shotsOnTargetCount = 0;
}

void DancingPrinces::setZoomOutFinished() { m_zoomOutFinished = true; }

void DancingPrinces::generateTargetPosition(size_t index)
{
    assert(index >= 0 && index < PRINCES_ENTITY_COUNT);

    auto createPosition = [this, index]() -> void {
        m_moveTargets[index].x
            = nce::RNG::realWithinRange(m_moveableRegions[index].left + DANCING_PRINCE_SPRITE_SIZE.x / 2.f,
                                        (m_moveableRegions[index].left + m_moveableRegions[index].width) - DANCING_PRINCE_SPRITE_SIZE.x / 2.f);

        m_moveTargets[index].y
            = nce::RNG::realWithinRange(m_moveableRegions[index].top + DANCING_PRINCE_SPRITE_SIZE.y / 2.f,
                                        (m_moveableRegions[index].top + m_moveableRegions[index].height) - DANCING_PRINCE_SPRITE_SIZE.y / 2.f);
    };

    do {
        createPosition();
    } while ((m_bossShapes[index].getPosition() - m_moveTargets[index]).lengthSq() < 8.f);
}

void DancingPrinces::constructOrResetTimer(std::string_view name)
{
    if (getAppState()->getTimerController().exists(name)) {
        getAppState()->getTimerController().resetTimer(name);
        getAppState()->getTimerController().pauseTimer(name);
    } else {
        getAppState()->getTimerController().addTimer(name, true);
    }
}

void DancingPrinces::updatePhase1(const sf::Time& dt)
{
    for (size_t i { 0 }; i < PRINCES_ENTITY_COUNT; ++i) {
        const auto delta { (m_moveTargets[i] - m_bossShapes[i].getPosition()) };
        const auto distSq { delta.lengthSq() };
        if (distSq <= MIN_DIST_FROM_TARGET_SQ) {
            generateTargetPosition(i);
        }

        m_bossShapes[i].move(delta.normalized() * DANCING_PRINCES_MOVE_SPEED * dt.asSeconds());
        tickBoss(i, dt);
    }

    const auto idx { getActiveBossIndex() };
    assert(idx);
    if (idx) {
        m_bossShield.setPosition(m_bossShapes[getOtherBossIndex(*idx)].getPosition());
    }
}

void DancingPrinces::updatePhase2(const sf::Time& dt) { NCE_UNUSED(dt); }

void DancingPrinces::tickBoss(size_t index, const sf::Time& dt)
{
    assert(index >= 0 && index < PRINCES_ENTITY_COUNT);
    auto& boss { m_bossShapes[index] };
    NCE_UNUSED(boss);
    switch (m_bossStates[index]) {
        using enum BossState;
    case Active: {
        m_debugLine[0].position = boss.getPosition();
        // If the timer is paused, we only just started, if we've expired we should fire
        if (getAppState()->getTimerController().isPaused(ACTIVE_BOSS_LEAF_CD)
            || getAppState()->getTimerController().getElapsed(ACTIVE_BOSS_LEAF_CD) > sf::seconds(getBossesDancingPrincesFloatVar(LEAF_DROP_CD_TIMER))) {
            auto proj { getAvailableProjectile(BossProjectile::ProjectileType::LeafFall) };
            if (proj) {
                sf::Vector2f pos = boss.getPosition();
                changeLeafSpawnPosition(pos);
                proj->fire(pos, sf::Vector2f { pos.x, 5000.f });
                getAppState()->getTimerController().playTimer(ACTIVE_BOSS_LEAF_CD);
                getAppState()->getTimerController().resetTimer(ACTIVE_BOSS_LEAF_CD);
            }
        }

        if (getAppState()->getTimerController().getElapsed(ACTIVE_BOSS_LASER_CD) > sf::seconds(getBossesDancingPrincesFloatVar(LASER_FIRE_CD_TIMER))) {
            fireLaser(index);
            getAppState()->getTimerController().resetTimer(ACTIVE_BOSS_LASER_CD);
        } else if (getAppState()->getTimerController().isPaused(ACTIVE_BOSS_LASER_CD)) {
            getAppState()->getTimerController().playTimer(ACTIVE_BOSS_LASER_CD);
        }
        static int coolDown { 0 };
        if (coolDown < 15)
            ++coolDown;
        else {
            m_debugLine[1].position = getPredictedShotPosition(index);
            coolDown = 0;
        }

    } break;
    case Acid:
        if (m_acidCounter != getBossesDancingPrincesFloatVar("acid_projectiles"))
            handleAcid(index, dt);
        else
            m_bossStates[index] = BossState::Shielded;
        break;
    case Shielded: {

    } break;
    default:
        assert(false);
        break;
    }
}

void DancingPrinces::setupMoveableRegions()
{
    m_moveableRegions[LEFT_BOSS_INDEX].left = m_regions[0].getPosition().x;
    m_moveableRegions[LEFT_BOSS_INDEX].top = m_regions[0].getPosition().y + OFFSET_TOP_OF_SCREEN;
    m_moveableRegions[LEFT_BOSS_INDEX].width = m_regions[0].getSize().x;
    m_moveableRegions[LEFT_BOSS_INDEX].height = 30.f * OFFSET_TOP_OF_SCREEN;

    m_moveableRegions[RIGHT_BOSS_INDEX].left = m_regions[2].getPosition().x;
    m_moveableRegions[RIGHT_BOSS_INDEX].top = m_regions[2].getPosition().y + OFFSET_TOP_OF_SCREEN;
    m_moveableRegions[RIGHT_BOSS_INDEX].width = m_regions[2].getSize().x;
    m_moveableRegions[RIGHT_BOSS_INDEX].height = 30.f * OFFSET_TOP_OF_SCREEN;

    m_bossShapes[LEFT_BOSS_INDEX].setPosition(m_moveableRegions[LEFT_BOSS_INDEX].getPosition() + (m_moveableRegions[LEFT_BOSS_INDEX].getSize() * 0.5f));
    m_bossShapes[RIGHT_BOSS_INDEX].setPosition(m_moveableRegions[RIGHT_BOSS_INDEX].getPosition() + (m_moveableRegions[RIGHT_BOSS_INDEX].getSize() * 0.5f));
}

void DancingPrinces::handleCollisionsLeftBoss(const nce::Manifold& manifold, nce::GameObject* const go)
{
    generincHandleCollisions(LEFT_BOSS_INDEX, manifold, go);
}

void DancingPrinces::handleCollisionsRightBoss(const nce::Manifold& manifold, nce::GameObject* const go)
{
    generincHandleCollisions(RIGHT_BOSS_INDEX, manifold, go);
}

void DancingPrinces::generincHandleCollisions(size_t bossIndex, const nce::Manifold& manifold, nce::GameObject* const go)
{
    const auto idx { getActiveBossIndex() };
    assert(idx);

    const auto before { getHealth() };
    if ((*idx) == bossIndex)
        BossBase::handleCollisions(manifold, go);

    // If there was an on target shot
    if (before > getHealth()) {
        ++m_shotsOnTargetCount;
        spdlog::debug("Shots that hit dancing princes {}", m_shotsOnTargetCount);
    }
}

auto DancingPrinces::getActiveBossIndex() const -> std::optional<size_t>
{
    for (size_t i { 0 }; i < PRINCES_ENTITY_COUNT; ++i) {
        if (m_bossStates[i] == BossState::Active)
            return { i };
    }

    return std::nullopt;
}

auto DancingPrinces::getOtherBossIndex(size_t idx) const -> size_t { return (idx == LEFT_BOSS_INDEX) ? RIGHT_BOSS_INDEX : LEFT_BOSS_INDEX; }

void DancingPrinces::handleAcid(size_t idx, const sf::Time& dt)
{
    NCE_UNUSED(dt);
    const auto activeIdx { getActiveBossIndex() };
    assert(activeIdx);

    sf::FloatRect* rect { nullptr };

    if ((*activeIdx) == LEFT_BOSS_INDEX)
        rect = &m_regions[0];
    else
        rect = &m_regions[2];

    if (getAppState()->getTimerController().getElapsed(ACID_FIRE_CD) > sf::seconds(getBossesDancingPrincesFloatVar("acid_fire_cd")) || m_firstAcidBomb) {
        auto p { getAvailableProjectile(BossProjectile::ProjectileType::Acid) };
        assert(p);
        if (p) {
            if (m_firstAcidBomb)
                m_acidTarget.x += m_acidSpacing * m_acidDirection / 2.f;
            else
                m_acidTarget.x += m_acidSpacing * m_acidDirection;

            p->fire(m_bossShapes[idx].getPosition(), m_acidTarget);
            m_acidCounter++;
            getAppState()->getTimerController().resetTimer(ACID_FIRE_CD);
            m_firstAcidBomb = false;
        }
    }
}

sf::Vector2f DancingPrinces::getPredictedShotPosition(size_t bossIndex)
{
    const auto speed { getBossesDancingPrincesFloatVar("laser_speed") };
    const auto playerMoveSpeed = getPlayerFloatVar("max_move_speed");
    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);

    if (!bossState)
        throw std::runtime_error("Unable to access boss state when firing Dancing Prince laser");

    std::pair<float, float> boundsMinMax;

    boundsMinMax.first = m_moveableRegions[bossIndex].left;
    boundsMinMax.second = m_moveableRegions[bossIndex].left + m_moveableRegions[bossIndex].width;

    return getLinearShotPredictedEndPoint(getAppState()->getTimerController(),
                                          speed,
                                          m_bossShapes[bossIndex].getPosition(),
                                          getPlayer()->getPosition(),
                                          getPlayer()->getXDirection(),
                                          playerMoveSpeed,
                                          getPlayer()->getDirectionChangeCounter(),
                                          boundsMinMax);
}

void DancingPrinces::fireLaser(size_t bossIndex)
{
    auto& boss { m_bossShapes[bossIndex] };
    auto proj { getAvailableProjectile(BossProjectile::ProjectileType::Flame) };
    assert(proj);
    if (proj) {
        const auto projectileEndPoint { getPredictedShotPosition(bossIndex) };
        proj->fire(boss.getPosition(), projectileEndPoint);
    }
}

void DancingPrinces::changeLeafSpawnPosition(sf::Vector2f& position)
{
    int rand = m_lastLeafSpawn;
    int counter = 0;

    while (rand == m_lastLeafSpawn && counter != 10) {
        rand = nce::RNG::intWithinRange(0, 2);
        counter++;
    }

    // If random is very unlucky, use the other method
    if (rand == m_lastLeafSpawn) {
        m_lastLeafSpawn += 1;
        if (m_lastLeafSpawn == 3)
            m_lastLeafSpawn = 0;

        rand = m_lastLeafSpawn;
    } else
        m_lastLeafSpawn = rand;

    position.x += m_leafSpawnPoints[rand].x;
    position.y += m_leafSpawnPoints[rand].y;
}
