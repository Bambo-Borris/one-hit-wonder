#include "BossBase.hpp"
#include "ArrowProjectile.hpp"
#include "PlayerCharacter.hpp"
#include <assert.h>

BossBase::BossBase(
    nce::AppState* currentState, PlayerCharacter* player, int32_t maxHitPoints, const std::vector<BossProjectile*>& projectiles, std::string_view bossName)
    : nce::GameObject(currentState)
    , m_player(player)
    , m_hitPoints(maxHitPoints)
    , m_maxHitPoints(maxHitPoints)
    , m_projectiles(projectiles)
    , m_name(bossName)
{
}

void BossBase::reset() { m_hitPoints = m_maxHitPoints; }

void BossBase::update(const sf::Time& dt) { NCE_UNUSED(dt); }

void BossBase::hitByArrow(int32_t arrowDamage)
{
    m_hitPoints -= m_damageModifier * arrowDamage;
    m_hitPoints = std::max(m_hitPoints, 0);
}

auto BossBase::getName() -> std::string_view { return m_name; }

void BossBase::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    NCE_UNUSED(manifold);

    if (go->getIdentifier() == "arrow_projectile") {
        auto arrow { dynamic_cast<ArrowProjectile*>(go) };
        assert(arrow);

        if (!arrow)
            throw std::runtime_error("Er, we found an arrow non arrow...");

        hitByArrow(arrow->getArrowDamage());
    }
}

auto BossBase::getAvailableProjectile(std::optional<BossProjectile::ProjectileType> typeFilter) -> BossProjectile*
{
    constexpr auto filterTypeless = [](BossProjectile* bp) { return !bp->isActive(); };
    const auto filterType = [&typeFilter](BossProjectile* bp) { return !bp->isActive() && bp->getType() == typeFilter; };

    BossProjectile* bp { nullptr };
    if (typeFilter) {
        auto findResult = std::find_if(m_projectiles.begin(), m_projectiles.end(), filterType);
        assert(findResult != m_projectiles.end());
        bp = *findResult;
    } else {
        auto findResult = std::find_if(m_projectiles.begin(), m_projectiles.end(), filterTypeless);
        assert(findResult != m_projectiles.end());
        bp = *findResult;
    }
    return bp;
}
