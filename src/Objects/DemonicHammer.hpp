#pragma once

#include "BossBase.hpp"

#include <nce/AnimatedSprite.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/TweeningAnimation.hpp>
#include <unordered_map>

class BossProjectile;
class CameraShake;

class DemonicHammer : public BossBase {
public:
    DemonicHammer(nce::AppState* appState, PlayerCharacter* player, const sf::Vector2f& playAreaSize, const std::vector<BossProjectile*>& projectiles);
    ~DemonicHammer();

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    virtual void reset() override;

protected:
    void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;
    virtual sf::Transformable* getTransformable() override { return &m_sprite; }

private:
    enum class ActivityState { Flying, NailBarrage, SmashDown };
    enum class SmashDownState { Falling, StuckInGround, TeleportShrink, TeleportGrow, MAX }; // Sub state to control smash down sequence
    enum class AnimationStage : uint32_t { Idle, Charging, Slam, MAX };

    void constructAnimations();
    void setActivityState(ActivityState nextState);
    void updateFlying(const sf::Time& dt);
    void updateNailBarrage();
    void updateSmashDown(const sf::Time& dt);
    void setAnimation(AnimationStage animation, bool playOnChange = false);

    [[nodiscard]] auto getCurrentDirectionToAngle() const -> sf::Angle { return m_flyingDirection.x < 0 ? sf::degrees(270.f) : sf::degrees(90.f); }
    [[nodiscard]] auto getOppositeFacingAngle() const -> sf::Angle { return m_flyingDirection.x < 0 ? sf::degrees(90.f) : sf::degrees(270.f); }

    void fireNailBarrageProjectiles();
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go) override;

    nce::AnimatedSprite m_sprite;
    std::unordered_map<AnimationStage, nce::Animation> m_animations;
    std::unordered_map<AnimationStage, sf::Time> m_animationFrameTimes;
    AnimationStage m_animationStage { AnimationStage::Idle };

    std::weak_ptr<nce::ColliderInfo> m_colliderInfo;
    std::unique_ptr<CameraShake> m_cameraShake { nullptr };
    std::unordered_map<int, std::unique_ptr<nce::TAFloat>> m_smashDownStateAnimationsFloat;
    std::unordered_map<int, std::unique_ptr<nce::TAVec2f>> m_smashDownStateAnimationsVector;

    sf::Vector2f m_playAreaSize;
    sf::Vector2f m_flyingDirection;

    ActivityState m_activityState { ActivityState::Flying };
    SmashDownState m_smashDownState { SmashDownState::Falling };
    nce::CollisionSolver::Callback m_cbWrapper;

    bool m_flyingRight { false };
    bool m_didHitFloor { false };
};
