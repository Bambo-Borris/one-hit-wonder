#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include <nce/GameObject.hpp>

class CameraShake;

class ArcLaser : public nce::GameObject {
public:
    enum class SideSpawn { Left, Right };
    ArcLaser(nce::AppState* currentState);
    ~ArcLaser();

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    void activateSequence(float startAngle, float endAngle);
    void setTracking(const sf::Transformable* transformable);
    auto hasAnimationConcluded() const -> bool { return m_isFinished; }
    void reset();

protected:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    const sf::Transformable* m_tracking { nullptr };
    sf::RectangleShape m_shape;

    sf::Time m_timer;

    float m_startRotation { 0.f };
    float m_endRotation { 0.f };
    bool m_isFinished { false };
    std::unique_ptr<CameraShake> m_cameraShake { nullptr };
};
