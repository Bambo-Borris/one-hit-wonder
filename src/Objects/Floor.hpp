#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include <nce/GameObject.hpp>

class BossStateBase;

namespace nce {
struct ColliderInfo;
}

class Floor : public nce::GameObject {
public:
    Floor(nce::AppState* currentState);
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    [[nodiscard]] auto getBoundsRect() -> sf::FloatRect { return m_shape.getGlobalBounds(); }

protected:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    sf::RectangleShape m_shape;
    std::shared_ptr<nce::ColliderInfo> m_colliderInfo;
};
