#pragma once

#include <nce/Collisions.hpp>
#include <nce/GameObject.hpp>

class RadialProjectile : public nce::GameObject {
public:
    RadialProjectile(nce::AppState* currentState, const sf::Vector2f& position);
    ~RadialProjectile();

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    [[nodiscard]] sf::Angle getAngle() { return m_angle; }
    [[nodiscard]] sf::Angle getAngleOffset() { return m_angleOffset; }
    [[nodiscard]] bool getActive() { return m_active; }
    [[nodiscard]] bool getInCirlce() { return m_inCircle; }
    void setAngle(const sf::Angle& ang) { m_angle = ang; }
    void setAngleOffset(const sf::Angle& ang) { m_angleOffset = ang; }
    void setActive(bool ean) { m_active = ean; }
    void setInCirlce(bool ean) { m_inCircle = ean; }
    void setPosition(const sf::Vector2f& pos) { m_shape.setPosition(pos); }
    void setRotation(const sf::Angle& ang) { m_shape.setRotation(ang); }

private:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    sf::CircleShape m_shape;
    std::shared_ptr<nce::ColliderInfo> m_colliderInfo;

    // Might add a Tweening Animation here for rotating projectiles

    sf::Angle m_angle;
    sf::Angle m_angleOffset;
    bool m_active { false };
    bool m_inCircle { true };
};
