#pragma once

#include "RadialProjectile.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

RadialProjectile::RadialProjectile(nce::AppState* currentState, const sf::Vector2f& position)
    : nce::GameObject(currentState)
    , m_colliderInfo(nce::MakeAABBCollider(getBossesDancingPrincesVectorVar("acid_pool_size") / 2.f, &m_shape, this))
{
    float radius { getBossesUnnamedFloatVar("projectile_radius") };
    m_shape.setRadius(radius);
    m_shape.setOrigin({ radius, radius });
    m_shape.setPosition(position);
    m_shape.setFillColor(sf::Color::Magenta);

    auto bossState = static_cast<BossStateBase*>(currentState);
    assert(bossState);
    if (!bossState->getSolver().addCollider(m_colliderInfo))
        throw std::runtime_error("Unable to create collider for explosion");

    setIdentifier("radial_projectile");
}

RadialProjectile::~RadialProjectile() { }

// Might not need this, could be done in ability?
void RadialProjectile::update(const sf::Time& dt) { NCE_UNUSED(dt); }

void RadialProjectile::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }