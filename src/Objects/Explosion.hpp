#pragma once

#include <SFML/Graphics/CircleShape.hpp>
#include <nce/Collisions.hpp>
#include <nce/GameObject.hpp>

#include <spdlog/spdlog.h>

namespace nce {
class AppState;
};

class Explosion : public nce::GameObject {
public:
    Explosion(nce::AppState* currentState, const sf::Vector2f& spawnPosition);
    ~Explosion();

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    sf::CircleShape m_shape;
    sf::Time m_aliveTime;
    std::shared_ptr<nce::ColliderInfo> m_colliderInfo;
};