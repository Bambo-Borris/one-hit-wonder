#include "AcidFloor.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <assert.h>
#include <nce/NCE.hpp>
#include <stdexcept>

AcidFloor::AcidFloor(nce::AppState* currentState, sf::Vector2f position)
    : nce::GameObject(currentState)
    , m_colliderInfo(nce::MakeAABBCollider(getBossesDancingPrincesVectorVar("acid_pool_size") / 2.f, &m_shape, this))
{
    m_shape.setSize(getBossesDancingPrincesVectorVar("acid_pool_size"));
    m_shape.setOrigin(m_shape.getSize() / 2.f);
    m_shape.setPosition(position);
    m_shape.setFillColor(sf::Color(107, 41, 214));
    m_timerName = nce::GenerateUUID();
    auto& timerController = currentState->getTimerController();
    timerController.addTimer(m_timerName);

    auto bossState = static_cast<BossStateBase*>(currentState);
    assert(bossState);
    if (!bossState->getSolver().addCollider(m_colliderInfo))
        throw std::runtime_error("Unable to create collider for explosion");

    setIdentifier("acid_floor");
}

AcidFloor::~AcidFloor() { getAppState()->getTimerController().removeTimer(m_timerName); }

void AcidFloor::update(const sf::Time& dt) { NCE_UNUSED(dt); }

void AcidFloor::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }
