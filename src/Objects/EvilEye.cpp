#include "EvilEye.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "../Utils/AIUtility.hpp"
#include "ArcLaser.hpp"
#include "ArrowProjectile.hpp"
#include "BossProjectile.hpp"
#include "PlayerCharacter.hpp"

#include <cmath>
#include <nce/AssetHolder.hpp>
#include <nce/TimerController.hpp>

constexpr sf::Vector2f FIRE_SHAPE_SIZE { 128.f, 128.f };
constexpr sf::Vector2f SPRITE_DIM { 32.f, 32.f };

constexpr auto SCREEN_TOP_OFFSET { 40.f };
constexpr auto DODGE_X_OFFSET { 200.f };

/**********************************************************************
Timers available:
==================
    - eyeWobble = Used to control horizontal motion with sin()
    - eyeArcLaser = Cooldown timer of arc laser
    - eyeDodgeCooldown = Cooldown timer for dodging
    - eyeFireCooldown = Cooldown on firing projectiles
**********************************************************************/

EvilEye::EvilEye(nce::AppState* currentState,
                 PlayerCharacter* player,
                 ArcLaser* arc,
                 const std::vector<BossProjectile*>& projectiles,
                 const std::vector<ArrowProjectile*>& arrows,
                 const sf::Vector2f& playAreaSize)
    : BossBase(currentState, player, int32_t(getBossesEvilEyeFloatVar("hit_points")), projectiles, gc::BOSS_NAMES[gc::EVIL_EYE_INDEX])
    , m_cbWrapper(std::bind(&EvilEye::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
    , m_arcLaser(arc)
    , m_playAreaSize(playAreaSize)
    , m_arrows(arrows)
{
    setIdentifier("evil_eye");
    setupAnimations();

    m_sprite.setOrigin(SPRITE_DIM * 0.5f);
    m_sprite.setScale(FIRE_SHAPE_SIZE.cwiseDiv(SPRITE_DIM));

    currentState->getTimerController().addTimer("eyeWobble");
    currentState->getTimerController().addTimer("eyeArcLaser");
    currentState->getTimerController().addTimer("eyeDodgeCooldown");
    currentState->getTimerController().addTimer("eyeFireCooldown");

    m_arcLaser->setTracking(&m_sprite);

    auto colliderInfo = nce::MakeCircleCollider(SPRITE_DIM.x * 0.40f, &m_sprite, this);
    m_colliderInfo = colliderInfo;

    auto bossState { dynamic_cast<BossStateBase*>(currentState) };
    assert(bossState);

    auto& solver { bossState->getSolver() };
    if (!solver.addCollider(colliderInfo))
        throw std::runtime_error("Unable to add collider");

    if (!solver.subscribeNotification({ colliderInfo, &m_cbWrapper }))
        throw std::runtime_error("Unable to subscribe for collider notifications");

    reset();
}

EvilEye::~EvilEye() { }

void EvilEye::update(const sf::Time& dt)
{
    BossBase::update(dt);

    auto& timerController { getAppState()->getTimerController() };

    m_sprite.update(dt);
    m_verticalOffset = std::sin(timerController.getElapsed("eyeWobble").asSeconds() * 2.f);
    const auto dodgeCooldown { getBossesEvilEyeFloatVar("dodge_cooldown") };
    // If the dodge cooldown has expired & we're not in any other state than
    // hovering then we can dodge. Otherwise we cannot
    if (timerController.getElapsed("eyeDodgeCooldown") >= sf::seconds(dodgeCooldown) && m_currentState == ActivityState::Hovering) {
        auto shouldDodge { false };
        for (auto& a : m_arrows) {
            assert(a);
            if (!a->isActive())
                continue;

            const auto distanceSq = (a->getPosition() - m_sprite.getPosition()).lengthSq();
            if (distanceSq < std::pow(150.f, 2.f)) {
                shouldDodge = true;
                break;
            }
        }

        if (shouldDodge) {
            m_currentState = ActivityState::Dodging;
            m_reachedMaxDodge = false;
            // Check if the player is to the left or right
            // of the boss, then dodge toward the players side
            auto playerPtr { getPlayer() };
            assert(playerPtr);
            if (playerPtr->getPosition().x <= m_sprite.getPosition().x)
                m_dodgeDirection = 1.f;
            else
                m_dodgeDirection = -1.f;
        }
    }

    switch (m_currentState) {
        using enum ActivityState;
    case Hovering:
        updateHovering(dt);
        break;
    case Dodging:
        updateDodging(dt);
        break;
    case Shooting:
        updateShooting(dt);
        break;
    case ArcLaserFiring: {
        updateArcLaser(dt);
    } break;
    default:
        throw std::runtime_error("Uhhh.... IDK?");
        break;
    }
    m_sprite.setPosition({ m_sprite.getPosition().x, (SCREEN_TOP_OFFSET * m_verticalOffset) + (m_playAreaSize.y * 0.5f) });

    // We need to keep track of how many frames the player spends on the left
    // and right of the screen for the arc laser
    auto playerPtr { getPlayer() };
    assert(playerPtr);
    if (playerPtr->getPosition().x < m_playAreaSize.x / 2.f) {
        ++m_framesOnLeft;
    } else if (playerPtr->getPosition().x > m_playAreaSize.x / 2.f) {
        ++m_framesOnRight;
    }
}

void EvilEye::reset()
{
    auto& timerController { getAppState()->getTimerController() };

    m_sprite.setPosition({ m_playAreaSize.x * 0.5f, (FIRE_SHAPE_SIZE.y * 0.5f) + SCREEN_TOP_OFFSET });
    timerController.resetTimer("eyeWobble");
    timerController.resetTimer("eyeFireCooldown");
    timerController.resetTimer("eyeDodgeCooldown");
    timerController.resetTimer("eyeArcLaser");

    m_hasFiredFirstShot = false;
    m_reachedMaxDodge = false;
    m_verticalOffset = 0.f;
    m_dodgeDirection = 0.f;
    m_framesOnLeft = 0;
    m_framesOnRight = 0;
    m_fireCooldownMax = sf::seconds(2.5f);
    m_currentState = ActivityState::Hovering;

    m_sprite.stop();
    m_sprite.play();

    BossBase::reset();
}

void EvilEye::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    target.draw(m_sprite, states);
    BossBase::draw(target, states);
}

void EvilEye::setupAnimations()
{
    auto texture = nce::AssetHolder::get().getTexture("bin/textures/GlaringOverlordIdleFront.png");
    if (!texture->generateMipmap())
        throw std::runtime_error("Unable to generate mip maps");

    m_hoverAnimation.setSpriteSheet(*texture);
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 0 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 1 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 2 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 3 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 4 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 5 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 6 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });
    m_hoverAnimation.addFrame(sf::IntRect { sf::Vector2i { 7 * int32_t(SPRITE_DIM.x), 0 * int32_t(SPRITE_DIM.y) }, sf::Vector2i { SPRITE_DIM } });

    m_sprite.setAnimation(m_hoverAnimation);
    m_sprite.setFrameTime(sf::milliseconds(150));
}

void EvilEye::updateHovering(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    auto& timerController { getAppState()->getTimerController() };

    // If we can fire
    if (timerController.getElapsed("eyeFireCooldown") > m_fireCooldownMax) {
        m_currentState = ActivityState::Shooting;
        return;
    } else {
        if (timerController.isPaused("eyeFireCooldown"))
            timerController.playTimer("eyeFireCooldown");
        // We should only transition to the arc laser
        // if we're at the valid time and we're not
        // currently about to fire a projectile.
        const auto arcLaserCooldown { getBossesEvilEyeFloatVar("arc_laser_cooldown") };
        if (timerController.getElapsed("eyeArcLaser") >= sf::seconds(arcLaserCooldown)) {
            m_currentState = ActivityState::ArcLaserFiring;
        }
    }
}

void EvilEye::updateDodging(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    constexpr auto DODGE_TIME { sf::seconds(0.75f) };
    const auto targetX { (m_playAreaSize.x / 2.f) + (m_dodgeDirection * DODGE_X_OFFSET) };

    auto& timerController { getAppState()->getTimerController() };

    // If we are on the initial animation, lets setup the tweening animation for it
    // otherwise check if we've finished the initial animation
    if (!m_dodgeAnimation) {
        m_dodgeAnimation = std::make_unique<nce::TAFloat>(
            nce::AnimationCurve::EaseOutElastic, "evilEyeDodge", m_playAreaSize.x / 2.f, targetX, DODGE_TIME, &timerController);
        assert(m_dodgeAnimation);
        m_dodgeAnimation->play();
    }

    const auto newX { m_dodgeAnimation->getTweenedValue() };
    m_sprite.setPosition({ newX, m_sprite.getPosition().y });

    if (m_dodgeAnimation->isComplete()) {
        if (!m_reachedMaxDodge) {
            m_reachedMaxDodge = true;
            m_dodgeAnimation.reset();
            m_dodgeAnimation
                = std::make_unique<nce::TAFloat>(nce::AnimationCurve::Lerp, "evilEyeDodge", targetX, m_playAreaSize.x / 2.f, DODGE_TIME, &timerController);
            m_dodgeAnimation->play();
        } else {
            m_currentState = ActivityState::Hovering;
            timerController.resetTimer("eyeDodgeCooldown");
            m_sprite.setPosition({ m_playAreaSize.x / 2.f, m_sprite.getPosition().y });
            m_dodgeAnimation.reset(nullptr);
        }
    }
}

void EvilEye::updateShooting(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    const auto playerMoveSpeed = getPlayerFloatVar("max_move_speed");
    auto proj = getAvailableProjectile();

    const auto playerPtr { getPlayer() };
    assert(playerPtr);

    const auto spawnLocation = m_sprite.getPosition();
    const auto projectileEndPoint = getLinearShotPredictedEndPoint(getAppState()->getTimerController(),
                                                                   proj->getSpeed(),
                                                                   spawnLocation,
                                                                   playerPtr->getPosition(),
                                                                   playerPtr->getXDirection(),
                                                                   playerMoveSpeed,
                                                                   playerPtr->getDirectionChangeCounter(),
                                                                   { 0.f, m_playAreaSize.x });
    auto& timerController { getAppState()->getTimerController() };

    proj->fire(spawnLocation, projectileEndPoint);

    timerController.resetTimer("eyeFireCooldown");
    timerController.pauseTimer("eyeFireCooldown");

    // lets re-randomise the cooldown then return back to our neutral state of hovering

    const auto fireCooldownMin = getBossesEvilEyeFloatVar("fire_cooldown_min");
    const auto fireCooldownMax = getBossesEvilEyeFloatVar("fire_cooldown_max");

    m_fireCooldownMax = sf::seconds(nce::RNG::realWithinRange(fireCooldownMin, fireCooldownMax));
    m_currentState = ActivityState::Hovering;
}

void EvilEye::updateArcLaser(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    assert(m_arcLaser);

    if (!m_arcLaser->isActive()) {
        assert(m_arcLaser);
        if (m_framesOnLeft > m_framesOnRight) {
            m_arcLaser->activateSequence(180.f, 90.f);
        } else if (m_framesOnRight > m_framesOnLeft) {
            m_arcLaser->activateSequence(0.f, 90.f);
        } else {

            const auto value = nce::RNG::intWithinRange(0, 100);
            if (value > 50)
                m_arcLaser->activateSequence(0.f, 90.f);
            else
                m_arcLaser->activateSequence(180.f, 90.f);
        }

        m_framesOnLeft = 0;
        m_framesOnRight = 0;
    } else {
        if (m_arcLaser->hasAnimationConcluded()) {
            m_currentState = ActivityState::Hovering;
            m_arcLaser->setActive(false);
            auto& timerController { getAppState()->getTimerController() };
            timerController.resetTimer("eyeArcLaser");
            timerController.resetTimer("eyeFireCooldown");
        }
    }
}

void EvilEye::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go) { BossBase::handleCollisions(manifold, go); }
