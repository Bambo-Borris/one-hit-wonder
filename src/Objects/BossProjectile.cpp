#include "BossProjectile.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "AcidFloor.hpp"
#include "Explosion.hpp"
#include "PlayerCharacter.hpp"

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/MathsUtils.hpp>
#include <nce/NCE.hpp>
#include <nce/RNG.hpp>
#include <spdlog/spdlog.h>

/* Dimensions in game */

constexpr auto FIRE_SPRITE_SIZE { sf::Vector2f { 16.f, 32.f } };
constexpr auto NAIL_SHAPE_SIZE { sf::Vector2f { 16.f, 4.f } };
constexpr auto LEAF_FALL_SIZE { sf::Vector2f { 24.f, 24.f } };
constexpr auto BOMB_SPRITE_SIZE { sf::Vector2f { 16.f, 16.f } };

constexpr auto SEEKING_ARROW_SHAPE_RADIUS { 8.f };
constexpr auto ACID_BOMB_SHAPE_RADIUS { 16.f };

/* Dimensions in texture */

constexpr auto FLAME_SPRITE_DIM { sf::Vector2i { 16, 32 } };
constexpr auto BOMB_SPRITE_DIM { sf::Vector2i { 16, 16 } };

BossProjectile::BossProjectile(nce::AppState* currentState)
    : GameObject(currentState)
    , m_cbWrapper(std::bind(&BossProjectile::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))

{
    constructAnimations();
    setIdentifier(fmt::format("boss_projectile_{}", nce::GenerateUUID()));
    setupProjectile();
    setRenderLayer(5);
}

void BossProjectile::update(const sf::Time& dt)
{
    if (m_sprite)
        m_sprite->update(dt);

    switch (m_projectileType) {
        using enum ProjectileType;
    case Flame:
        updateFlame(dt);
        break;
    case SeekingArrow:
        updateSeekingArrow(dt);
        break;
    case FlameBarrage:
        updateFlame(dt);
        break;
    case Bomb:
        updateBomb(dt);
        break;
    case LeafFall:
        updateLeafFall(dt);
        break;
    case Acid:
        updateAcid(dt);
        break;
    default:
        assert(false);
        break;
    }

    // Check if we've exited the screen
    auto bossState = dynamic_cast<BossStateBase*>(getAppState());
    assert(bossState);
    if (m_shape) {
        if (!m_shape->getGlobalBounds().findIntersection(bossState->getPlayAreaSize())) {
            reset();
        }
    } else if (m_sprite) {
        if (!m_sprite->getGlobalBounds().findIntersection(bossState->getPlayAreaSize())) {
            m_sprite->stop();
            reset();
        }
    }
}

void BossProjectile::fire(const sf::Vector2f& start, const sf::Vector2f& end, PlayerCharacter* player)
{
    setActive(true);
    if (m_shape)
        m_shape->setPosition(start);
    else if (m_sprite)
        m_sprite->setPosition(start);

    switch (m_projectileType) {
        using enum ProjectileType;
    case Flame: {
        const auto stateID { getAppState()->getStateID() };
        if (stateID == gc::EVIL_EYE_STATE_NAME)
            m_speed = getBossesEvilEyeFloatVar("laser_speed");
        else
            m_speed = getBossesDancingPrincesFloatVar("laser_speed");
        fireStraightProjectile(start, end);

    } break;
    case FlameBarrage:
        m_speed = getBossesSharedFloatVar("nail_speed");
        fireStraightProjectile(start, end);
        break;
    case SeekingArrow:
        assert(player);
        m_speed = getBossesSharedFloatVar("arrow_speed");
        fireSeekingArrow(start, end, player);
        break;
    case Bomb:
        fireBomb(start, end);
        break;
    case LeafFall:
        fireLeafFall(start, end);
        break;
    case Acid:
        m_speed = getBossesDancingPrincesFloatVar("acid_projectile_speed");
        fireAcid(start, end);
        break;
    default:
        assert(false);
        break;
    }

    if (m_sprite)
        m_sprite->play();
}

void BossProjectile::setProjectileType(ProjectileType type)
{
    m_projectileType = type;
    m_velocity = {};

    m_sprite.reset();
    m_shape.reset();

    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    auto& solver { bossState->getSolver() };
    solver.unsubscribeNotification({ m_colliderInfo, &m_cbWrapper });
    solver.removeCollider(m_colliderInfo);
    setupProjectile();
}

void BossProjectile::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (m_shape)
        target.draw(*m_shape, states);
    else if (m_sprite)
        target.draw(*m_sprite, states);
}

void BossProjectile::constructAnimations()
{
    auto fireSpriteSheet { nce::AssetHolder::get().getTexture("bin/textures/fire_projectiles.png") };
    if (!fireSpriteSheet)
        throw std::runtime_error("Unable to load fire projectiles spritesheet");

    auto bombSpriteSheet { nce::AssetHolder::get().getTexture("bin/textures/projectile_bomb.png") };
    if (!bombSpriteSheet)
        throw std::runtime_error("Unable to load bomb projectiles spritesheet");

    m_animationFrameTimes[ProjectileType::Flame] = sf::milliseconds(25);
    m_animations[ProjectileType::Flame].setSpriteSheet(*fireSpriteSheet);
    m_animations[ProjectileType::Flame].addFrame({ sf::Vector2i { 0 * FLAME_SPRITE_DIM.x, 0 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });
    m_animations[ProjectileType::Flame].addFrame({ sf::Vector2i { 1 * FLAME_SPRITE_DIM.x, 0 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });
    m_animations[ProjectileType::Flame].addFrame({ sf::Vector2i { 2 * FLAME_SPRITE_DIM.x, 0 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });

    m_animationFrameTimes[ProjectileType::FlameBarrage] = sf::milliseconds(25);
    m_animations[ProjectileType::FlameBarrage].setSpriteSheet(*fireSpriteSheet);
    m_animations[ProjectileType::FlameBarrage].addFrame({ sf::Vector2i { 0 * FLAME_SPRITE_DIM.x, 1 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });
    m_animations[ProjectileType::FlameBarrage].addFrame({ sf::Vector2i { 1 * FLAME_SPRITE_DIM.x, 1 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });
    m_animations[ProjectileType::FlameBarrage].addFrame({ sf::Vector2i { 2 * FLAME_SPRITE_DIM.x, 1 * FLAME_SPRITE_DIM.y }, FLAME_SPRITE_DIM });

    m_animationFrameTimes[ProjectileType::Bomb] = sf::milliseconds(100);
    m_animations[ProjectileType::Bomb].setSpriteSheet(*bombSpriteSheet);
    m_animations[ProjectileType::Bomb].addFrame({ sf::Vector2i { 0 * BOMB_SPRITE_DIM.x, 0 * BOMB_SPRITE_DIM.y }, BOMB_SPRITE_DIM });
    m_animations[ProjectileType::Bomb].addFrame({ sf::Vector2i { 1 * BOMB_SPRITE_DIM.x, 0 * BOMB_SPRITE_DIM.y }, BOMB_SPRITE_DIM });
    m_animations[ProjectileType::Bomb].addFrame({ sf::Vector2i { 2 * BOMB_SPRITE_DIM.x, 0 * BOMB_SPRITE_DIM.y }, BOMB_SPRITE_DIM });
}

void BossProjectile::setupProjectile()
{
    m_shape.reset();

    switch (m_projectileType) {
        using enum ProjectileType;
    case Flame: {
        m_sprite = std::make_unique<nce::AnimatedSprite>();
        m_sprite->setAnimation(m_animations[ProjectileType::Flame]);
        m_sprite->setFrameTime(m_animationFrameTimes[ProjectileType::Flame]);
        m_sprite->setScale(FIRE_SPRITE_SIZE.cwiseDiv(sf::Vector2f { FLAME_SPRITE_DIM }));
        m_sprite->setOrigin(FIRE_SPRITE_SIZE * 0.5f);
        m_sprite->setLooped(true);

        m_colliderInfo = nce::MakeOBBCollider(FIRE_SPRITE_SIZE * 0.5f, m_sprite.get(), this);
    } break;
    case FlameBarrage: {
        m_sprite = std::make_unique<nce::AnimatedSprite>();
        m_sprite->setAnimation(m_animations[ProjectileType::FlameBarrage]);
        m_sprite->setFrameTime(m_animationFrameTimes[ProjectileType::FlameBarrage]);
        m_sprite->setScale(FIRE_SPRITE_SIZE.cwiseDiv(sf::Vector2f { FLAME_SPRITE_DIM }));
        m_sprite->setOrigin(FIRE_SPRITE_SIZE * 0.5f);
        m_sprite->setLooped(true);

        m_colliderInfo = nce::MakeOBBCollider(FIRE_SPRITE_SIZE * 0.5f, m_sprite.get(), this);
    } break;
    case SeekingArrow: {
        m_shape = std::make_shared<sf::CircleShape>();
        auto circle = std::dynamic_pointer_cast<sf::CircleShape>(m_shape);
        assert(circle);
        circle->setPointCount(3);
        circle->setRadius(SEEKING_ARROW_SHAPE_RADIUS);
        circle->setOrigin({ SEEKING_ARROW_SHAPE_RADIUS, SEEKING_ARROW_SHAPE_RADIUS });
        circle->setFillColor(sf::Color::Green);
    } break;
    case Bomb: {
        m_sprite = std::make_unique<nce::AnimatedSprite>();
        m_sprite->setAnimation(m_animations[ProjectileType::Bomb]);
        m_sprite->setFrameTime(m_animationFrameTimes[ProjectileType::Bomb]);
        m_sprite->setScale(BOMB_SPRITE_SIZE.cwiseDiv(sf::Vector2f { BOMB_SPRITE_DIM }));
        m_sprite->setOrigin(BOMB_SPRITE_SIZE * 0.5f);
        m_sprite->setLooped(true);

        m_speed = 0.f;
        m_colliderInfo = nce::MakeOBBCollider(BOMB_SPRITE_SIZE * 0.5f, m_sprite.get(), this);

    } break;
    case LeafFall: {
        m_shape = std::make_shared<sf::RectangleShape>(LEAF_FALL_SIZE);
        auto rect { std::dynamic_pointer_cast<sf::RectangleShape>(m_shape) };
        rect->setOrigin(rect->getSize() * 0.5f);
        rect->setRotation(sf::degrees(45.f));
        rect->setFillColor({ 22, 88, 97 });
        m_speed = 0.f;
        m_colliderInfo = nce::MakeOBBCollider(LEAF_FALL_SIZE * 0.5f, m_shape.get(), this);

    } break;
    case Acid: {
        m_shape = std::make_shared<sf::CircleShape>();
        auto circle = std::dynamic_pointer_cast<sf::CircleShape>(m_shape);
        assert(circle);
        circle->setRadius(ACID_BOMB_SHAPE_RADIUS);
        circle->setOrigin({ ACID_BOMB_SHAPE_RADIUS, ACID_BOMB_SHAPE_RADIUS });
        circle->setFillColor(sf::Color::Green);
        m_colliderInfo = nce::MakeCircleCollider(ACID_BOMB_SHAPE_RADIUS, m_shape.get(), this);
    } break;
    default:
        assert(false);
        break;
    }

    // We should be wary here, definitely need to look at a better approach
    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    if (!bossState->getSolver().addCollider(m_colliderInfo))
        throw std::runtime_error("Unable to add collider");

    if (!bossState->getSolver().subscribeNotification({ m_colliderInfo, &m_cbWrapper }))
        throw std::runtime_error("Unable subscribe to collider callback");
}

void BossProjectile::fireStraightProjectile(const sf::Vector2f& start, const sf::Vector2f& end)
{
    if ((end - start) != sf::Vector2f {})
        m_velocity = (end - start).normalized() * m_speed;
    else
        m_velocity = {};
}

void BossProjectile::fireSeekingArrow(const sf::Vector2f& start, const sf::Vector2f& end, PlayerCharacter* player)
{
    assert(player);
    m_player = player;
    if ((end - start) != sf::Vector2f {})
        m_velocity = (end - start).normalized() * m_speed;
    else
        m_velocity = {};
}

void BossProjectile::fireBomb(const sf::Vector2f& start, const sf::Vector2f& end)
{
    m_direction = (end - start).normalized();
    m_velocity = {};
    m_speed = 0.f;
}

void BossProjectile::fireLeafFall(const sf::Vector2f& start, const sf::Vector2f& end)
{
    m_direction = (end - start).normalized();
    m_velocity = {};
    m_swayClock.restart();
}

void BossProjectile::fireAcid(const sf::Vector2f& start, const sf::Vector2f& end)
{
    if ((end - start) != sf::Vector2f {})
        m_velocity = (end - start).normalized() * m_speed;
    else
        m_velocity = {};
}

void BossProjectile::updateFlame(const sf::Time& dt)
{
    m_sprite->move(m_velocity * dt.asSeconds());
    updateRotationFromVelocity();
}

void BossProjectile::updateSeekingArrow(const sf::Time& dt)
{
    assert(m_player);
    constexpr auto RotationSpeed = 90.f;
    m_shape->move(m_velocity * dt.asSeconds());

    const auto velocityAngle = m_velocity.angle();
    const auto directionAngle = (m_player->getPosition() - m_shape->getPosition()).angle();
    const auto angleDelta { (directionAngle - velocityAngle) };

    if (angleDelta != sf::Angle::Zero) {
        const auto sign { angleDelta.asDegrees() / std::abs(angleDelta.asDegrees()) };
        m_velocity = m_velocity.rotatedBy(sf::degrees(sign * RotationSpeed * dt.asSeconds()));
    }
}

void BossProjectile::updateBomb(const sf::Time& dt)
{
    m_sprite->move(m_velocity * dt.asSeconds());
    const auto gravity = getWorldVectorVar("gravity") * getWorldFloatVar("boss_projectile_gravity_scale");
    m_velocity += gravity * dt.asSeconds();
    updateRotationFromVelocity();
}

void BossProjectile::updateLeafFall(const sf::Time& dt)
{
    m_shape->move(m_velocity * dt.asSeconds());
    const auto swayRange { getWorldFloatVar("leaf_fall_sway_range") };
    const auto dropSpeed { getWorldFloatVar("leaf_fall_drop_speed") };
    m_velocity = { swayRange * std::cos(m_swayClock.getElapsedTime().asSeconds()), dropSpeed };
}

void BossProjectile::updateAcid(const sf::Time& dt) { m_shape->move(m_velocity * dt.asSeconds()); }

void BossProjectile::updateRotationFromVelocity()
{
    sf::Angle offset;

    if (m_projectileType == ProjectileType::Bomb) {
        offset = sf::degrees(90.f);
    } else if (m_projectileType == ProjectileType::Flame || m_projectileType == ProjectileType::FlameBarrage) {
        offset = sf::degrees(-90.f);
    } else {
        offset = sf::degrees(0.f);
    }
    auto transformable = m_sprite ? static_cast<sf::Transformable*>(m_sprite.get()) : static_cast<sf::Transformable*>(m_shape.get());
    if (m_velocity != sf::Vector2f {})
        transformable->setRotation(m_velocity.angle() + offset);
    else
        transformable->setRotation(sf::Angle::Zero + offset);
}

void BossProjectile::handleCollisions(const nce::Manifold& manifold, GameObject* const go)
{
    NCE_UNUSED(manifold);
    if (go->getIdentifier() == "floor") {
        reset();

        // We need to spawn an explosion effect if we're falling rain
        if (m_projectileType == ProjectileType::Bomb) {
            auto appState { getAppState() };
            auto contactPoint = m_sprite->getPosition() + sf::Vector2f(0.f, m_sprite->getGlobalBounds().height / 2.f);
            contactPoint += (manifold.normal * manifold.penetration);
            auto explosionObject = new Explosion(appState, contactPoint);
            if (!appState->addGameObject(explosionObject))
                throw std::runtime_error("Unable to create explosion object");
        } else if (m_projectileType == ProjectileType::Acid) {
            auto appState { getAppState() };
            auto contactPoint = m_shape->getPosition() + sf::Vector2f(0.f, m_shape->getGlobalBounds().height / 2.f);
            contactPoint += (manifold.normal * manifold.penetration);

            auto floor = dynamic_cast<Floor*>(go);
            if (!floor)
                throw std::runtime_error("Unable to cast game object to floor type");

            auto acidFloorObject = new AcidFloor(
                appState, sf::Vector2f { contactPoint.x, floor->getBoundsRect().top - getBossesDancingPrincesVectorVar("acid_pool_size").y / 2.f });
            if (!appState->addGameObject(acidFloorObject))
                throw std::runtime_error("Unable to create acid floor object");
        }

        if (m_sprite)
            m_sprite->stop();
    }
}

void BossProjectile::terminate() { }

void BossProjectile::reset()
{
    setActive(false);
    m_velocity = {};
    m_player = nullptr;
}
