#pragma once

#include "BossProjectile.hpp"

#include <nce/Collisions.hpp>
#include <nce/GameObject.hpp>

class PlayerCharacter;

class BossBase : public nce::GameObject {
public:
    BossBase(
        nce::AppState* currentState, PlayerCharacter* player, int32_t maxHitPoints, const std::vector<BossProjectile*>& projectiles, std::string_view bossName);
    virtual ~BossBase() = default;

    auto getHealth() const -> int32_t { return m_hitPoints; }
    auto getMaxHealth() const -> int32_t { return m_maxHitPoints; }

    virtual void reset();
    virtual void update(const sf::Time& dt) override;

    void hitByArrow(int32_t arrowDamage);
    void debugSlay() { m_hitPoints = 0; }

    [[nodiscard]] auto getName() -> std::string_view;

protected:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override
    {
        NCE_UNUSED(target);
        NCE_UNUSED(states);
    }
    auto getPlayer() { return m_player; }
    virtual sf::Transformable* getTransformable() { return nullptr; }
    void setDamageModifier(int32_t damageModifier) { m_damageModifier = damageModifier; }
    virtual void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go);
    [[nodiscard]] auto getAvailableProjectile(std::optional<BossProjectile::ProjectileType> typeFilter = std::nullopt) -> BossProjectile*;

private:
    PlayerCharacter* m_player;
    int32_t m_hitPoints;
    int32_t m_maxHitPoints;
    std::string m_name;

    int32_t m_damageModifier { 1 };
    std::vector<BossProjectile*> m_projectiles;
};
