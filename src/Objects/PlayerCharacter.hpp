#pragma once

#include <nce/CollisionSolver.hpp>
#include <nce/GameObject.hpp>

#include <nce/AnimatedSprite.hpp>
#include <nce/Collisions.hpp>
#include <optional>
#include <vector>

class ArrowProjectile;

class PlayerCharacter : public nce::GameObject {
public:
    PlayerCharacter(nce::AppState* currentStatem, const std::vector<ArrowProjectile*>& projectiles);
    ~PlayerCharacter();

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT)
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    void setDebugInvincibility(bool invincible) { m_isDebugInvincibility = invincible; }
    void lockPlayerControls();
    void unlockPlayerControls();
    void reset();
    void resetDirectionChangeCounter() { m_directionChangeCounter = 0; }

    [[nodiscard]] auto isInvincible() const -> bool { return m_isInvincibleFrames; }
    [[nodiscard]] auto getPosition() const -> sf::Vector2f { return m_sprite.getPosition(); }
    [[nodiscard]] auto isDashing() const -> bool { return m_dashActive; }
    [[nodiscard]] auto getXDirection() const -> float;
    [[nodiscard]] auto getDashCooldown() const -> sf::Time;
    [[nodiscard]] auto getDirectionChangeCounter() const -> std::uint32_t { return m_directionChangeCounter; }
    [[nodiscard]] auto wasPlayerHit() const -> bool { return m_wasHit; }
    [[nodiscard]] auto isDebugInvincible() const -> bool { return m_isDebugInvincibility; }
    [[nodiscard]] auto getGlobalBounds() const -> sf::FloatRect;
    [[nodiscard]] auto isAiming() const { return m_isAiming; }
    [[nodiscard]] auto getAimChargePercentage() const -> float;

    [[nodiscard]] auto getTotalPlayerDeaths() { return m_totalDeaths; }
    [[nodiscard]] auto getTotalPlayerShotsFired() { return m_totalShotsFired; }

    void recordPlayerDeath() { ++m_totalDeaths; }

protected:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    enum class AnimationStage : uint32_t { Idle, Run, Roll };

    void constructAnimations();
    void handleShootControls(const sf::Time& dt);
    void handleMovementControls(const sf::Time& dt);
    void checkBounds();
    void dashUpdate(const sf::Time& dt);
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go);
    void setAnimation(AnimationStage animStage, bool playOnChange = false);

    auto getFreeProjectile() const -> std::optional<std::size_t>;

    nce::AnimatedSprite m_sprite;
    std::vector<ArrowProjectile*> m_projectiles;

    std::unordered_map<AnimationStage, nce::Animation> m_animations;
    std::unordered_map<AnimationStage, sf::Time> m_animationFrameTimes;

    sf::Vector2f m_direction;
    sf::Vector2f m_velocity;

    float m_dashBoost { 1.f };
    float m_dashDirection { 0.f };

    sf::Time m_dashTimer;
    sf::Time m_dashCooldown;
    sf::Time m_shootChargeUpTimer;
    std::uint32_t m_directionChangeCounter { 0 };
    AnimationStage m_animationStage { AnimationStage::Idle };

    nce::CollisionSolver::Callback m_cbWrapper;
    std::weak_ptr<nce::ColliderInfo> m_colliderInfo;

    static uint32_t m_totalDeaths;
    static uint32_t m_totalShotsFired;

    bool m_dashActive { false };
    bool m_isAiming { false };
    bool m_isInvincibleFrames { false };
    bool m_isDebugInvincibility { false };
    bool m_wasHit { false };
    bool m_controlsLocked { false };
    bool m_facingRight { true };
    bool m_triggeredThisDash { false };
};
