#include "Explosion.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/AppState.hpp>

constexpr auto BORDER_THICKNESS { 0.25f };
constexpr auto SHAPE_RADIUS { 0.5f };

Explosion::Explosion(nce::AppState* currentState, const sf::Vector2f& spawnPosition)
    : nce::GameObject(currentState)
    , m_shape(SHAPE_RADIUS)
    , m_colliderInfo(nce::MakeCircleCollider(SHAPE_RADIUS + BORDER_THICKNESS, &m_shape, this))
{
    m_shape.setOrigin({ SHAPE_RADIUS, SHAPE_RADIUS });
    m_shape.setPosition(spawnPosition);

    m_shape.setFillColor(sf::Color::Transparent);
    m_shape.setOutlineThickness(0.25f);
    m_shape.setOutlineColor(sf::Color::Yellow);

    auto bossState = static_cast<BossStateBase*>(currentState);
    assert(bossState);
    if (!bossState->getSolver().addCollider(m_colliderInfo))
        throw std::runtime_error("Unable to create collider for explosion");

    setIdentifier("explosion");
}

Explosion::~Explosion()
{
    auto bossState = static_cast<BossStateBase*>(getAppState());
    assert(bossState);
    bossState->getSolver().removeCollider(m_colliderInfo);
}

void Explosion::update(const sf::Time& dt)
{
    m_aliveTime += dt;

    const auto scaleUpSpeed = getWorldFloatVar("explosion_scale_up_speed");
    const auto explosionDuration = sf::seconds(getWorldFloatVar("explosion_duration"));
    m_shape.setScale(m_shape.getScale() + (sf::Vector2f(scaleUpSpeed, scaleUpSpeed) * dt.asSeconds()));

    if (m_aliveTime > explosionDuration)
        setActive(false);
}

void Explosion::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }
