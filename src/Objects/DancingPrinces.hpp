#pragma once

#include "BossBase.hpp"

#include <nce/AnimatedSprite.hpp>
#include <nce/Animation.hpp>
#include <nce/Collisions.hpp>
#include <nce/TweeningAnimation.hpp>

#include <array>
#include <vector>

class PlayerCharacter;
class BossProjectile;

constexpr auto PRINCES_ENTITY_COUNT { 2 };
constexpr size_t LEFT_BOSS_INDEX { 0 };
constexpr size_t RIGHT_BOSS_INDEX { 1 };

class DancingPrinces : public BossBase {
public:
    enum class BossPhase { Phase1, Phase2 };
    DancingPrinces(nce::AppState* currentState,
                   PlayerCharacter* player,
                   const std::vector<BossProjectile*>& projectiles,
                   const std::array<sf::FloatRect, 3>& regions);

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    virtual void reset() override;

    void setZoomOutFinished();

    [[nodiscard]] auto isLeftBossActive() const { return m_bossStates[0] == BossState::Active; }
    [[nodiscard]] auto getBossPhase() const { return m_currentPhase; }
    [[nodiscard]] auto readyToShieldSwap() const { return m_shotsOnTargetCount == 8u; }

    void swapShields();
    void forcePhase1Swap();

protected:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    enum class BossState { Active, Acid, Shielded };

    void generateTargetPosition(size_t index);
    void constructOrResetTimer(std::string_view name);
    void updatePhase1(const sf::Time& dt);
    void updatePhase2(const sf::Time& dt);

    void tickBoss(size_t index, const sf::Time& dt);

    void setupMoveableRegions();

    void handleCollisionsLeftBoss(const nce::Manifold& manifold, nce::GameObject* const go);
    void handleCollisionsRightBoss(const nce::Manifold& manifold, nce::GameObject* const go);
    void generincHandleCollisions(size_t bossIndex, const nce::Manifold& manifold, nce::GameObject* const go);
    auto getActiveBossIndex() const -> std::optional<size_t>;
    auto getOtherBossIndex(size_t idx) const -> size_t;

    void handleAcid(size_t idx, const sf::Time& dt);

    sf::Vector2f getPredictedShotPosition(size_t bossIndex);

    void fireLaser(size_t bossIndex);
    void changeLeafSpawnPosition(sf::Vector2f& position);

    std::array<sf::RectangleShape, PRINCES_ENTITY_COUNT> m_bossShapes;
    std::array<sf::FloatRect, 3> m_regions;
    std::array<std::weak_ptr<nce::ColliderInfo>, PRINCES_ENTITY_COUNT> m_colliderInfo;
    std::array<sf::FloatRect, PRINCES_ENTITY_COUNT> m_moveableRegions;
    std::array<sf::Vector2f, PRINCES_ENTITY_COUNT> m_moveTargets;
    std::array<BossState, PRINCES_ENTITY_COUNT> m_bossStates;
    std::array<sf::Vector2f, 3> m_leafSpawnPoints;

    sf::RectangleShape m_bossShield;
    BossPhase m_currentPhase {};

    nce::CollisionSolver::Callback m_cbWrapperLeft;
    nce::CollisionSolver::Callback m_cbWrapperRight;

    uint32_t m_shotsOnTargetCount { 0 };
    sf::VertexArray m_debugLine;

    float m_acidDirection { 0.f };
    float m_acidSpacing { 0.f };
    sf::Vector2f m_acidTarget { 0.f, 0.f };
    int m_acidCounter { 0 };

    int m_lastLeafSpawn = 0;

    bool m_zoomOutFinished { false };
    bool m_firstAcidBomb { false };
};
