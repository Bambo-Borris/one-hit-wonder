#pragma once

#include "BossBase.hpp"
#include "SplittableProjectile.hpp"

#include <nce/AnimatedSprite.hpp>

constexpr size_t SPLIT_PROJECTILES_COUNT { 30 };

class Unnamed : public BossBase {
public:
    Unnamed(nce::AppState* appState, PlayerCharacter* player, const sf::Vector2f& playAreaSize, const std::vector<BossProjectile*>& projectiles);
    ~Unnamed();

    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    virtual void reset() override;

protected:
    void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    enum class AnimationStage : uint32_t { Idle, AnimationStagesCount };
    enum class Phase { BasicAttacking };

    void constructAnimations();
    void updateBasicAttacking(const sf::Time& dt);
    void setAnimationStage(AnimationStage stage);
    void handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go) override;
    void throwBoulder();

    std::array<nce::Animation, size_t(AnimationStage::AnimationStagesCount)> m_animations;
    std::array<sf::Time, size_t(AnimationStage::AnimationStagesCount)> m_animationTimes;

    nce::CollisionSolver::Callback m_cbWrapper;
    std::weak_ptr<nce::ColliderInfo> m_colliderInfo;

    std::array<SplittableProjectile*, SPLIT_PROJECTILES_COUNT> m_splittables;

    AnimationStage m_animationStage { AnimationStage::Idle };
    Phase m_phase { Phase::BasicAttacking };
    nce::AnimatedSprite m_sprite;
    bool m_floatingRecentre { false };
};
