#include "Floor.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/Collisions.hpp>

Floor::Floor(nce::AppState* currentState)
    : GameObject(currentState)
{
    auto bossState { dynamic_cast<BossStateBase*>(currentState) };
    assert(bossState);

    const auto groundHeight { getWorldFloatVar("ground_height") };
    m_shape = sf::RectangleShape { { bossState->getPlayAreaSize().width, groundHeight * 2.f } };

    auto texture = nce::AssetHolder::get().getTexture("bin/textures/floor.png");
    assert(texture);
    if (!texture)
        throw std::runtime_error("Unable to load floor floor texture");

    setIdentifier("floor");
    m_shape.setOrigin(m_shape.getSize() * 0.5f);
    m_shape.setPosition(
        { m_shape.getSize().x / 2.f, ((bossState->getPlayAreaSize().top + bossState->getPlayAreaSize().height) + m_shape.getSize().y / 2.f) - groundHeight });
    m_shape.setTexture(texture);

    m_colliderInfo = nce::MakeAABBCollider(m_shape.getSize() * 0.5f, &m_shape, this);

    auto& solver { bossState->getSolver() };

    if (!solver.addCollider(m_colliderInfo))
        throw std::runtime_error("Unable to create collider for floor");
}

void Floor::update(const sf::Time& dt)
{
    NCE_UNUSED(dt);

    auto bossState = dynamic_cast<BossStateBase*>(getAppState());
    assert(bossState);

    if (m_shape.getSize().x != bossState->getPlayAreaSize().getSize().x) {
        m_shape.setSize({ bossState->getPlayAreaSize().getSize().x, m_shape.getSize().y });
        m_shape.setOrigin(m_shape.getSize() * 0.5f);
        (*m_colliderInfo) = (*nce::MakeAABBCollider(m_shape.getSize() * 0.5f, &m_shape, this));
    }
}

void Floor::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }
