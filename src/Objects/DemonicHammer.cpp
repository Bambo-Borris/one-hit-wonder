#include "DemonicHammer.hpp"
#include "../Effects/CameraShake.hpp"
#include "../GameplayConstants.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "BossProjectile.hpp"
#include "PlayerCharacter.hpp"

#include <nce/App.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/Collisions.hpp>
#include <nce/TimerController.hpp>

/**********************************************************************
Timers available:
==================
    - nail_barrage_cooldown_timer = Cooldown for nail barrage (in
      seconds)
    - smash_down_cooldown_timer = Cooldown timer for smash down (in
      seconds)
    - falling_rain_cooldown_timer = Cooldown timer for placing falling
      rain (in seconds)
**********************************************************************/

constexpr auto DEMONIC_EYE_SIZE { sf::Vector2f { 300, 300 } };
constexpr auto SPRITE_DIM { sf::Vector2i { 100, 100 } };
constexpr auto Y_OFFSET { 150.f };

DemonicHammer::DemonicHammer(nce::AppState* appState,
                             PlayerCharacter* player,
                             const sf::Vector2f& playAreaSize,
                             const std::vector<BossProjectile*>& projectiles)
    : BossBase(appState, player, int32_t(getBossesDemonicHammerFloatVar("hit_points")), projectiles, gc::BOSS_NAMES[gc::DEMONIC_HAMMER_INDEX])
    , m_playAreaSize(playAreaSize)
    , m_cbWrapper(std::bind(&DemonicHammer::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
{
    setIdentifier("demonic_hammer");

    constructAnimations();
    setAnimation(AnimationStage::Idle);
    m_sprite.setOrigin(sf::Vector2f { SPRITE_DIM } / 2.f);
    m_sprite.setScale(DEMONIC_EYE_SIZE.cwiseDiv(sf::Vector2f { SPRITE_DIM }));

    auto colliderInfo = nce::MakeOBBCollider((DEMONIC_EYE_SIZE / 2.f) * 0.15f, &m_sprite, this);
    m_colliderInfo = colliderInfo;

    auto bossState { dynamic_cast<BossStateBase*>(appState) };
    assert(bossState);

    if (bossState) {
        if (!bossState->getSolver().addCollider(colliderInfo))
            throw std::runtime_error("Unable to setup demonic hammer collider");

        if (!bossState->getSolver().subscribeNotification({ colliderInfo, &m_cbWrapper }))
            throw std::runtime_error("Unable to subscribe to demonic hammer collision events");
    }

    appState->getTimerController().addTimer("nail_barrage_cooldown_timer");
    appState->getTimerController().addTimer("smash_down_cooldown_timer");
    appState->getTimerController().addTimer("falling_rain_cooldown_timer");

    // Setup smash down animations map
    {
        using enum SmashDownState;
        m_smashDownStateAnimationsFloat[int(StuckInGround)] = nullptr;
        m_smashDownStateAnimationsVector[int(TeleportGrow)] = nullptr;
    }

    reset();
}

DemonicHammer::~DemonicHammer() { }

void DemonicHammer::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    target.draw(m_sprite, states);
    BossBase::draw(target, states);
}

void DemonicHammer::update(const sf::Time& dt)
{
    BossBase::update(dt);

    m_sprite.update(dt);

    switch (m_activityState) {
        using enum ActivityState;
    case Flying:
        updateFlying(dt);
        break;
    case NailBarrage:
        updateNailBarrage();
        break;
    case SmashDown:
        updateSmashDown(dt);
        break;
    default:
        assert(false);
        break;
    }
}

void DemonicHammer::reset()
{
    BossBase::reset();
    setActivityState(ActivityState::Flying);
    getAppState()->getTimerController().resetTimer("smash_down_cooldown_timer");
    getAppState()->getTimerController().resetTimer("falling_rain_cooldown_timer");

    m_flyingRight = false;
    m_sprite.setPosition({ m_playAreaSize.x * 0.5f, Y_OFFSET });
    m_cameraShake.reset(nullptr);
    setAnimation(AnimationStage::Idle, true);
}

void DemonicHammer::constructAnimations()
{
    auto spritesheet = nce::AssetHolder::get().getTexture("bin/textures/golem_sheet.png");
    assert(spritesheet);
    if (!spritesheet)
        throw std::runtime_error("Unable to load demonic hammer spritesheet");

    m_animationFrameTimes[AnimationStage::Idle] = sf::milliseconds(100);
    m_animations[AnimationStage::Idle].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Idle].addFrame({ { 0 * SPRITE_DIM.x, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { 1 * SPRITE_DIM.x, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { 2 * SPRITE_DIM.x, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { 3 * SPRITE_DIM.x, 0 * SPRITE_DIM.y }, SPRITE_DIM });

    m_animationFrameTimes[AnimationStage::Charging] = sf::milliseconds(125);
    m_animations[AnimationStage::Charging].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Charging].addFrame({ { 0 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 1 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 2 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 3 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 4 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 5 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 6 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Charging].addFrame({ { 7 * SPRITE_DIM.x, 1 * SPRITE_DIM.y }, SPRITE_DIM });

    m_animationFrameTimes[AnimationStage::Slam] = sf::milliseconds(100);
    m_animations[AnimationStage::Slam].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Slam].addFrame({ { 0 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 1 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 2 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 3 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 4 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 5 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 6 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Slam].addFrame({ { 7 * SPRITE_DIM.x, 3 * SPRITE_DIM.y }, SPRITE_DIM });
}

void DemonicHammer::setActivityState(ActivityState nextState)
{
    static sf::Vector2f initialColliderSize {};

    switch (nextState) {
        using enum ActivityState;
    case Flying:
        getAppState()->getTimerController().resetTimer("nail_barrage_cooldown_timer");
        setAnimation(AnimationStage::Idle, true);

        if (m_activityState == SmashDown) {
            auto& metadata = std::get<nce::OBBMetadata>(m_colliderInfo.lock()->metadata);
            metadata.halfBounds = initialColliderSize;
        }
        break;
    case NailBarrage:
        setAnimation(AnimationStage::Charging, true);
        break;
    case SmashDown: {
        auto& metadata = std::get<nce::OBBMetadata>(m_colliderInfo.lock()->metadata);
        initialColliderSize = metadata.halfBounds;
        metadata.halfBounds *= 0.60f;
        setAnimation(AnimationStage::Slam, true);
        m_smashDownState = SmashDownState::Falling;
        m_didHitFloor = false;
    } break;
    default:
        assert(false);
        break;
    }

    m_activityState = nextState;
}

void DemonicHammer::updateFlying(const sf::Time& dt)
{
    // Firstly we'll handle our logic for the flying
    const auto flySpeed = getBossesDemonicHammerFloatVar("fly_speed");
    !m_flyingRight ? m_flyingDirection.x = -1.f : m_flyingDirection.x = 1.f;

    if (m_colliderInfo.expired())
        throw std::runtime_error("Unable to access demonic hammer collider");

    auto colliderInfo { m_colliderInfo.lock() };

    const auto bounds { nce::BoundingBoxFromCollider(colliderInfo.get()) };
    const auto offScreenLeft { bounds.left <= 0.f };
    const auto offScreenRight { bounds.left + bounds.width >= m_playAreaSize.x };

    if (offScreenLeft || offScreenRight) {
        m_flyingRight = !m_flyingRight;
        // We need to resolve the collision and move the shape back
        // so that we don't jitter at the screen edge
        if (offScreenLeft) {
            m_sprite.move({ 0.5f + (-bounds.left), 0.f });
        } else {
            m_sprite.move({ (m_playAreaSize.x - (bounds.left + bounds.width)) - 0.5f, 0.f });
        }
    } else {
        m_sprite.move(m_flyingDirection * flySpeed * dt.asSeconds());
    }

    // Check if we could do a smash down (this has a higher priority than nail barrage)
    // else check if we could do a nail barrage mode
    const auto smashDownCooldown { getBossesDemonicHammerFloatVar("smash_down_cooldown") };
    if (getAppState()->getTimerController().getElapsed("smash_down_cooldown_timer") >= sf::seconds(smashDownCooldown)) {
        const auto smashDownMaxDistance { getBossesDemonicHammerFloatVar("smash_down_max_dist") };
        if (std::abs((getPlayer()->getPosition() - m_sprite.getPosition()).x) < smashDownMaxDistance) {
            setActivityState(ActivityState::SmashDown);
            getAppState()->getTimerController().resetTimer("smash_down_cooldown_timer");
            return;
        }
    }

    if (getAppState()->getTimerController().getElapsed("nail_barrage_cooldown_timer") >= sf::seconds(getBossesDemonicHammerFloatVar("nail_barrage_cooldown"))) {
        const auto nailBarrageMaxDist { getBossesDemonicHammerFloatVar("nail_barrage_max_dist") };
        if (std::abs(getPlayer()->getPosition().x - m_sprite.getPosition().x) <= nailBarrageMaxDist) {
            setActivityState(ActivityState::NailBarrage);
        }
    }

    if (getAppState()->getTimerController().getElapsed("falling_rain_cooldown_timer").asSeconds()
        > getBossesDemonicHammerFloatVar("falling_rain_emit_cooldown")) {
        getAppState()->getTimerController().resetTimer("falling_rain_cooldown_timer");
        auto projectile { getAvailableProjectile({ BossProjectile::ProjectileType::Bomb }) };
        projectile->fire(m_sprite.getPosition(), m_sprite.getPosition() + sf::Vector2f { 0.f, 2000.f }, nullptr);
    }
}

void DemonicHammer::updateNailBarrage()
{
    // If we're no longer playing our animation, we've completed it
    // so we can file the nail barrage, then change our activity state
    if (!m_sprite.isPlaying()) {
        fireNailBarrageProjectiles();
        setActivityState(ActivityState::Flying);
        getAppState()->getTimerController().resetTimer("falling_rain_cooldown_timer");
    }
}

void DemonicHammer::updateSmashDown(const sf::Time& dt)
{
    // We must follow a strict sequence of events when we're doing this smash down:
    // 1. Fly to the ground
    // 2. If we hit the player, they will fail the level.
    // 3. (If we didn't hit the player) wait momentarily in the ground
    // 4. Shrink
    // 5. Grow back at the top-centre of the screen and then continue flying
    const auto stateIdx { int(m_smashDownState) };
    auto timerController = &getAppState()->getTimerController();

    switch (m_smashDownState) {
        using enum SmashDownState;
    case Falling:
        if (!m_didHitFloor) {
            const auto smashDownSpeed { getBossesDemonicHammerFloatVar("smash_down_speed") };
            m_sprite.move({ 0.f, smashDownSpeed * dt.asSeconds() });
        } else {
            m_smashDownState = StuckInGround;
            const auto wobblePeriodMax { getBossesDemonicHammerFloatVar("stuck_in_ground_time") };
            const auto shakeIntensity { getBossesDemonicHammerFloatVar("shake_effect_intensiy") };
            m_cameraShake
                = std::make_unique<CameraShake>(getAppState()->getApp()->getRenderTexture(), *timerController, sf::seconds(wobblePeriodMax), shakeIntensity);
            assert(m_cameraShake);

            if (!timerController->exists("wobble_period"))
                timerController->addTimer("wobble_period");
            else
                timerController->resetTimer("wobble_period");

            return;
        }
        break;
    case StuckInGround: {
        constexpr auto minOffset { -1.5f };
        constexpr auto maxOffset { 1.5f };
        constexpr auto wobbleSwayTime { sf::seconds(0.1f) };
        auto& animPtr = m_smashDownStateAnimationsFloat[stateIdx];

        if (!animPtr) {
            // TODO add wobble sway duration to bosses.dat
            animPtr = std::make_unique<nce::TAFloat>(nce::AnimationCurve::Lerp, "smash_down_wobble", minOffset, maxOffset, wobbleSwayTime, timerController);
            animPtr->play();
        }

        const auto wobblePeriodMax { getBossesDemonicHammerFloatVar("stuck_in_ground_time") };
        if (getAppState()->getTimerController().getElapsed("wobble_period") > sf::seconds(wobblePeriodMax) && animPtr->isComplete()) {
            animPtr.reset();
            getAppState()->getTimerController().removeTimer("wobble_period");
            m_smashDownState = TeleportShrink;
            return;
        }

        if (animPtr->isComplete()) {
            const auto currentStart { animPtr->getStart() };
            const auto nextStart { (currentStart == minOffset) ? maxOffset : minOffset };
            const auto nextEnd { (currentStart == minOffset) ? minOffset : maxOffset };

            animPtr.reset();
            animPtr = std::make_unique<nce::TAFloat>(nce::AnimationCurve::Lerp, "smash_down_wobble", nextStart, nextEnd, wobbleSwayTime, timerController);
            animPtr->play();
        }
        m_sprite.setRotation(sf::degrees(0.f + animPtr->getTweenedValue()));
    } break;
    case TeleportShrink: {
        auto& animPtr = m_smashDownStateAnimationsVector[stateIdx];
        if (!animPtr) {
            const auto scale { DEMONIC_EYE_SIZE.cwiseDiv(sf::Vector2f { SPRITE_DIM }) };
            animPtr = std::make_unique<nce::TAVec2f>(
                nce::AnimationCurve::Lerp, "smash_down_shrink", scale, sf::Vector2f { 0.f, 0.f }, sf::seconds(0.25f), timerController);
            animPtr->play();
        }
        m_sprite.setScale(animPtr->getTweenedValue());

        if (animPtr->isComplete()) {
            animPtr.reset();
            m_sprite.setPosition({ m_playAreaSize.x / 2.f, Y_OFFSET });
            m_smashDownState = SmashDownState::TeleportGrow;
        }
    } break;
    case SmashDownState::TeleportGrow: {
        auto& animPtr = m_smashDownStateAnimationsVector[stateIdx];
        if (!animPtr) {
            const auto scale { DEMONIC_EYE_SIZE.cwiseDiv(sf::Vector2f { SPRITE_DIM }) };
            animPtr = std::make_unique<nce::TAVec2f>(
                nce::AnimationCurve::Lerp, "smash_down_grow", sf::Vector2f { 0.f, 0.f }, scale, sf::seconds(0.25f), timerController);
            animPtr->play();
        }

        m_sprite.setScale(animPtr->getTweenedValue());

        if (animPtr->isComplete()) {
            animPtr.reset();
            setActivityState(ActivityState::Flying);
            getAppState()->getTimerController().resetTimer("smash_down_cooldown_timer");
            // Need to reset this here so boss doesn't start dropping rain
            // straight after a smash down
            getAppState()->getTimerController().resetTimer("falling_rain_cooldown_timer");
            m_cameraShake.reset();
            m_sprite.setRotation(sf::degrees(0.f));
        }
    } break;
    default:
        assert(false);
        break;
    }

    if (m_cameraShake) {
        if (!m_cameraShake->isCompleted())
            m_cameraShake->update();
    }
}

void DemonicHammer::setAnimation(AnimationStage animation, bool playOnChange)
{
    if (!m_animations.contains(animation) || !m_animationFrameTimes.contains(animation))
        throw std::runtime_error("Unable to find correct animation for DemonicHammer");

    m_animationStage = animation;
    m_sprite.setAnimation(m_animations[animation]);
    m_sprite.setFrameTime(m_animationFrameTimes[animation]);

    switch (m_animationStage) {
    case AnimationStage::Idle:
        m_sprite.setLooped(true);
        break;
    case AnimationStage::Charging:
        m_sprite.setLooped(false);
        break;
    case AnimationStage::Slam:
        m_sprite.setLooped(false);
        break;
    }

    if (playOnChange) {
        m_sprite.stop();
        m_sprite.play();
    }
}

void DemonicHammer::fireNailBarrageProjectiles()
{
    // Fire requires an end point, so we just use this to generate one super far away
    // since anything that hits the floor or goes off the edges will be destroyed anyway!
    constexpr auto ensureOffscreenMultiplier { 3000.f };
    const auto projectileCount { getBossesDemonicHammerFloatVar("barrage_projectile_count") };
    const auto& centre { m_sprite.getPosition() };

    if (m_colliderInfo.expired())
        throw std::runtime_error("Unable to get demonic hammer collider info");

    auto bounds { nce::BoundingBoxFromCollider(m_colliderInfo.lock().get()) };

    const auto radius { std::max(bounds.width, bounds.height) / 2.f };

    constexpr auto arcStartAngle { sf::degrees(45.f) };
    constexpr auto arcEndAngle { sf::degrees(135.f) };
    const auto step { sf::degrees((arcEndAngle - arcStartAngle).asDegrees() / projectileCount) };

    for (auto i { arcStartAngle }; i < arcEndAngle; i += step) {
        const auto startPoint { centre + radius * sf::Vector2f { 1.f, i } };
        const sf::Vector2f endPoint { ensureOffscreenMultiplier, i };
        auto projectile = getAvailableProjectile({ BossProjectile::ProjectileType::FlameBarrage });
        projectile->fire(startPoint, endPoint);
    }
}

void DemonicHammer::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    // If we're in smash down stuck in ground we'll double the modifier before we check if we
    // were hit by an arrow (handled in BossBase), then undo the modifier change so we don't
    // retain it beyond this collision check
    if (m_activityState == ActivityState::SmashDown && m_smashDownState == SmashDownState::StuckInGround)
        setDamageModifier(2);
    BossBase::handleCollisions(manifold, go);
    if (m_activityState == ActivityState::SmashDown && m_smashDownState == SmashDownState::StuckInGround)
        setDamageModifier(1);

    if (!m_didHitFloor) {
        if (go->getIdentifier() == "floor") {
            m_didHitFloor = true;
            m_sprite.move(-manifold.normal * manifold.penetration); // Move back so we're just touching the floor rather than intersecting
        }
    }
}
