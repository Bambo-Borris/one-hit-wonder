#include "SplittableProjectile.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "ArrowProjectile.hpp"

constexpr auto SPLITTABLE_PROJECTILE_SIZE { sf::Vector2f(52.f, 52.f) };
constexpr auto SMALL_SPLITTABLE_RADIUS { SPLITTABLE_PROJECTILE_SIZE.x / 4.f };

SplittableProjectile::SplittableProjectile(nce::AppState* currentState)
    : nce::GameObject(currentState)
    , m_shape(SPLITTABLE_PROJECTILE_SIZE.x / 2.f)
    , m_cbWrapper(std::bind(&SplittableProjectile::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
    , m_cbWrapperSplit(std::bind(&SplittableProjectile::handleSplitCollisions, this, std::placeholders::_1, std::placeholders::_2))
{
    m_shape.setOrigin({ SPLITTABLE_PROJECTILE_SIZE.x / 2.f, SPLITTABLE_PROJECTILE_SIZE.x / 2.f });
    m_shape.setPosition({ 100, 100 });
    setRenderLayer(4);

    for (auto& small : m_splitShapes) {
        small = sf::CircleShape(SMALL_SPLITTABLE_RADIUS);
        small.setOrigin({ SMALL_SPLITTABLE_RADIUS, SMALL_SPLITTABLE_RADIUS });
    }

    auto bossState = dynamic_cast<BossStateBase*>(currentState);
    assert(bossState);

    if (!bossState)
        throw std::runtime_error("Null boss state encountered");

    auto coll = nce::MakeCircleCollider(SPLITTABLE_PROJECTILE_SIZE.x / 2.f, &m_shape, this);
    if (!bossState->getSolver().addCollider(coll))
        throw std::runtime_error("Unable to add collider for SplittableProjectile");
    m_colliderInfo = coll;

    if (!bossState->getSolver().subscribeNotification({ coll, &m_cbWrapper }))
        throw std::runtime_error("Unable to subscribe to collision notifications for SplittableProjectile");

    for (auto& small : m_splitShapes) {
        auto smallColl = nce::MakeCircleCollider(SMALL_SPLITTABLE_RADIUS, &small, this);
        if (!bossState->getSolver().addCollider(smallColl))
            throw std::runtime_error("Unable to add collider for SplittableProjectile");

        if (!bossState->getSolver().subscribeNotification({ smallColl, &m_cbWrapperSplit }))
            throw std::runtime_error("Unable to subscribe to collision notifications for SplittableProjectile");
    }

    setIdentifier("splittable_projectile");
}

void SplittableProjectile::update(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    const auto speed = getBossesUnnamedFloatVar("boulder_speed");
    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    if (!m_split) {
        m_shape.move(m_direction * speed * dt.asSeconds());
        if (!bossState->getPlayAreaSize().contains(m_shape.getPosition()))
            setActive(false);
    } else {
        for (size_t i { 0 }; i < m_splitDirections.size(); ++i) {
            if (m_splitActivity[i])
                m_splitShapes[i].move(m_splitDirections[i] * speed * dt.asSeconds());
        }

        for (size_t i { 0 }; i < m_splitShapes.size(); ++i) {
            if (!bossState->getPlayAreaSize().contains(m_splitShapes[i].getPosition()))
                m_splitActivity[i] = false;
        }
        // Check if both are now dead, if so we'll stop ourselves
        auto findActiveResult = std::find(m_splitActivity.begin(), m_splitActivity.end(), true);
        if (findActiveResult == m_splitActivity.end())
            setActive(false);
    }
}

void SplittableProjectile::fire(const sf::Vector2f& direction, const sf::Vector2f& position)
{
    m_direction = direction;
    m_shape.setPosition(position);
    setActive(true);
    m_split = false;

    for (auto& small : m_splitShapes)
        small.setPosition({ -2000, -2000 });

    for (auto& activity : m_splitActivity)
        activity = true;
}

void SplittableProjectile::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (!m_split) {
        target.draw(m_shape, states);
    } else {
        for (size_t i { 0 }; i < m_splitShapes.size(); ++i)
            if (m_splitActivity[i])
                target.draw(m_splitShapes[i], states);
    }
}

void SplittableProjectile::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    NCE_UNUSED(manifold);
    NCE_UNUSED(go);

    if (go->getIdentifier() == "floor") {
        setActive(false);
    } else if (go->getIdentifier() == "arrow_projectile") {
        if (!m_split) {
            m_split = true;
            auto theta = getBossesUnnamedFloatVar("split_angle");
            m_splitDirections[0] = m_direction.rotatedBy(sf::degrees(-theta));
            m_splitDirections[1] = m_direction.rotatedBy(sf::degrees(theta));

            for (auto& small : m_splitShapes)
                small.setPosition(m_shape.getPosition());

            m_shape.setPosition({ -2000, -2000 });
        }
    }
}

void SplittableProjectile::handleSplitCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    NCE_UNUSED(manifold);
    NCE_UNUSED(go);

    if (!m_split)
        return;

    /*    if (go->getIdentifier() == "arrow_projectile") {
            auto arrow = dynamic_cast<ArrowProjectile*>(go);
            assert(arrow);
            if (!arrow)
                throw std::runtime_error("Encountered non arrow arrow >.<");

            auto dist = std::numeric_limits<float>::min();
            size_t index { 0 };
            for (size_t i { 0 }; i < m_splitShapes.size(); ++i) {
                const auto sqLength { (arrow->getPosition() - m_splitShapes[i].getPosition()).lengthSq() };
                if (dist < sqLength) {
                    index = i;
                    dist = sqLength;
                }
            }

            m_splitActivity[index] = false;

        } else*/
    if (go->getIdentifier() == "floor") {
        auto lowestY = std::numeric_limits<float>::min();
        size_t index { 0 };
        for (size_t i { 0 }; i < m_splitShapes.size(); ++i) {
            if (m_splitShapes[i].getPosition().y > lowestY) {
                lowestY = m_splitShapes[i].getPosition().y;
                index = i;
            }
        }

        m_splitActivity[index] = false;
    }
}
