#include "Unnamed.hpp"
#include "../GameplayConstants.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/TimerController.hpp>

constexpr auto SPRITE_INGAME_SIZE { sf::Vector2f { 192.f, 192.f } };
constexpr auto SPRITE_IMAGE_DIMENSION { sf::Vector2i { 32, 32 } };

// Timers available

constexpr auto BASIC_ATTACK_TIMER_NAME { "unnamed_basic_attack_cd" };

Unnamed::Unnamed(nce::AppState* appState, PlayerCharacter* player, const sf::Vector2f& playAreaSize, const std::vector<BossProjectile*>& projectiles)
    : BossBase(appState, player, 100, projectiles, gc::BOSS_NAMES[gc::UNNAMED_INDEX])
    , m_cbWrapper(std::bind(&Unnamed::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
{
    NCE_UNUSED(playAreaSize);

    setIdentifier("unnamed_boss");

    constructAnimations();
    setAnimationStage(AnimationStage::Idle);

    m_sprite.setOrigin(sf::Vector2f(SPRITE_IMAGE_DIMENSION) / 2.f);
    m_sprite.setScale(SPRITE_INGAME_SIZE.cwiseDiv(m_sprite.getLocalBounds().getSize()));

    auto colliderInfo = nce::MakeOBBCollider(sf::Vector2f { SPRITE_IMAGE_DIMENSION } * 0.5f, &m_sprite, this);
    m_colliderInfo = colliderInfo;

    auto bossState { dynamic_cast<BossStateBase*>(appState) };
    assert(bossState);

    auto& solver { bossState->getSolver() };
    if (!solver.addCollider(colliderInfo))
        throw std::runtime_error("Unable to add collider");

    if (!solver.subscribeNotification({ colliderInfo, &m_cbWrapper }))
        throw std::runtime_error("Unable to subscribe for collider notifications");

    for (auto& sp : m_splittables) {
        sp = new SplittableProjectile(appState);
        sp->setActive(false);
        if (!appState->addGameObject(sp))
            throw std::runtime_error("Unable to allocate Splittable Projectile");
    }

    getAppState()->getTimerController().addTimer(BASIC_ATTACK_TIMER_NAME, true);

    reset();
}

Unnamed::~Unnamed() { }

void Unnamed::update(const sf::Time& dt)
{
    m_sprite.update(dt);

    switch (m_phase) {
        using enum Phase;
    case BasicAttacking:
        updateBasicAttacking(dt);
        break;
    default:
        assert(false && "Missing phase implementation or invalid phase value");
        break;
    }
}

void Unnamed::reset()
{
    BossBase::reset();
    setAnimationStage(AnimationStage::Idle);
    auto bossState = dynamic_cast<BossStateBase*>(getAppState());
    assert(bossState);
    if (!bossState)
        throw std::runtime_error("Null boss state identified");

    m_sprite.setPosition(bossState->getPlayAreaSize().getPosition() + (bossState->getPlayAreaSize().getSize() / 2.f));
    for (auto& sp : m_splittables)
        sp->setActive(false);

    getAppState()->getTimerController().resetTimer(BASIC_ATTACK_TIMER_NAME, false);
    getAppState()->getTimerController().playTimer(BASIC_ATTACK_TIMER_NAME);
}

void Unnamed::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_sprite, states); }

void Unnamed::constructAnimations()
{
    auto texture = nce::AssetHolder::get().getTexture("bin/textures/ArchFiendIdleFront.png");
    if (!texture)
        throw std::runtime_error("Unable to load 4th boss texture");

    m_animationTimes[size_t(AnimationStage::Idle)] = sf::milliseconds(150);
    m_animations[size_t(AnimationStage::Idle)].setSpriteSheet(*texture);

    for (int i(0); i < 8; ++i)
        m_animations[size_t(AnimationStage::Idle)].addFrame({ { i * SPRITE_IMAGE_DIMENSION.x, 0 * SPRITE_IMAGE_DIMENSION.y }, SPRITE_IMAGE_DIMENSION });
}

void Unnamed::updateBasicAttacking(const sf::Time& dt)
{
    NCE_UNUSED(dt);
    // Determine the angle of the vector pointing from the boss to the player
    const auto playerPos = getPlayer()->getPosition();
    const auto direction = (playerPos - m_sprite.getPosition()).normalized();
    const auto angleBetween = std::abs(direction.angleTo(sf::Vector2f { 0.f, 1.f }).asDegrees());

    const auto moveBy = [this, &direction](float speedScale, float dt) -> void {
        const auto speed = getBossesUnnamedFloatVar("floating_speed");
        if (direction.x < 0.f)
            m_sprite.move({ -speed * dt * speedScale, 0.f });
        else
            m_sprite.move({ speed * dt * speedScale, 0.f });
    };

    if (angleBetween > 40.f) {
        m_floatingRecentre = true;
        moveBy(1.f, dt.asSeconds());
    } else {
        if (m_floatingRecentre) {
            if (angleBetween > 25.f)
                moveBy(1.5f, dt.asSeconds());
            else
                m_floatingRecentre = false;
        }
    }

    const auto basicAttackCd = getBossesUnnamedFloatVar("basic_attack_cooldown");
    if (getAppState()->getTimerController().getElapsed(BASIC_ATTACK_TIMER_NAME) > sf::seconds(basicAttackCd)) {
        // Fire projectile here
        auto result = std::find_if(m_splittables.begin(), m_splittables.end(), [](SplittableProjectile* go) { return !go->isActive(); });
        if (result != m_splittables.end()) {
            (*result)->fire(direction, m_sprite.getPosition());
        } else {
            assert(false && "Ran out of split projectiles");
        }

        getAppState()->getTimerController().resetTimer(BASIC_ATTACK_TIMER_NAME);
    }
}

void Unnamed::setAnimationStage(AnimationStage stage)
{
    m_sprite.stop();

    switch (stage) {
    case AnimationStage::Idle:
        m_sprite.setLooped(true);
        break;
    }

    m_sprite.setFrameTime(m_animationTimes[size_t(stage)]);
    m_sprite.setAnimation(m_animations[size_t(stage)]);
    m_sprite.play(m_animations[size_t(stage)]);
    m_animationStage = stage;
}

void Unnamed::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go) { BossBase::handleCollisions(manifold, go); }

void Unnamed::throwBoulder() { }
