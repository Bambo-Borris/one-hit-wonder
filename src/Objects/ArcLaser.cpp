#include "ArcLaser.hpp"
#include "../Effects/CameraShake.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/App.hpp>
#include <nce/MathsUtils.hpp>
#include <nce/TimerController.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <algorithm>
#include <cassert>
#include <cmath>

ArcLaser::ArcLaser(nce::AppState* currentState)
    : GameObject(currentState)
{
    setIdentifier("arc_laser");

    auto bossState { dynamic_cast<BossStateBase*>(currentState) };
    assert(bossState);

    const auto playAreaSize { bossState->getPlayAreaSize() };
    m_shape = sf::RectangleShape { { std::max(playAreaSize.width, playAreaSize.height) * 1.5f, 48.f } };

    m_shape.setFillColor(sf::Color::White);
    m_shape.setOrigin({ m_shape.getSize() / 2.f });
    m_shape.setPosition(playAreaSize.getPosition() + playAreaSize.getSize() * 0.5f);

    auto colliderInfo = nce::MakeOBBCollider(m_shape.getSize() * 0.5f, &m_shape, this);
    if (!bossState->getSolver().addCollider(colliderInfo))
        throw std::runtime_error("Unable to add collider to solver!");
}

ArcLaser::~ArcLaser() { }

void ArcLaser::update(const sf::Time& dt)
{
    m_timer += dt;

    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    const auto playArea { bossState->getPlayAreaSize() };

    if (m_tracking)
        m_shape.setPosition(m_tracking->getPosition());
    else
        m_shape.setPosition(playArea.getPosition() + playArea.getSize() * 0.5f);

    const auto t = std::clamp((m_timer.asSeconds() / getBossesEvilEyeFloatVar("arc_laser_sweep_time")), 0.f, 1.f);
    const auto theta { std::lerp(m_startRotation, m_endRotation, t) };

    m_shape.setRotation(sf::degrees(theta));
    if (t >= 1.f)
        m_isFinished = true;

    if (m_cameraShake && !hasAnimationConcluded()) {
        m_cameraShake->update();
    } else {
        if (m_cameraShake)
            m_cameraShake.reset();
    }
}

void ArcLaser::activateSequence(float startAngle, float endAngle)
{
    m_startRotation = startAngle;
    m_endRotation = endAngle;
    m_timer = sf::Time::Zero;
    m_shape.setRotation(sf::degrees(startAngle));
    setActive(true);
    m_isFinished = false;

    const auto arcTime { sf::seconds(getBossesEvilEyeFloatVar("arc_laser_sweep_time")) };
    const auto shakeIntensity { getBossesEvilEyeFloatVar("shake_effect_intensiy") };

    if (m_cameraShake)
        m_cameraShake.reset();

    m_cameraShake = std::make_unique<CameraShake>(getAppState()->getApp()->getRenderTexture(), getAppState()->getTimerController(), arcTime, shakeIntensity);
}

void ArcLaser::setTracking(const sf::Transformable* transformable) { m_tracking = transformable; }

void ArcLaser::reset() { m_cameraShake.reset(nullptr); }

void ArcLaser::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }
