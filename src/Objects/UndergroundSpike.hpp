#pragma once

#include <nce/GameObject.hpp>
#include <nce/TweeningAnimation.hpp>

constexpr auto UNDERGROUND_SPIKE_SIZE { sf::Vector2f { 36.f, 256.f } };
constexpr auto SPIKE_SPACING { 20.f };

class UndergroundSpike : public nce::GameObject {
public:
    UndergroundSpike(nce::AppState* currentState, const sf::FloatRect& region, float xPosition);
    ~UndergroundSpike();

    // Inherited via GameObject
    virtual void update(const sf::Time& dt) override;
    virtual void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT) override
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    }

    void reset();

    // Begin a raise lower animation with an optional delay time
    // which can be used to delay when the sequence begins.
    void startRaise(const sf::Time& delay);
    void startLower(const sf::Time& delay);

    [[nodiscard]] auto isAnimating() const { return m_phase != AnimationPhase::Idle || m_raiseDelay != sf::Time::Zero; }
    [[nodiscard]] auto isAnimationQueued() const { return m_lowerDelay != sf::Time::Zero || m_raiseDelay != sf::Time::Zero; }
    [[nodiscard]] auto isRaiseSequence() const { return (isAnimating() && m_phase == AnimationPhase::Raise); }

private:
    enum class AnimationPhase { Idle, Raise, Lower };

    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;
    void setupRaise();
    void setupLower();

    sf::FloatRect m_region;
    sf::RectangleShape m_shape;
    std::string m_uuid;
    std::string m_delayRaiseTimerName;
    std::string m_delayLowerTimerName;
    std::string m_holdTimerName;

    nce::TweeningAnimation<sf::Vector2f> m_raiseTween;
    nce::TweeningAnimation<sf::Vector2f> m_lowerTween;

    sf::Time m_raiseDelay;
    sf::Time m_lowerDelay;

    float m_xPosition;
    AnimationPhase m_phase { AnimationPhase::Idle };
};
