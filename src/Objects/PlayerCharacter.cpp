#include "PlayerCharacter.hpp"
#include "../GameplayVars.hpp"
#include "../States/BossStateBase.hpp"
#include "../Utils/GameMetrics.hpp"
#include "../Utils/GameplayUtility.hpp"
#include "ArrowProjectile.hpp"
#include "Floor.hpp"
#include "UndergroundSpike.hpp"

#include <nce/App.hpp>
#include <nce/AssetHolder.hpp>
#include <nce/InputHandler.hpp>
#include <nce/TimerController.hpp>

#include <spdlog/spdlog.h>

constexpr auto MAX_BOW_PULLBACK_TIME { sf::seconds(0.7f) };
constexpr auto POST_DODGE_IFRAMES { 10 }; // How many frames does the player have iframes for post-dodging
constexpr auto SPRITE_DIM { sf::Vector2i(64, 64) };
constexpr auto COLLIDER_HALF_DIM { sf::Vector2f { 5.5f, 14.f } };

/**********************************************************************
Timers available:
==================
    - player_fire_cooldown_timer = Used to throttle player fire rate
**********************************************************************/

uint32_t PlayerCharacter::m_totalDeaths { 0 };
uint32_t PlayerCharacter::m_totalShotsFired { 0 };

PlayerCharacter::PlayerCharacter(nce::AppState* currentState, const std::vector<ArrowProjectile*>& projectiles)
    : nce::GameObject(currentState)
    , m_projectiles(projectiles)
    , m_cbWrapper(std::bind(&PlayerCharacter::handleCollisions, this, std::placeholders::_1, std::placeholders::_2))
{
    setIdentifier("player");
    constructAnimations();

    m_sprite.setAnimation(m_animations[AnimationStage::Idle]);
    m_sprite.setOrigin(sf::Vector2f(SPRITE_DIM) * 0.5f);
    m_sprite.setScale({ 2.0f, 2.0f });

    auto bossState { dynamic_cast<BossStateBase*>(currentState) };
    spdlog::debug("Global bounds {} {}", m_sprite.getGlobalBounds().getSize().x, m_sprite.getGlobalBounds().getSize().y);
    auto colliderInfo = nce::MakeOBBCollider(COLLIDER_HALF_DIM, &m_sprite, this);

    if (!bossState->getSolver().addCollider(colliderInfo))
        throw std::runtime_error("Unable to add collider to solver!");

    if (!bossState->getSolver().subscribeNotification({ colliderInfo, &m_cbWrapper }))
        throw std::runtime_error("Unable to subscribe to collider callback!");

    m_colliderInfo = colliderInfo;

    currentState->getTimerController().addTimer("player_fire_cooldown_timer");
    reset();
}

PlayerCharacter::~PlayerCharacter() { }

void PlayerCharacter::update(const sf::Time& dt)
{
    m_sprite.update(dt);
    dashUpdate(dt);
    handleMovementControls(dt);
    handleShootControls(dt);
    checkBounds();

    m_facingRight = (m_sprite.getScale().x / std::abs(m_sprite.getScale().x)) > 0.f;

    if (m_direction.x != 0.f && !m_dashActive) {
        if (m_animationStage != AnimationStage::Run)
            setAnimation(AnimationStage::Run, true);
    }

    if (!m_dashActive && m_velocity.x != 0.f) {
        const auto velocityFacingRight = m_velocity.x > 0.f;
        if (m_facingRight != velocityFacingRight) {
            m_sprite.setScale({ -m_sprite.getScale().x, m_sprite.getScale().y });
        } else {
            m_sprite.setScale({ m_sprite.getScale().x, m_sprite.getScale().y });
        }
    }

    if (m_direction.x == 0.f && !m_dashActive && m_velocity.x == 0.f) {
        if (m_animationStage != AnimationStage::Idle)
            setAnimation(AnimationStage::Idle);
    }
}

void PlayerCharacter::lockPlayerControls() { m_controlsLocked = true; }

void PlayerCharacter::unlockPlayerControls() { m_controlsLocked = false; }

void PlayerCharacter::reset()
{
    m_velocity = { 0.f, 0.f };
    m_dashBoost = 1.f;
    m_dashTimer = sf::Time::Zero;
    m_dashCooldown = sf::Time::Zero;
    m_shootChargeUpTimer = sf::Time::Zero;

    m_dashActive = false;
    m_isAiming = false;
    m_wasHit = false;
    m_facingRight = true;
    m_isInvincibleFrames = false;

    auto floor = getAppState()->getObjectOfType<Floor>();
    assert(floor);
    const auto& v { getAppState()->getApp()->getRenderTexture()->getView() };
    const sf::Vector2f position { v.getCenter().x, floor->getBoundsRect().top - 31.f };

    m_sprite.setPosition(position);
    getAppState()->getTimerController().resetTimer("player_fire_cooldown_timer");
    m_directionChangeCounter = 0;
    setAnimation(AnimationStage::Idle);
    m_sprite.play();

    m_velocity = {};
}

auto PlayerCharacter::getXDirection() const -> float { return m_direction.x; }

auto PlayerCharacter::getDashCooldown() const -> sf::Time { return m_dashCooldown; }

auto PlayerCharacter::getGlobalBounds() const -> sf::FloatRect
{
    assert(!m_colliderInfo.expired());
    if (m_colliderInfo.expired())
        throw std::runtime_error("Player collider was deleted");

    return nce::BoundingBoxFromCollider(m_colliderInfo.lock().get());
}

auto PlayerCharacter::getAimChargePercentage() const -> float { return m_shootChargeUpTimer / MAX_BOW_PULLBACK_TIME; }

void PlayerCharacter::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_sprite, states); }

void PlayerCharacter::constructAnimations()
{
    auto spritesheet = nce::AssetHolder::get().getTexture("bin/textures/spritesheet.png");
    assert(spritesheet);
    if (!spritesheet)
        throw std::runtime_error("Unable to load player spritesheet");

    // Idle Animation
    m_animations[AnimationStage::Idle].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Idle].addFrame({ { SPRITE_DIM.x * 0, 5 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { SPRITE_DIM.x * 1, 5 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { SPRITE_DIM.x * 2, 5 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Idle].addFrame({ { SPRITE_DIM.x * 3, 5 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animationFrameTimes[AnimationStage::Idle] = sf::milliseconds(250);

    // Run Animation
    m_animations[AnimationStage::Run].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 0, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 1, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 2, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 3, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 4, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 5, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 6, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Run].addFrame({ { SPRITE_DIM.x * 7, 0 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animationFrameTimes[AnimationStage::Run] = sf::milliseconds(125);

    // Slide Animation
    const auto dashTime { getPlayerFloatVar("dash_length") };

    m_animations[AnimationStage::Roll].setSpriteSheet(*spritesheet);
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 0, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 1, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 2, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 3, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 4, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 5, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animations[AnimationStage::Roll].addFrame({ { SPRITE_DIM.x * 6, 2 * SPRITE_DIM.y }, SPRITE_DIM });
    m_animationFrameTimes[AnimationStage::Roll] = sf::milliseconds(int32_t((dashTime * 1000.f) / 7.f));
}

void PlayerCharacter::handleShootControls(const sf::Time& dt)
{
    if (m_controlsLocked)
        return;

    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);

    auto& checker { bossState->getButtonActionChecker() };
    if (checker.checkAction("shoot_hold")) {
        if (!m_isAiming) {
            m_isAiming = true;
            bossState->getSfxManager().triggerSoundEvent("bow_draw");
        } else {
            if (m_shootChargeUpTimer < MAX_BOW_PULLBACK_TIME)
                m_shootChargeUpTimer += dt;
        }
    }

    // If the user released the fire button
    // and they were aiming, then we should fire the arrow
    const auto heldState { checker.checkAction("shoot_hold") };
    const auto releasedState { checker.checkAction("shoot_release") };

    static sf::Vector2f rightStickPos { 1.f, 0.f };
    auto& gamePad { nce::InputHandler::GetGamePad() };
    if (gamePad.hasValidConnectedDevice()) {
        const sf::Vector2f temp { gamePad.getAxisPosition(nce::GamePad::Axis::RightStickX), -gamePad.getAxisPosition(nce::GamePad::Axis::RightStickY) };
        if (temp != sf::Vector2f {})
            rightStickPos = temp;
    }

    if (!heldState && releasedState) {
        if (m_isAiming && getAppState()->getTimerController().getElapsed("player_fire_cooldown_timer") > sf::seconds(getPlayerFloatVar("fire_cooldown"))) {
            const auto projectileIndex = getFreeProjectile();

            // Note, If we hit this we need to up the projectile count!
            assert(projectileIndex);

            if (projectileIndex) {
                const auto projectileSpeed { getPlayerFloatVar("projectile_speed") };
                auto multiplier { (m_shootChargeUpTimer.asSeconds() / MAX_BOW_PULLBACK_TIME.asSeconds()) };
                multiplier = std::clamp(multiplier, 0.1f, 1.f);

                sf::Vector2f direction;
                if ((*releasedState) == nce::ButtonActionChecker::ActionTrigger::Keyboard) {
                    const auto mouseWorldPos = GetMouseRTWorldPos(getAppState()->getApp());
                    direction = (mouseWorldPos - m_sprite.getPosition());
                    if (direction != sf::Vector2f {})
                        direction = direction.normalized();
                    else
                        direction = { 0.f, -1.f };

                } else if ((*releasedState) == nce::ButtonActionChecker::ActionTrigger::GamePad) {
                    direction = rightStickPos;
                }

                auto projectilePtr { m_projectiles[*projectileIndex] };
                auto damage = int32_t(multiplier * getPlayerFloatVar("bow_damage"));
                damage = std::clamp(damage, 1, int32_t(getPlayerFloatVar("bow_damage")));

                assert(projectilePtr);
                if (projectilePtr) {
                    projectilePtr->fire(m_sprite.getPosition(), direction, (projectileSpeed * multiplier), damage);
                    GameMetrics::AddPlayerTotalShotsFired();
                    ++m_totalShotsFired;
                }
            }
            m_isAiming = false;
            m_shootChargeUpTimer = sf::Time::Zero;
            getAppState()->getTimerController().resetTimer("player_fire_cooldown_timer");
            bossState->getSfxManager().stopSoundEvent("bow_draw");
            bossState->getSfxManager().triggerSoundEvent("bow_fire");
        }
    } else if (!heldState && !releasedState) {
        m_isAiming = false;
        m_shootChargeUpTimer = sf::Time::Zero;
    }
}

void PlayerCharacter::handleMovementControls(const sf::Time& dt)
{
    if (m_controlsLocked)
        return;

    // Movement
    const auto previousDirection { m_direction };
    m_direction = {};
    const auto dashTime { getPlayerFloatVar("dash_length") };

    auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
    assert(bossState);
    auto& checker { bossState->getButtonActionChecker() };

    if (checker.checkAction("move_left"))
        m_direction.x = -1.f;
    else if (checker.checkAction("move_right"))
        m_direction.x = 1.f;
    else
        m_direction = {};

    if (m_direction != sf::Vector2f {} && previousDirection != m_direction)
        ++m_directionChangeCounter;

    if (checker.checkAction("dodge") && !m_dashActive && (m_dashCooldown <= sf::Time::Zero)) {
        if (m_direction.x == 0.f) {
            // If they don't pick a direction we'll just
            // dodge to the right
            m_dashDirection = nce::RNG::intWithinRange(0, 100) < 50 ? -1.f : 1.f;
        } else {
            m_dashDirection = m_direction.x;
        }
        m_dashActive = true;
        m_dashTimer = sf::seconds(dashTime);
        m_dashBoost = 2.5f;
        setAnimation(AnimationStage::Roll);
        m_sprite.play();

        bossState->getSfxManager().triggerSoundEvent("dodge");
    }

    const auto groundFriction { getWorldFloatVar("ground_friction") };
    const auto playerAcceleration { getPlayerFloatVar("acceleration") };
    const auto playerMaxMoveSpeed { getPlayerFloatVar("max_move_speed") };

    // Handle momentum based movement if we're not dashing
    if (!m_dashActive) {

        // Apply friction
        if (m_velocity.x > 0.f)
            m_velocity.x = std::max(0.f, m_velocity.x - groundFriction * playerAcceleration * dt.asSeconds());
        else if (m_velocity.x < 0.f)
            m_velocity.x = std::min(0.f, m_velocity.x + groundFriction * playerAcceleration * dt.asSeconds());

        // Apply acceleration
        if (checker.checkAction("move_left"))
            m_velocity.x = std::max(-playerMaxMoveSpeed, m_velocity.x - playerAcceleration * dt.asSeconds());
        else if (checker.checkAction("move_right"))
            m_velocity.x = std::min(playerMaxMoveSpeed, m_velocity.x + playerAcceleration * dt.asSeconds());

        if (m_direction.x < 0.f)
            m_velocity.x = std::max(-playerMaxMoveSpeed, m_velocity.x - playerAcceleration * dt.asSeconds());
        else if (m_direction.x > 0.f)
            m_velocity.x = std::min(playerMaxMoveSpeed, m_velocity.x + playerAcceleration * dt.asSeconds());

        m_sprite.move(m_velocity * dt.asSeconds());
    } else { // Handle dashing
        m_sprite.move(sf::Vector2f(m_dashDirection * (m_dashBoost * playerMaxMoveSpeed), 0.f) * dt.asSeconds());
    }
}

void PlayerCharacter::checkBounds()
{
    auto bossState = dynamic_cast<BossStateBase*>(getAppState());
    assert(bossState);

    // We do some convoluted logic here to discern where the far left side of the OBB collider is
    // it's massively wasteful as we generate all 4 vertices transformed, then compute the width (without
    // scale factored in) and then discern the lowest x position of all vertices. Lots of assumptions that
    // could break down if we scale the player.

    const auto verts = nce::ComputeOBBVerticesTransformed(COLLIDER_HALF_DIM * 2.f, m_sprite.getTransform(), m_sprite.getOrigin());
    const auto width { std::abs(verts[1].x - verts[0].x) };

    auto left { verts[0].x };
    for (size_t i { 1 }; i < verts.size(); ++i) {
        left = std::min(left, verts[i].x);
    }

    if (left < bossState->getPlayAreaSize().left)
        m_sprite.setPosition({ bossState->getPlayAreaSize().left + (width / 2.f), m_sprite.getPosition().y });

    const auto maxRight { bossState->getPlayAreaSize().left + bossState->getPlayAreaSize().width - (width / 2.f) };
    if ((left + (width / 2.f)) > maxRight)
        m_sprite.setPosition({ maxRight, m_sprite.getPosition().y });
}

void PlayerCharacter::dashUpdate(const sf::Time& dt)
{
    const auto dashCooldownTime { getPlayerFloatVar("dash_cooldown") };
    static auto iframes { 0 };
    if (m_dashActive) {
        m_dashTimer -= dt;
        if (m_dashTimer <= sf::Time::Zero) {
            m_triggeredThisDash = false;
            m_dashActive = false;
            m_dashCooldown = sf::seconds(dashCooldownTime);
            m_dashBoost = 1.f;
            m_isInvincibleFrames = true;
            m_velocity = sf::Vector2f(m_dashDirection, 0.f) * getPlayerFloatVar("max_move_speed");
        }
    } else if (m_dashCooldown > sf::Time::Zero) {
        // If we've finished dodging we need to have a small window
        // of iframes before we triger the cooldown
        if (!m_isInvincibleFrames) {
            m_dashCooldown -= dt;
        } else {
            ++iframes;
            if (iframes > POST_DODGE_IFRAMES) {
                iframes = 0;
                m_isInvincibleFrames = false;
            }
        }
    }
}

void PlayerCharacter::handleCollisions(const nce::Manifold& manifold, nce::GameObject* const go)
{
    NCE_UNUSED(manifold);

    if (m_isDebugInvincibility)
        return;

    const auto identifier { go->getIdentifier() };
    const auto isBossProjectile { identifier.find("boss_projectile") != std::string::npos };
    if (isBossProjectile || identifier == "explosion" || identifier == "splittable_projectile") {
        // if we're dashing or we're still in the invincible window post-dodge
        // we shouldn't detect a projectile hit.
        // TODO floating status text saying player dodged the projectile)
        if (isDashing()) {
            auto bossState { dynamic_cast<BossStateBase*>(getAppState()) };
            assert(bossState);
            if (!m_triggeredThisDash) {
                bossState->getSfxManager().triggerSoundEvent("dodge_ping");
                m_triggeredThisDash = true;
            }

        } else if (!isInvincible() && !isDashing()) {
            m_wasHit = true;
        }
    }

    if (go->getIdentifier() == "arc_laser")
        m_wasHit = true;

    if (go->getIdentifier() == "demonic_hammer")
        m_wasHit = true;

    if (go->getIdentifier() == "underground_spike") {
        auto spikeInstance { dynamic_cast<UndergroundSpike*>(go) };

        assert(go);
        if (!go)
            throw std::runtime_error("We hit a spike that's not a spike, uhhhh");

        // If the spike is animating then it should be possible to kill us,
        // otherwise it should just push us back by the normal (This is bad
        // potentially if we're above the spike, since this will force us upwards)
        if (spikeInstance->isRaiseSequence()) {
            m_wasHit = true;
        } else {
            m_sprite.move(manifold.normal * manifold.penetration);
        }
    }

    if (go->getIdentifier() == "acid_floor")
        m_wasHit = true;
}

void PlayerCharacter::setAnimation(AnimationStage animStage, bool playOnChange)
{
    if (m_animations.find(animStage) == m_animations.end() || m_animationFrameTimes.find(animStage) == m_animationFrameTimes.end())
        throw std::runtime_error("Unable to find correct animation for PlayerCharacter");

    m_sprite.setAnimation(m_animations[animStage]);
    m_sprite.setFrameTime(m_animationFrameTimes[animStage]);
    m_animationStage = animStage;
    switch (animStage) {
    case AnimationStage::Idle:
        m_sprite.setLooped(true);
        break;
    case AnimationStage::Run:
        m_sprite.setLooped(true);
        break;
    case AnimationStage::Roll:
        m_sprite.setLooped(false);
        break;
    default:
        assert(false);
        break;
    }

    if (playOnChange) {
        m_sprite.stop();
        m_sprite.play();
    }
}

auto PlayerCharacter::getFreeProjectile() const -> std::optional<std::size_t>
{
    for (std::size_t i = 0; i < m_projectiles.size(); ++i) {
        auto p { m_projectiles[i] };
        assert(p);
        if (!p->isActive()) {
            return { i };
        }
    }
    return std::nullopt;
}
