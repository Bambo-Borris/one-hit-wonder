#include "CameraShake.hpp"

#include <nce/RNG.hpp>
#include <nce/TimerController.hpp>

CameraShake::CameraShake(sf::RenderTarget* target, nce::TimerController& timerController, sf::Time duration, float intensity)
    : m_renderTarget(target)
    , m_timerController(&timerController)
    , m_originalCentre(target->getView().getCenter())
    , m_duration(duration)
    , m_intensity(intensity)
{
    m_timerController->addTimer("camera_shake_effect_timer");
    spdlog::debug("CameraShake: View Centre [{}, {}]", m_originalCentre.x, m_originalCentre.y);
}

CameraShake::~CameraShake()
{
    m_timerController->removeTimer("camera_shake_effect_timer");
    auto view { m_renderTarget->getView() };
    view.setCenter(m_originalCentre);
    m_renderTarget->setView(view);
}

void CameraShake::update()
{
    if (!m_timerController->exists("camera_shake_effect_timer")) {
        m_timerController->addTimer("camera_shake_effect_timer");
        return;
    }

    auto view { m_renderTarget->getView() };
    if (!isCompleted()) {
        const auto shakeTimer { m_timerController->getElapsed("camera_shake_effect_timer") };
        const auto shakeAmount { m_intensity * (1.f - (shakeTimer / m_duration)) };

        view.setCenter(m_originalCentre
                       + sf::Vector2f(nce::RNG::realWithinRange(-shakeAmount, shakeAmount), nce::RNG::realWithinRange(-shakeAmount, shakeAmount)));
    } else {
        view.setCenter(m_originalCentre);
    }
    m_renderTarget->setView(view);
}

bool CameraShake::isCompleted() const { return m_timerController->getElapsed("camera_shake_effect_timer") >= m_duration; }
