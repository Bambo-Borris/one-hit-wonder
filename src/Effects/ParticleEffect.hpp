#pragma once

enum class EffectType : uint8_t { BloodSpurt };

struct ParticleProperties {
    sf::Color startColour { sf::Color::White };
    sf::Color endColour { sf::Color::White };
    sf::Vector2f acceleration {};
    sf::Time minLifeTime {};
    sf::Time maxLifeTime {};

    float alphaStart { 0.f };
    float alphaEnd { 0.f };
    float scaleStart { 1.f };
    float scaleEnd { 1.f };
    float startSpeed { 0.f };
    float endSpeed { 0.f };
    float maxSpeed { 0.f };

    sf::Angle minStartAngle { sf::degrees(0) };
    sf::Angle maxStartAngle { sf::degrees(0) };
};

struct EmitterProperties {
    sf::Time spawnFrequency { sf::seconds(0.01f) };
    sf::Time lifeTime {};
    sf::Vector2f position {};
    sf::Vector2f quadSize { 1.f, 1.f };
    sf::Texture* texture { nullptr };
};

class ParticleEffect : public sf::Drawable {
public:
    ParticleEffect(const EmitterProperties& emitter, const ParticleProperties& particles, sf::Transformable* anchorTransformable = nullptr);
    [[nodiscard]] auto isCompleted() const -> bool
    {
        // an emitter lifetime of 0 means an infinite time emitter
        if (m_emitterProperties.lifeTime == sf::Time::Zero)
            return false;
        return m_aliveTime >= m_emitterProperties.lifeTime;
    }

    void update(const sf::Time& dt);

protected:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const;

private:
    struct Particle {
        sf::Vector2f position;
        sf::Vector2f velocity;
        float alpha { 1.f };
        float scale { 1.f };
        sf::Color colour;
        sf::Time aliveTime;
        sf::Time maxAliveTime;
        sf::Angle angle;
    };

    void insertNewParticle();
    void generateQuads();

    EmitterProperties m_emitterProperties;
    ParticleProperties m_particleProperties;

    std::vector<Particle> m_particles;
    sf::VertexArray m_particleVertices;
    sf::Transformable* m_anchorTransformable;
    sf::Vector2f m_localPosition;
    sf::Time m_aliveTime;
    sf::Time m_emitClock;
};

[[nodiscard]] std::unique_ptr<ParticleEffect> MakeBloodSpurt(const sf::Vector2f& position, const sf::Vector2f& normal, sf::Transformable* t);

// This is a test effect, it exists simply for testing implementation details
[[nodiscard]] std::unique_ptr<ParticleEffect> MakeInfiniteSpewer(const sf::Vector2f& position);