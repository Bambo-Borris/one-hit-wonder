#include "ParticleEffect.hpp"
#include "../GameplayVars.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/RNG.hpp>

ParticleEffect::ParticleEffect(const EmitterProperties& emitter, const ParticleProperties& particles, sf::Transformable* anchorTransformable)
    : m_emitterProperties(emitter)
    , m_particleProperties(particles)
    , m_particleVertices(sf::PrimitiveType::Triangles)
    , m_anchorTransformable(anchorTransformable)
{
    if (anchorTransformable)
        m_localPosition = anchorTransformable->getInverseTransform() * emitter.position;
}

void ParticleEffect::update(const sf::Time& dt)
{
    // If we've completed but we've not been purged yet
    // we don't want to contribute to additional performance
    // problems
    if (isCompleted())
        return;

    // This logic needs to be revisted to be more accurate
    // since if we're 2x over the frequency, maybe we should
    // spawn 2 particles etc.
    if (m_emitClock > m_emitterProperties.spawnFrequency) {
        const auto numberToSpawn { uint32_t(m_emitClock / m_emitterProperties.spawnFrequency) };
        const auto remainder { m_emitClock % m_emitterProperties.spawnFrequency };

        for (auto i { 0u }; i < numberToSpawn; ++i) {
            insertNewParticle();
            m_particles.back().aliveTime = sf::milliseconds(m_emitterProperties.spawnFrequency.asMilliseconds() * i);
        }

        m_emitClock = remainder;
        // m_emitClock = sf::Time::Zero;
        spdlog::debug("Will spawn {} with remainder {}", numberToSpawn, remainder.asMicroseconds());
    }

    for (auto& p : m_particles) {
        const auto t = std::clamp((p.aliveTime / p.maxAliveTime), 0.f, 1.f);
        p.position += p.velocity * dt.asSeconds();
        // If we have an acceleration we'll apply it
        // otherwise we'll use the end speed and lerp
        if (m_particleProperties.acceleration != sf::Vector2f {}) {
            p.velocity += m_particleProperties.acceleration * dt.asSeconds();
        } else {
            const auto speed = std::lerp(m_particleProperties.startSpeed, m_particleProperties.endSpeed, t);
            p.velocity = p.velocity.normalized() * speed;
        }

        // Lerp Colour & Alpha
        p.colour = nce::LerpColour(m_particleProperties.startColour, m_particleProperties.endColour, t);
        p.alpha = std::lerp(m_particleProperties.alphaStart, m_particleProperties.alphaEnd, t);
        p.aliveTime += dt;
    }

    // Purge expired particles
    auto removeIt = std::remove_if(m_particles.begin(), m_particles.end(), [this](const Particle& p) { return p.aliveTime >= p.maxAliveTime; });
    m_particles.erase(removeIt, m_particles.end());

    generateQuads();

    m_emitClock += dt;
    m_aliveTime += dt;
}

void ParticleEffect::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    auto statesCopy { states };
    if (m_emitterProperties.texture)
        statesCopy.texture = m_emitterProperties.texture;
    target.draw(m_particleVertices, statesCopy);
}

void ParticleEffect::insertNewParticle()
{
    Particle p;
    p.maxAliveTime = sf::seconds(nce::RNG::realWithinRange(m_particleProperties.minLifeTime.asSeconds(), m_particleProperties.maxLifeTime.asSeconds()));
    p.velocity
        = { 1.f, sf::degrees(nce::RNG::realWithinRange(m_particleProperties.minStartAngle.asDegrees(), m_particleProperties.maxStartAngle.asDegrees())) };
    p.velocity *= m_particleProperties.startSpeed;
    if (!m_anchorTransformable)
        p.position = m_emitterProperties.position;
    else
        p.position = m_anchorTransformable->getTransform() * m_localPosition;
    p.colour = m_particleProperties.startColour;
    m_particles.push_back(p);
}

void ParticleEffect::generateQuads()
{
    m_particleVertices.clear();
    for (auto& p : m_particles) {
        sf::Transformable t;
        t.setOrigin(m_emitterProperties.quadSize / 2.f);
        t.setPosition(p.position);
        t.setRotation(p.angle);

        std::array<sf::Vertex, 4> vertexPositions;
        vertexPositions[0].position = t.getTransform() * sf::Vector2f { 0.f, 0.f };
        vertexPositions[1].position = t.getTransform() * sf::Vector2f { m_emitterProperties.quadSize.x, 0.f };
        vertexPositions[2].position = t.getTransform() * sf::Vector2f { m_emitterProperties.quadSize.x, m_emitterProperties.quadSize.y };
        vertexPositions[3].position = t.getTransform() * sf::Vector2f { 0.f, m_emitterProperties.quadSize.y };

        vertexPositions[0].color = p.colour;
        vertexPositions[1].color = p.colour;
        vertexPositions[2].color = p.colour;
        vertexPositions[3].color = p.colour;

        const auto alpha { uint8_t(255.f * p.alpha) };
        vertexPositions[0].color.a = alpha;
        vertexPositions[1].color.a = alpha;
        vertexPositions[2].color.a = alpha;
        vertexPositions[3].color.a = alpha;

        if (m_emitterProperties.texture) {
            const sf::Vector2f size { m_emitterProperties.texture->getSize() };
            vertexPositions[0].texCoords = sf::Vector2f { 0.f, 0.f };
            vertexPositions[1].texCoords = sf::Vector2f { size.x, 0.f };
            vertexPositions[2].texCoords = sf::Vector2f { size.x, size.y };
            vertexPositions[3].texCoords = sf::Vector2f { 0.f, size.y };
        }

        m_particleVertices.append(vertexPositions[0]);
        m_particleVertices.append(vertexPositions[1]);
        m_particleVertices.append(vertexPositions[2]);

        m_particleVertices.append(vertexPositions[2]);
        m_particleVertices.append(vertexPositions[3]);
        m_particleVertices.append(vertexPositions[0]);
    }
}

std::unique_ptr<ParticleEffect> MakeBloodSpurt(const sf::Vector2f& position, const sf::Vector2f& normal, sf::Transformable* t)
{
    EmitterProperties emitter;
    emitter.quadSize = { 2.f, 2.f };
    emitter.lifeTime = sf::milliseconds(800);
    emitter.spawnFrequency = sf::milliseconds(5);
    emitter.position = position;
    // emitter.texture = nce::AssetHolder::get().getTexture("bin/textures/particle.png");

    ParticleProperties particles;
    particles.startSpeed = 250.f;
    particles.endSpeed = 0.f;
    particles.minStartAngle = normal.angle() - sf::degrees(10.f);
    particles.maxStartAngle = normal.angle() + sf::degrees(10.f);
    particles.minLifeTime = sf::milliseconds(250);
    particles.maxLifeTime = sf::milliseconds(300);
    particles.acceleration = getWorldVectorVar("gravity");

    particles.startColour = sf::Color::Red;
    particles.endColour = sf::Color::Red;

    particles.alphaStart = 1.f;
    particles.alphaEnd = 0.1f;

    return std::make_unique<ParticleEffect>(emitter, particles, t);
}

std::unique_ptr<ParticleEffect> MakeInfiniteSpewer(const sf::Vector2f& position)
{
    EmitterProperties emitter;
    emitter.quadSize = { 8.f, 8.f };
    emitter.lifeTime = sf::milliseconds(5000);
    emitter.spawnFrequency = sf::milliseconds(8);
    emitter.position = position;
    emitter.texture = nce::AssetHolder::get().getTexture("bin/textures/particle.png");

    ParticleProperties particles;
    particles.startSpeed = 100.f;
    particles.endSpeed = 0.f;
    particles.minStartAngle = sf::degrees(0.f);
    particles.maxStartAngle = sf::degrees(359.f);
    particles.minLifeTime = sf::milliseconds(500);
    particles.maxLifeTime = sf::milliseconds(750);

    particles.startColour = { 255, 241, 145 };
    particles.endColour = { 255, 98, 44 };

    particles.alphaStart = 1.f;
    particles.alphaEnd = 0.1f;

    return std::make_unique<ParticleEffect>(emitter, particles);
}
