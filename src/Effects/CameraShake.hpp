#pragma once

#include <SFML/Graphics/View.hpp>

namespace nce {
class TimerController;
} // nce namespace

class CameraShake {
public:
    CameraShake(sf::RenderTarget* target, nce::TimerController& timerController, sf::Time duration, float intensity);
    ~CameraShake();

    void update();
    bool isCompleted() const;

private:
    sf::RenderTarget* m_renderTarget;
    nce::TimerController* m_timerController;
    sf::Vector2f m_originalCentre;
    sf::Time m_duration;
    float m_intensity { 0.f };
    sf::Time m_timer;
};
