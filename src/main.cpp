#include "GameplayConstants.hpp"
#include "HUD/GUIHelpers.hpp"
#include "States/DancingPrincesFightState.hpp"
#include "States/DemonicHammerFightState.hpp"
#include "States/EvilEyeFightState.hpp"
#include "States/MenuState.hpp"
#include "States/SplashScreenState.hpp"
#include "States/UnnamedFightState.hpp"
#include "Utils/GameMetrics.hpp"

#include <SFML/GpuPreference.hpp>
#include <nce/App.hpp>

#ifdef _MSC_VER
#include <Windows.h>
#include <minidumpapiset.h>
#include <tchar.h>
#endif

#include <imgui-SFML.h>
#include <imgui.h>

SFML_DEFINE_DISCRETE_GPU_PREFERENCE

// For generating crash dump stuff we need some horrific code
// so here goes nothing!
#ifdef _MSC_VER

typedef BOOL(WINAPI* MINIDUMPWRITEDUMP)(HANDLE hProcess,
                                        DWORD dwPid,
                                        HANDLE hFile,
                                        MINIDUMP_TYPE DumpType,
                                        CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
                                        PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
                                        CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

LONG WINAPI HandleException(struct _EXCEPTION_POINTERS* apExceptionInfo)
{
    HMODULE mhLib = ::LoadLibrary(_T("dbghelp.dll"));
    if (!mhLib)
        return EXCEPTION_EXECUTE_HANDLER;
    MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress(mhLib, "MiniDumpWriteDump");

    HANDLE hFile = ::CreateFile(_T("one-hit-wonder.dmp"), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    _MINIDUMP_EXCEPTION_INFORMATION ExInfo;
    ExInfo.ThreadId = ::GetCurrentThreadId();
    ExInfo.ExceptionPointers = apExceptionInfo;
    ExInfo.ClientPointers = FALSE;

    pDump(GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL);
    ::CloseHandle(hFile);

    return EXCEPTION_EXECUTE_HANDLER;
}
#endif

constexpr auto WINDOW_TITLE { "One Hit Wonder" };
constexpr auto TARGET_FPS { 60 };
constexpr auto APP_CONFIG_FILE_NAME { "settings.cfg" };

const nce::ConfigFile::ConfigFileSpec APP_CONFIG_FILE_SPEC { { "window_width", nce::ConfigFile::ConfigFieldType::Int },
                                                             { "window_height", nce::ConfigFile::ConfigFieldType::Int },
                                                             { "window_mode", nce::ConfigFile::ConfigFieldType::String },
                                                             /*{ "anti_aliasing", nce::ConfigFile::ConfigFieldType::Int },*/
                                                             { "master_volume", nce::ConfigFile::ConfigFieldType::Int } };

// nce::AppState* loadStateAsync(std::string stateName, sf::RenderWindow* window, sf::RenderTexture* texture, nce::App* app, std::any metaData)
nce::AppState* loadStateAsync(std::string stateName, nce::App* app, std::any metaData)
{
    nce::AppState* state { nullptr };
    try {
        sf::Vector2f playArea = std::any_cast<sf::Vector2f>(metaData);
        if (stateName == gc::EVIL_EYE_STATE_NAME)
            state = new EvilEyeFightState(app, playArea);
        else if (stateName == gc::DEMONIC_HAMMER_STATE_NAME)
            state = new DemonicHammerFightState(app, playArea);
        else if (stateName == gc::DANCING_PRINCES_STATE_NAME)
            state = new DancingPrinceFightState(app, playArea);
        else if (stateName == gc::UNNAMED_STATE_NAME)
            state = new UnnamedFightState(app, playArea);
        else if (stateName == "menu_state")
            state = new MenuState(app, playArea);
        else
            spdlog::critical("Unable to load state with name {}", stateName);
    } catch (const std::runtime_error& error) {
        spdlog::critical("Unable to load state: {}", error.what());
        throw;
    }
    return state;
}

int main(int argc, char* argv[])
{
    NCE_UNUSED(argc);
    NCE_UNUSED(argv);

#if _MSC_VER
    SetUnhandledExceptionFilter(HandleException);
#endif
    try {
        nce::App app({ 1024, 576 }, WINDOW_TITLE, TARGET_FPS, TARGET_FPS, { loadStateAsync }, "bin/textures/loading_screen.png");
        GameMetrics::LoadOrInitDefault();
        SetupImGuiFonts();
        ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
#ifndef ONE_HIT_WONDER_DEBUG
        app.setState(new SplashScreenState(&app, sf::Vector2f { 1024, 576 }));
#else
        app.setState(new MenuState(&app, sf::Vector2f { 1024, 576 }));
#endif
        app.run();

        GameMetrics::WriteToDisk();
    } catch (const std::runtime_error& e) {
        NCE_UNUSED(e);
        spdlog::shutdown();
        std::abort();
    }

    return 0;
}
