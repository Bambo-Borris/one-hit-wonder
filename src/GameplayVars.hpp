#pragma once

#include <SFML/System/Vector2.hpp>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>

struct FileDataNode {
    std::unordered_map<std::string, float> m_gameplayFloats;
    std::unordered_map<std::string, sf::Vector2f> m_gameplayVectors;
};

struct FileData {
    FileDataNode playerData;
    std::unordered_map<std::string, FileDataNode> bossesDataMap;
    FileDataNode worldData;
};

enum class NodeIdentifiers { Player, BossesShared, BossesEvilEye, BossesDemonicHammer, BossesDancingPrinces, BossesUnnamed, World };

class GameplayVars {
public:
    static auto get() -> GameplayVars&;

    [[nodiscard]] auto getFloat(NodeIdentifiers nodeId, std::string_view name) const -> float;
    [[nodiscard]] auto getVector(NodeIdentifiers nodeId, std::string_view name) const -> sf::Vector2f;

    void setFloat(NodeIdentifiers nodeId, std::string_view name, float value);
    void setVector(NodeIdentifiers nodeId, std::string_view name, const sf::Vector2f& value);

    void writeToFile();

    [[nodiscard]] auto getAllFloatKeys(NodeIdentifiers nodeId) const -> std::vector<std::string>;
    [[nodiscard]] auto getAllVectorKeys(NodeIdentifiers nodeId) const -> std::vector<std::string>;

private:
    [[nodiscard]] auto getFloatMapFromNodeIdentifier(NodeIdentifiers nodeId) const -> std::unordered_map<std::string, float>*;
    [[nodiscard]] auto getVector2fMapFromNodeIdentifier(NodeIdentifiers nodeId) const -> std::unordered_map<std::string, sf::Vector2f>*;

    GameplayVars();
    std::unique_ptr<FileData> m_fileData;
};

#define GameVars() GameplayVars::get()

// There is surely a better way with some template magic and rethinking
// to clean the below up. But for now it will suffic... (famous last words)
// Coming back to this, we could probably use some template specialization
// to achieve a cleaner result
inline float getPlayerFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::Player, key); }
// inline sf::Vector2f getPlayerVectorVar(std::string_view key); // Hasn't been necessarily so will stub
inline float getBossesSharedFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::BossesShared, key); }
// inline sf::Vector2f getBossesSharedVectorVar(std::string_view key); // Hasn't been necessarily so will stub
inline float getBossesEvilEyeFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::BossesEvilEye, key); }
// inline sf::Vector2f getBossesEvilEyeVectorVar(std::string_view key); // Hasn't been necessarily so will stub
inline float getBossesDemonicHammerFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::BossesDemonicHammer, key); }
// inline sf::Vector2f getBossesDemonicHammerVectorVar(std::string_view key); // Hasn't been necessarily so will stub
inline float getBossesDancingPrincesFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::BossesDancingPrinces, key); }
inline sf::Vector2f getBossesDancingPrincesVectorVar(std::string_view key) { return GameVars().getVector(NodeIdentifiers::BossesDancingPrinces, key); }
inline float getBossesUnnamedFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::BossesUnnamed, key); }
// inline sf::Vector2f getBossesDemonicHammerVectorVar(std::string_view key); // Hasn't been necessarily so will stub
inline float getWorldFloatVar(std::string_view key) { return GameVars().getFloat(NodeIdentifiers::World, key); }
inline sf::Vector2f getWorldVectorVar(std::string_view key) { return GameVars().getVector(NodeIdentifiers::World, key); }

inline void setPlayerFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::Player, key, value); }
// inline void setPlayerVectorVar(std::string_view key, const sf::Vector2f& value); // Hasn't been necessarily so will stub
inline void setBossesSharedFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::BossesShared, key, value); }
// inline void setBossesSharedVectorVar(std::string_view key, const sf::Vector2f& value); // Hasn't been necessarily so will stub
inline void setBossesEvilEyeFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::BossesEvilEye, key, value); }
// inline void setBossesEvilEyeVectorVar(std::string_view key, const sf::Vector2f& value); // Hasn't been necessarily so will stub
inline void setBossesDemonicHammerFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::BossesDemonicHammer, key, value); }
// inline void setBossesDemonicHammerVectorVar(std::string_view key, const sf::Vector2f& value); // Hasn't been necessarily so will stub
inline void setBossesDancingPrincesFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::BossesDancingPrinces, key, value); }
inline void setBossesDancingPrincesVectorVar(std::string_view key, const sf::Vector2f& value)
{
    GameVars().setVector(NodeIdentifiers::BossesDancingPrinces, key, value);
}
inline void setBossesUnnamedFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::BossesUnnamed, key, value); }
inline void setWorldFloatVar(std::string_view key, float value) { GameVars().setFloat(NodeIdentifiers::World, key, value); }
inline void setWorldVectorVar(std::string_view key, const sf::Vector2f& value) { GameVars().setVector(NodeIdentifiers::World, key, value); }
