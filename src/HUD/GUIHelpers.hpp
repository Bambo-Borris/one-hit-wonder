#pragma once

#include <imgui-SFML.h>
#include <imgui.h>

#include "MenuConstants.hpp"
#include <nce/ButtonActionChecker.hpp>

inline auto SetupImGuiFonts() -> void
{
    auto& io = ImGui::GetIO();
    io.Fonts->Clear();
    io.Fonts->AddFontFromFileTTF("bin/fonts/OldWizard.ttf", 32.f);
    if (!ImGui::SFML::UpdateFontTexture())
        spdlog::error("Unable to update font texture for imgui");

    io.Fonts->AddFontFromFileTTF("bin/fonts/TimesNewPixel.ttf", 24.f);
    if (!ImGui::SFML::UpdateFontTexture())
        spdlog::error("Unable to update font texture for imgui");

    io.Fonts->AddFontFromFileTTF("bin/fonts/OldWizard.ttf", 128.f);
    if (!ImGui::SFML::UpdateFontTexture())
        spdlog::error("Unable to update font texture for imgui");
}

inline auto PushFancyFont() { ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[0]); }

inline auto PushDetailFont() { ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]); }

inline auto PushFancyFontBig() { ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[2]); }

inline auto SetupImGuiPadBinds(nce::ButtonActionChecker& checker)
{
    // Gamepad binds
    checker.addGamePadAction("menu_up", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::LeftStickUp);
    checker.addGamePadAction("menu_up", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::DirectionUp);

    checker.addGamePadAction("menu_down", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::LeftStickDown);
    checker.addGamePadAction("menu_down", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::DirectionDown);

    checker.addGamePadAction("menu_left", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::LeftStickLeft);
    checker.addGamePadAction("menu_left", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::DirectionLeft);

    checker.addGamePadAction("menu_right", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::LeftStickRight);
    checker.addGamePadAction("menu_right", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::DirectionRight);

    checker.addGamePadAction("menu_proceed", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::A);
    checker.addGamePadAction("menu_back", nce::ButtonActionChecker::ActionType::Pressed, nce::GamePad::Buttons::B);

    // Keyboard binds
    checker.addKeyboardAction("menu_back", nce::ButtonActionChecker::ActionType::Pressed, sf::Keyboard::Escape);
}

inline auto CheckImGuiPadBinds(nce::ButtonActionChecker& checker)
{
    if (checker.checkAction("menu_up"))
        ImGui::GetIO().AddKeyEvent(ImGuiKey_UpArrow, true);
    else
        ImGui::GetIO().AddKeyEvent(ImGuiKey_UpArrow, false);

    if (checker.checkAction("menu_down"))
        ImGui::GetIO().AddKeyEvent(ImGuiKey_DownArrow, true);
    else
        ImGui::GetIO().AddKeyEvent(ImGuiKey_DownArrow, false);

    if (checker.checkAction("menu_left"))
        ImGui::GetIO().AddKeyEvent(ImGuiKey_LeftArrow, true);
    else
        ImGui::GetIO().AddKeyEvent(ImGuiKey_LeftArrow, false);

    if (checker.checkAction("menu_right"))
        ImGui::GetIO().AddKeyEvent(ImGuiKey_RightArrow, true);
    else
        ImGui::GetIO().AddKeyEvent(ImGuiKey_RightArrow, false);

    if (checker.checkAction("menu_proceed"))
        ImGui::GetIO().AddKeyEvent(ImGuiKey_Space, true);
    else
        ImGui::GetIO().AddKeyEvent(ImGuiKey_Space, false);
}

inline auto displayControlsMenu()
{
    enum class ControlsMenuMode { PC, Gamepad };
    static ControlsMenuMode menuMode { ControlsMenuMode::PC };

    if (ImGui::BeginTabBar("Sections")) {
        if (ImGui::TabItemButton("PC"))
            menuMode = ControlsMenuMode::PC;
        if (ImGui::TabItemButton("Gamepad"))
            menuMode = ControlsMenuMode::Gamepad;

        ImGui::EndTabBar();
    }

    if (ImGui::BeginTable("table1", 2, ImGuiTableFlags_RowBg)) {
        const ImU32 bgColour = ImColor(0.25f, 0.25f, 0.25f, 0.5f);

        if (menuMode == ControlsMenuMode::PC) {
            ImGui::TableSetupColumn("Actions", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("PC", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableHeadersRow();

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Move Left");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("A");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Move Right");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("D");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Aim");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Cursor Position");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Pullback Arrow");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Hold LMB");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Release Arrow");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Release LMB");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Dodge");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Space Bar");
        } else {
            ImGui::TableSetupColumn("Actions", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableSetupColumn("Gamepad", ImGuiTableColumnFlags_WidthStretch);
            ImGui::TableHeadersRow();

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Move Left");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Left Stick Left or D-Pad Left");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Move Right");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Left Stick Right or D-Pad Right");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Aim");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Right Stick Direction");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Pullback Arrow");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Hold Left or Right Trigger");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Release Arrow");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Release Left or Right Trigger");

            ImGui::TableNextRow();
            ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, bgColour);
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Dodge");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Left or Right Bumper");
        }
        ImGui::EndTable();
    }
}

inline auto CentreImGuiButton(bool smallButton = false)
{
    const auto avail = ImGui::GetContentRegionAvail().x;
    float off = avail * 0.5f - mc::MENU_BUTTON_SIZE.x * 0.5f;

    if (smallButton)
        off += mc::MENU_BUTTON_SIZE.x * 0.5f - mc::MENU_BUTTON_SIZE_SMALL.x * 0.5f;

    if (off > 0.0f)
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + off);
}

inline void TextCentered(std::string_view text)
{
    auto windowWidth = ImGui::GetWindowSize().x;
    auto textWidth = ImGui::CalcTextSize(text.data()).x;

    ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
    ImGui::Text(text.data());
}
