#pragma once

#include <nce/HUDElement.hpp>

class DarkenHUDAnimated : public nce::HUDElement {
public:
    DarkenHUDAnimated(nce::AppState* currentState, const sf::Time& fadeInTime, const sf::Color& colour, const sf::Vector2f& size);

    virtual void update(const sf::Time& dt, sf::RenderTarget& target) override;
    void playDarken();

private:
    // Inherited via HUDElement
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;
    sf::RectangleShape m_shape;
    sf::Color m_colour;
    sf::Time m_fadeInTime;
};
