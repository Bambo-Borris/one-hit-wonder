#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <nce/HUDElement.hpp>

class PlayerCharacter;
class BossStateBase;

class PadAimIndicator : public nce::HUDElement {
public:
    PadAimIndicator(BossStateBase* currentState, PlayerCharacter* player);

    // Inherited via GameObject
    virtual void update(const sf::Time& dt, sf::RenderTarget& target) override;

protected:
    // Inherited via GameObject
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

private:
    std::optional<sf::Sprite> m_sprite;
    PlayerCharacter* m_player;
    bool m_displayShape { false };
    BossStateBase* m_bossState;
};
