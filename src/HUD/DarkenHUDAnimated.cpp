#include "DarkenHUDAnimated.hpp"

#include <nce/AppState.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/TimerController.hpp>

constexpr auto TIMER_NAME { "darken_hud_fade_in" };

DarkenHUDAnimated::DarkenHUDAnimated(nce::AppState* currentState, const sf::Time& fadeInTime, const sf::Color& colour, const sf::Vector2f& size)
    : nce::HUDElement(currentState)
    , m_shape(size)
    , m_colour(colour)
    , m_fadeInTime(fadeInTime)
{
    getAppState()->getTimerController().addTimer(TIMER_NAME, true);
    m_shape.setFillColor(colour);
}

void DarkenHUDAnimated::update(const sf::Time& dt, sf::RenderTarget& target)
{
    NCE_UNUSED(dt);
    NCE_UNUSED(target);

    auto t = getAppState()->getTimerController().getElapsed(TIMER_NAME) / m_fadeInTime;
    t = std::clamp(t, 0.f, 1.f);
    const auto col { nce::LerpColour({ m_colour.r, m_colour.g, m_colour.b, 0 }, { m_colour.r, m_colour.g, m_colour.b, 150 }, t) };
    m_shape.setFillColor(col);
}

void DarkenHUDAnimated::playDarken()
{
    getAppState()->getTimerController().resetTimer(TIMER_NAME);
    getAppState()->getTimerController().playTimer(TIMER_NAME);
}

void DarkenHUDAnimated::draw(sf::RenderTarget& target, const sf::RenderStates& states) const { target.draw(m_shape, states); }
