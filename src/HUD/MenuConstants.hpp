#pragma once
#include <SFML/System/Vector2.hpp>

namespace mc {
constexpr auto MENU_PANEL_SIZE { sf::Vector2f { 1024, 576 } };
constexpr auto MENU_BUTTON_SIZE { sf::Vector2f { MENU_PANEL_SIZE.x * 0.5f, 48.f } };
constexpr auto MENU_BUTTON_SIZE_SMALL { sf::Vector2f { MENU_PANEL_SIZE.x * 0.25f, 48.f } };
} // namespace mc