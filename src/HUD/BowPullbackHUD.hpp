#pragma once

#include <nce/HUDElement.hpp>

class PlayerCharacter;

class BowPullbackHUD : public nce::HUDElement {
public:
    BowPullbackHUD(nce::AppState* currentState, PlayerCharacter* player);

    virtual void update(const sf::Time& dt, sf::RenderTarget& target) override;

private:
    // Inherited via HUDElement
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    PlayerCharacter* m_player;
    sf::RectangleShape m_backgroundShape;
    sf::RectangleShape m_indicatorShape;
};
