#include "BowPullbackHUD.hpp"
#include "../Objects/PlayerCharacter.hpp"

constexpr auto BACKGROUND_SIZE { sf::Vector2f { 64.f, 16.f } };
constexpr auto INDICATOR_SIZE { sf::Vector2f { 58.f, 10.f } };

BowPullbackHUD::BowPullbackHUD(nce::AppState* currentState, PlayerCharacter* player)
    : nce::HUDElement(currentState)
    , m_player(player)
    , m_backgroundShape(BACKGROUND_SIZE)
    , m_indicatorShape(INDICATOR_SIZE)
{
    m_backgroundShape.setOrigin(BACKGROUND_SIZE * 0.5f);
    m_indicatorShape.setOrigin(INDICATOR_SIZE * 0.5f);
    m_indicatorShape.setFillColor(sf::Color::Blue);
}

void BowPullbackHUD::update(const sf::Time& dt, sf::RenderTarget& target)
{
    NCE_UNUSED(dt);

    if (!m_player->isAiming())
        return;

    const auto bounds { m_player->getGlobalBounds() };
    const auto pos { sf::Vector2f { bounds.left + bounds.width / 2.f, bounds.top } };

    const auto pixelPos { sf::Vector2f { target.mapCoordsToPixel(pos) } };
    const auto targetSize { sf::Vector2f { target.getSize() } };
    const auto anchored { pixelPos.cwiseDiv(targetSize) - sf::Vector2f { 0.f, 0.05f } };
    auto finalPos { anchored.cwiseMul(targetSize) };
    finalPos.x = std::clamp(finalPos.x, m_backgroundShape.getSize().x / 2.f, float(target.getSize().x) - (m_backgroundShape.getSize().x / 2.f));
    finalPos.y = std::clamp(finalPos.y, m_backgroundShape.getSize().y / 2.f, float(target.getSize().y) - (m_backgroundShape.getSize().y / 2.f));
    m_backgroundShape.setPosition(finalPos);

    const auto xSizeScale { m_player->getAimChargePercentage() };
    const auto newWidth { std::clamp(INDICATOR_SIZE.x * xSizeScale, 0.f, INDICATOR_SIZE.x) };

    m_indicatorShape.setSize({ newWidth, INDICATOR_SIZE.y });
    m_indicatorShape.setPosition(finalPos);
}

void BowPullbackHUD::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (!m_player->isAiming())
        return;

    target.draw(m_backgroundShape, states);
    target.draw(m_indicatorShape, states);
}
