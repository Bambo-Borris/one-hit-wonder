#include "BossHealthBar.hpp"
#include "../Objects/BossBase.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/TextUtils.hpp>

constexpr auto BACKGROUND_SHAPE_SIZE { sf::Vector2f { 650.f, 73.125f } };
constexpr auto FILL_SHAPE_SIZE { sf::Vector2f { 64.f, 5.f } };

BossHealthBar::BossHealthBar(nce::AppState* currentState, BossBase* boss)
    : nce::HUDElement(currentState)
    , m_fillShape(FILL_SHAPE_SIZE)
    , m_boss(boss)
{
    assert(boss);
    if (!boss)
        throw std::runtime_error("Null boss instance passed to Boss Health Bar HUD");

    auto texture = nce::AssetHolder::get().getTexture("bin/textures/healthbar.png");
    if (!texture)
        throw std::runtime_error("Unable to load healthbar texture");

    m_healthbarSprite.emplace(*texture);
    m_healthbarSprite->setOrigin(m_healthbarSprite->getLocalBounds().getSize() / 2.f);
    m_healthbarSprite->setScale(BACKGROUND_SHAPE_SIZE.cwiseDiv(m_healthbarSprite->getLocalBounds().getSize()));

    m_fillShape.setOrigin(m_fillShape.getSize() * 0.5f);
    m_fillShape.setScale(m_healthbarSprite->getScale());
    m_fillShape.setFillColor({ 161, 14, 14 });

    auto font = nce::AssetHolder::get().getFont("bin/fonts/OldWizard.ttf");
    m_nameText.emplace(*font);
    m_nameText->setCharacterSize(48);
    m_nameText->setString(boss->getName().data());
    nce::CentreTextOrigin(*m_nameText);
    m_nameText->setFillColor({ 240, 221, 12 });
}

void BossHealthBar::update(const sf::Time& dt, sf::RenderTarget& target)
{
    NCE_UNUSED(dt);
    m_healthbarSprite->setPosition({ float(target.getSize().x) * 0.5f, (m_healthbarSprite->getGlobalBounds().height / 2.f) + 10.f });

    const auto healthScale { (float(m_boss->getHealth()) / float(m_boss->getMaxHealth())) };
    const auto newWidth { std::clamp(FILL_SHAPE_SIZE.x * healthScale, 1.f, FILL_SHAPE_SIZE.x) };

    m_fillShape.setSize({ newWidth, FILL_SHAPE_SIZE.y });
    m_fillShape.setPosition(m_healthbarSprite->getPosition());

    m_nameText->setPosition(m_fillShape.getPosition() + sf::Vector2f { 0.f, m_nameText->getGlobalBounds().getSize().y / 16.f });
}

void BossHealthBar::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (m_boss->getHealth() > 0)
        target.draw(m_fillShape, states);
    target.draw(*m_healthbarSprite, states);
    target.draw(*m_nameText, states);
}
