#pragma once

#include <nce/HUDElement.hpp>

class BossBase;

class BossHealthBar : public nce::HUDElement {
public:
    BossHealthBar(nce::AppState* currentState, BossBase* boss);
    // Inherited via HUDElement
    virtual void update(const sf::Time& dt, sf::RenderTarget& target) override;

private:
    virtual void draw(sf::RenderTarget& target, const sf::RenderStates& states) const override;

    std::optional<sf::Sprite> m_healthbarSprite;

    sf::RectangleShape m_fillShape;
    std::optional<sf::Text> m_nameText;
    BossBase* m_boss;
};
