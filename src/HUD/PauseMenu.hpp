#pragma once

namespace nce {
class ButtonActionChecker;
}

class PauseMenu {
public:
    PauseMenu(nce::ButtonActionChecker* bac);

    /// <summary>
    /// Update the pause menu
    /// </summary>
    void update(sf::RenderWindow* window, bool& isPaused, bool& exitToMainMenu);

private:
    enum class MenuMode { PauseMenu, Controls };

    MenuMode m_menuMode { MenuMode::PauseMenu };
    nce::ButtonActionChecker* m_bac;
    bool m_displayMenu { false };
    bool m_survivedSingleFrame { false };
};