#include "PadAimIndicator.hpp"
#include "../Objects/PlayerCharacter.hpp"
#include "../States/BossStateBase.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/InputHandler.hpp>
#include <nce/MathsUtils.hpp>

#include <cassert>

constexpr auto DISTANCE_FROM_PLAYER { 128.f };

PadAimIndicator::PadAimIndicator(BossStateBase* currentState, PlayerCharacter* player)
    : nce::HUDElement(currentState)
    , m_bossState(currentState)
    , m_player(player)
{
    auto texture { nce::AssetHolder::get().getTexture("bin/textures/gamepad_direction_indicator.png") };
    m_sprite.emplace(*texture);
    m_sprite->setOrigin(m_sprite->getLocalBounds().getSize() / 2.f);
    m_sprite->setPosition({ 100, 100 });
}

void PadAimIndicator::update(const sf::Time& dt, sf::RenderTarget& target)
{
    NCE_UNUSED(dt);

    auto& checker { m_bossState->getButtonActionChecker() };

    if (!nce::InputHandler::IsGamePadConnected())
        return;

    auto actionResult { checker.checkAction("shoot_hold") };
    if (!checker.checkAction("shoot_hold") || (*actionResult) != nce::ButtonActionChecker::ActionTrigger::GamePad) {
        m_displayShape = false;
        return;
    }

    auto& gamePad { nce::InputHandler::GetGamePad() };
    static sf::Vector2f rightStickPos { 1.f, 0.f };

    m_displayShape = true;
    sf::Vector2f axisValue { gamePad.getAxisPosition(nce::GamePad::Axis::RightStickX), -gamePad.getAxisPosition(nce::GamePad::Axis::RightStickY) };
    if (axisValue == sf::Vector2f {})
        axisValue = rightStickPos;

    const auto direction { axisValue.normalized() };
    const auto angle { std::atan2(axisValue.y, axisValue.x) };
    auto playerPos { m_player->getPosition() };
    playerPos = sf::Vector2f { target.mapCoordsToPixel(playerPos) };
    m_sprite->setPosition(playerPos + (direction * DISTANCE_FROM_PLAYER));
    m_sprite->setRotation(sf::radians(angle));
    rightStickPos = axisValue;
}

void PadAimIndicator::draw(sf::RenderTarget& target, const sf::RenderStates& states) const
{
    if (m_displayShape)
        target.draw(*m_sprite, states);
}
