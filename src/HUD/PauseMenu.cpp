#include "PauseMenu.hpp"
#include "../HUD/GUIHelpers.hpp"
#include "../HUD/MenuConstants.hpp"

#include <imgui-SFML.h>
#include <imgui.h>

PauseMenu::PauseMenu(nce::ButtonActionChecker* bac)
    : m_bac(bac)
{
    assert(m_bac);
}

void PauseMenu::update(sf::RenderWindow* window, bool& isPaused, bool& exitToMainMenu)
{
    CheckImGuiPadBinds(*m_bac);
    PushDetailFont();
    ImGui::Begin("Paused",
                 nullptr,
                 ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoBackground
                     | ImGuiWindowFlags_NoTitleBar);
    const auto position = (sf::Vector2f(window->getSize()) * 0.5f) - (mc::MENU_PANEL_SIZE * 0.5f);
    ImGui::SetWindowSize({ mc::MENU_PANEL_SIZE.x, 0.f });
    ImGui::SetWindowPos(position);

    switch (m_menuMode) {
    case MenuMode::PauseMenu:
        CentreImGuiButton();
        if (ImGui::Button("Resume", mc::MENU_BUTTON_SIZE))
            isPaused = false;

        CentreImGuiButton();
        if (ImGui::Button("Controls", mc::MENU_BUTTON_SIZE))
            m_menuMode = MenuMode::Controls;

        CentreImGuiButton();
        if (ImGui::Button("Exit To Menu", mc::MENU_BUTTON_SIZE))
            exitToMainMenu = true;

        CentreImGuiButton();
        if (ImGui::Button("Exit To Desktop", mc::MENU_BUTTON_SIZE))
            window->close();

        if (m_bac->checkAction("menu_back") && m_survivedSingleFrame)
            isPaused = false;

        break;

    case MenuMode::Controls:
        displayControlsMenu();

        ImGui::Separator();

        CentreImGuiButton(true);
        if (ImGui::Button("Back", mc::MENU_BUTTON_SIZE_SMALL))
            m_menuMode = MenuMode::PauseMenu;

        if (m_bac->checkAction("menu_back") && m_survivedSingleFrame)
            m_menuMode = MenuMode::PauseMenu;

        break;

    default:
        assert(false);
        break;
    }

    ImGui::End();
    ImGui::PopFont();

    if (!isPaused) {
        m_menuMode = MenuMode::PauseMenu;
        m_survivedSingleFrame = false;
    } else if (!m_survivedSingleFrame)
        m_survivedSingleFrame = true;
}
