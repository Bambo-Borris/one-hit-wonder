#include "GameplayVars.hpp"

#include <fstream>
#include <spdlog/spdlog.h>
#include <stdexcept>

auto GameplayVars::get() -> GameplayVars&
{
    static GameplayVars& instance = *new GameplayVars;
    return instance;
}

auto GameplayVars::getFloat(NodeIdentifiers nodeId, std::string_view name) const -> float
{
    const auto map { getFloatMapFromNodeIdentifier(nodeId) };

    const auto result = (*map).find(name.data());
    if (result == (*map).end())
        throw std::runtime_error("Float var not found");

    return result->second;
}

auto GameplayVars::getVector(NodeIdentifiers nodeId, std::string_view name) const -> sf::Vector2f
{
    const auto map { getVector2fMapFromNodeIdentifier(nodeId) };

    const auto result = (*map).find(name.data());
    if (result == (*map).end())
        throw std::runtime_error("Vector2f var not found");
    return result->second;
}

void GameplayVars::setFloat(NodeIdentifiers nodeId, std::string_view name, float value)
{
    auto map { getFloatMapFromNodeIdentifier(nodeId) };

    const auto result = (*map).find(name.data());
    if (result == (*map).end())
        throw std::runtime_error("Float var not found");

    (*map)[name.data()] = value;
}

void GameplayVars::setVector(NodeIdentifiers nodeId, std::string_view name, const sf::Vector2f& value)
{
    auto map { getVector2fMapFromNodeIdentifier(nodeId) };

    const auto result = (*map).find(name.data());
    if (result == (*map).end())
        throw std::runtime_error("Vector2f var not found");

    (*map)[name.data()] = value;
}

void GameplayVars::writeToFile()
{
    using json = nlohmann::json;
    std::ofstream outputFile;
    outputFile.open("bin/data/gamevars.json", std::ios::out);

    if (outputFile.fail()) {
        spdlog::error("Unable to save file bin/data/bosses.dat");
        return;
    }

    json rootObject(R"(
    {
        "player": {},
        "bosses":
        {
            "shared":{}, 
            "evil_eye":{},
            "demonic_hammer":{},
            "dancing_princes": {}
        },
        "world":{}
    }
    )"_json);

    // Write player
    for (const auto& [k, v] : m_fileData->playerData.m_gameplayFloats) {
        rootObject["player"][k] = v;
    }

    for (const auto& [k, v] : m_fileData->playerData.m_gameplayVectors) {
        rootObject["player"][k] = json::array({ v.x, v.y });
    }

    // Write bosses shared
    for (const auto& [k, v] : m_fileData->bossesDataMap["shared"].m_gameplayFloats) {
        rootObject["bosses"]["shared"][k] = v;
    }

    for (const auto& [k, v] : m_fileData->bossesDataMap["shared"].m_gameplayVectors) {
        rootObject["bosses"]["shared"][k] = json::array({ v.x, v.y });
    }

    // Write evil eye
    for (const auto& [k, v] : m_fileData->bossesDataMap["evil_eye"].m_gameplayFloats) {
        rootObject["bosses"]["evil_eye"][k] = v;
    }

    for (const auto& [k, v] : m_fileData->bossesDataMap["evil_eye"].m_gameplayVectors) {
        rootObject["bosses"]["evil_eye"][k] = json::array({ v.x, v.y });
    }

    // Write demonic hammer
    for (const auto& [k, v] : m_fileData->bossesDataMap["demonic_hammer"].m_gameplayFloats) {
        rootObject["bosses"]["demonic_hammer"][k] = v;
    }

    for (const auto& [k, v] : m_fileData->bossesDataMap["demonic_hammer"].m_gameplayVectors) {
        rootObject["bosses"]["demonic_hammer"][k] = json::array({ v.x, v.y });
    }

    // Write dancing princes
    for (const auto& [k, v] : m_fileData->bossesDataMap["dancing_princes"].m_gameplayFloats) {
        rootObject["bosses"]["dancing_princes"][k] = v;
    }

    for (const auto& [k, v] : m_fileData->bossesDataMap["dancing_princes"].m_gameplayVectors) {
        rootObject["bosses"]["dancing_princes"][k] = json::array({ v.x, v.y });
    }

    // Write world
    for (const auto& [k, v] : m_fileData->worldData.m_gameplayFloats) {
        rootObject["world"][k] = v;
    }
    for (const auto& [k, v] : m_fileData->worldData.m_gameplayVectors) {
        rootObject["world"][k] = json::array({ v.x, v.y });
    }

    outputFile << rootObject;
    outputFile.close();
}

auto GameplayVars::getAllFloatKeys(NodeIdentifiers nodeId) const -> std::vector<std::string>
{
    std::vector<std::string> keys;
    auto map { getFloatMapFromNodeIdentifier(nodeId) };

    for (const auto& pair : *map) {
        keys.push_back(pair.first);
    }
    return keys;
}

auto GameplayVars::getAllVectorKeys(NodeIdentifiers nodeId) const -> std::vector<std::string>
{
    std::vector<std::string> keys;
    auto map { getVector2fMapFromNodeIdentifier(nodeId) };

    for (const auto& pair : *map) {
        keys.push_back(pair.first);
    }
    return keys;
}

auto GameplayVars::getFloatMapFromNodeIdentifier(NodeIdentifiers nodeId) const -> std::unordered_map<std::string, float>*
{
    std::unordered_map<std::string, float>* map { nullptr };
    switch (nodeId) {
        using enum NodeIdentifiers;
    case Player:
        map = &m_fileData->playerData.m_gameplayFloats;
        break;
    case BossesShared:
        map = &m_fileData->bossesDataMap["shared"].m_gameplayFloats;
        break;
    case BossesEvilEye:
        map = &m_fileData->bossesDataMap["evil_eye"].m_gameplayFloats;
        break;
    case BossesDemonicHammer:
        map = &m_fileData->bossesDataMap["demonic_hammer"].m_gameplayFloats;
        break;
    case BossesDancingPrinces:
        map = &m_fileData->bossesDataMap["dancing_princes"].m_gameplayFloats;
        break;
    case BossesUnnamed:
        map = &m_fileData->bossesDataMap["unnamed"].m_gameplayFloats;
        break;
    case World:
        map = &m_fileData->worldData.m_gameplayFloats;
        break;
    default:
        assert(false);
    }

    return map;
}

auto GameplayVars::getVector2fMapFromNodeIdentifier(NodeIdentifiers nodeId) const -> std::unordered_map<std::string, sf::Vector2f>*
{
    std::unordered_map<std::string, sf::Vector2f>* map { nullptr };
    switch (nodeId) {
        using enum NodeIdentifiers;
    case Player:
        map = &m_fileData->playerData.m_gameplayVectors;
        break;
    case BossesShared:
        map = &m_fileData->bossesDataMap["shared"].m_gameplayVectors;
        break;
    case BossesEvilEye:
        map = &m_fileData->bossesDataMap["evil_eye"].m_gameplayVectors;
        break;
    case BossesDemonicHammer:
        map = &m_fileData->bossesDataMap["demonic_hammer"].m_gameplayVectors;
        break;
    case BossesDancingPrinces:
        map = &m_fileData->bossesDataMap["dancing_princes"].m_gameplayVectors;
        break;
    case BossesUnnamed:
        map = &m_fileData->bossesDataMap["unnamed"].m_gameplayVectors;
        break;
    case World:
        map = &m_fileData->worldData.m_gameplayVectors;
        break;
    default:
        assert(false);
        break;
    }
    return map;
}

GameplayVars::GameplayVars()
{
    using json = nlohmann::json;

    constexpr auto gameVarsPath { "bin/data/gamevars.json" };
    constexpr auto extractToFileDataNode = [](FileDataNode* node, std::string_view key, const json& object) -> void {
        if (object.is_number_float()) {
            node->m_gameplayFloats[key.data()] = object.get<float>();
        } else if (object.is_array()) {
            assert(object.size() == 2);
            node->m_gameplayVectors[key.data()].x = object[0].get<float>();
            node->m_gameplayVectors[key.data()].y = object[1].get<float>();
        }
    };

    std::ifstream inputFile;

    if (!std::filesystem::exists(gameVarsPath))
        throw std::runtime_error("Game vars file does not exist!");

    inputFile.open(gameVarsPath, std::ios::in);
    if (inputFile.fail())
        throw std::runtime_error("Unable to load bosses data file");

    json rootNode;
    inputFile >> rootNode;
    inputFile.close();

    spdlog::info("Loading {} data", gameVarsPath);

    m_fileData = std::make_unique<FileData>();

    // Validate the root node types we need exist
    if (rootNode.find("player") == rootNode.end())
        throw std::runtime_error("Missing player node");

    if (rootNode.find("bosses") == rootNode.end())
        throw std::runtime_error("Missing bosses node");

    if (rootNode.find("world") == rootNode.end())
        throw std::runtime_error("Missing world node");

    for (const auto& child : rootNode.items()) {
        if (!child.value().is_object())
            throw std::runtime_error("Invalid type found in json file");

        if (child.value().empty())
            throw std::runtime_error("Invalid object found in json file");

        const auto isPlayer { child.key() == "player" };
        const auto isBoss { child.key() == "bosses" };
        const auto isWorld { child.key() == "world" };

        if (isPlayer || isWorld) {
            for (const auto& [k, v] : child.value().items()) {
                auto ptr = isPlayer ? &m_fileData->playerData : &m_fileData->worldData;
                extractToFileDataNode(ptr, k, v);
            }
        } else if (isBoss) {
            for (const auto& [bossKey, bossValue] : child.value().items()) {
                if (bossValue.empty())
                    throw std::runtime_error("Invalid boss object found in json file");

                auto& bossFileNode = m_fileData->bossesDataMap[bossKey];

                for (const auto& [k, v] : bossValue.items()) {
                    extractToFileDataNode(&bossFileNode, k, v);
                }
            }
        }
    }

    spdlog::info("Successfully loaded {}", gameVarsPath);
}
