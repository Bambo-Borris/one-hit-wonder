#pragma once

// Game Constants
namespace gc {
/* State identifiers (used for transitioning between states) */

constexpr auto ENGINE_SPLASH_STATE_NAME { "engine_splash" };
constexpr auto MENU_STATE_NAME { "menu_state" };
constexpr auto EVIL_EYE_STATE_NAME { "evil_eye_fight_state" };
constexpr auto DEMONIC_HAMMER_STATE_NAME { "demonic_hammer_fight_state" };
constexpr auto DANCING_PRINCES_STATE_NAME { "dancing_princes_fight_state" };
constexpr auto UNNAMED_STATE_NAME { "unnamed_fight_state" };

/* Boss figures & name strings */

constexpr auto BOSS_COUNT { 4 };
constexpr std::array<const char*, BOSS_COUNT> BOSS_NAMES { "Evil Eye", "Phanax Stelite", "Gulon Keriaxis", "Unnamed" };

/* Boss indicies (used for accessing boss name strings) */

constexpr size_t EVIL_EYE_INDEX { 0 };
constexpr size_t DEMONIC_HAMMER_INDEX { 1 };
constexpr size_t DANCING_PRINCES_INDEX { 2 };
constexpr size_t UNNAMED_INDEX { 3 };

constexpr size_t BOSS_PROJECTILES_COUNT_DANCING_PRINCES { 100 };

constexpr auto ZOOM_OUT_SCALE { 1.5f };

} // namespace gc
