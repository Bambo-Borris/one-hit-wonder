#pragma once

#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>

#include <nce/App.hpp>
#include <nce/InputHandler.hpp>
#include <numeric>

/// <summary>
/// When initialising a CollisionSolver from a play area we need
/// a self contained structure of information to store the calculated
/// grid size, cell size & position
/// </summary>
struct SolverInitData {
    sf::Vector2u cellSize;
    sf::Vector2u gridSize;
    sf::Vector2f position;
};

[[nodiscard]] inline auto GenerateSolverFromViewBounds(const sf::FloatRect& bounds) -> SolverInitData
{
    const auto gcd { std::gcd(uint32_t(bounds.width), uint32_t(bounds.height)) };
    SolverInitData initData;
    initData.cellSize = sf::Vector2u { gcd, gcd };
    initData.gridSize = sf::Vector2u { uint32_t(bounds.width) / gcd, uint32_t(bounds.height) / gcd };
    initData.position = { bounds.left, bounds.top };
    return initData;
}

// Not in world, but RT screen space
[[nodiscard]] inline sf::Vector2f GetMouseRTPos(nce::App* app)
{
    assert(app);
    if (!app)
        throw std::runtime_error("Invalid App passed to GetMouseRTPos");

    sf::Vector2f topLeft;
    topLeft = (sf::Vector2f { app->getRenderWindow()->getSize() } / 2.f) - (sf::Vector2f { app->getRenderTexture()->getSize() } / 2.f);

    auto screenPos = sf::Vector2f { nce::InputHandler::GetMouseScreenPosition() };
    screenPos.x = std::clamp(screenPos.x, topLeft.x, topLeft.x + float(app->getRenderTexture()->getSize().x));
    screenPos.y = std::clamp(screenPos.y, topLeft.y, topLeft.y + float(app->getRenderTexture()->getSize().y));

    screenPos -= topLeft;
    return screenPos;
}

// World space transformed from the RT world pos
[[nodiscard]] inline sf::Vector2f GetMouseRTWorldPos(nce::App* app)
{
    assert(app);

    if (!app)
        throw std::runtime_error("Invalid App passed to GetMouseRTWorldPos");

    const auto pos = GetMouseRTPos(app);
    return app->getRenderTexture()->mapPixelToCoords(sf::Vector2i { pos });
}
