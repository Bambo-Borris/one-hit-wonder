#pragma once

#include <SFML/System/Vector2.hpp>
#include <cstdint>
#include <nce/TimerController.hpp>

// Collection of utility functions to allow us to reuse useful
// features across bosses.

/// <summary>
/// Will return a sf::Vector2f representing the end position a projectile should be fired in
/// with the resultant position being based on predictive logic to account for player behaviours.
/// </summary>
[[nodiscard]] sf::Vector2f getLinearShotPredictedEndPoint(nce::TimerController& tc,
                                                          float projectileSpeed,
                                                          const sf::Vector2f& spawnLocation,
                                                          const sf::Vector2f& targetLocation,
                                                          float targetXDirection,
                                                          float targetSpeed,
                                                          std::uint32_t directionChangeAmount,
                                                          std::pair<float, float> playAreaMinMax);
