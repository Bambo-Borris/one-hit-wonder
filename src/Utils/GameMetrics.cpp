#include "GameMetrics.hpp"

#include <fstream>
#include <nlohmann/json.hpp>
#include <spdlog/fmt/fmt.h>

bool GameMetrics::m_foundExistingSaveFile { false };
TrackedMetrics GameMetrics::m_trackedMetrics {};

constexpr auto SAVE_FILE_NAME { "save.dat" };

void GameMetrics::LoadOrInitDefault()
{
    if (!std::filesystem::exists(SAVE_FILE_NAME)) {
        ConfigureDefault();
        return;
    }

    std::ifstream inputFile;
    inputFile.open(SAVE_FILE_NAME, std::ios::in);
    if (inputFile.fail()) {
        spdlog::error("Unable to load game metrics to disk");
        return;
    }
    inputFile >> m_trackedMetrics;
    inputFile.close();
    m_foundExistingSaveFile = true;
}

void GameMetrics::WriteToDisk()
{
    std::ofstream outputFile;
    outputFile.open(SAVE_FILE_NAME, std::ios::out);
    if (outputFile.fail()) {
        spdlog::error("Unable to save game metrics to disk");
        return;
    }

    outputFile << m_trackedMetrics;
    outputFile.close();

    m_foundExistingSaveFile = true;
    return;
}

void GameMetrics::SetBossKillTime(size_t bossIndex, const sf::Time& time)
{
    assert(bossIndex >= 0 && bossIndex < gc::BOSS_COUNT);
    m_trackedMetrics.bossFightsData[bossIndex].killTime = time;
}

void GameMetrics::SetBossAttemptsCount(size_t bossIndex, unsigned int attemptsCount)
{
    assert(bossIndex >= 0 && bossIndex < gc::BOSS_COUNT);
    m_trackedMetrics.bossFightsData[bossIndex].attemptsCount = attemptsCount;
}

void GameMetrics::SetBossCompleted(size_t bossIndex)
{
    assert(bossIndex >= 0 && bossIndex < gc::BOSS_COUNT);
    m_trackedMetrics.bossFightsData[bossIndex].didKill = true;
}

void GameMetrics::SetPlayerAccuracyOnBoss(size_t bossIndex, float accuracy)
{
    assert(bossIndex >= 0 && bossIndex < gc::BOSS_COUNT);
    m_trackedMetrics.bossFightsData[bossIndex].accuracy = accuracy;
}

void GameMetrics::AddPlayerTotalShotsFired() { ++m_trackedMetrics.playerMetrics.totalShotsFired; }

void GameMetrics::AddPlayerTotalShotsOnTarget() { ++m_trackedMetrics.playerMetrics.totalShotsOnTarget; }

void GameMetrics::AddPlayerDeath() { ++m_trackedMetrics.playerMetrics.totalDeaths; }

void GameMetrics::ConfigureDefault()
{
    for (size_t i { 0 }; i < gc::BOSS_COUNT; ++i) {
        m_trackedMetrics.bossFightsData[i].bossName = gc::BOSS_NAMES[i];
    }
}