#pragma once

#include "../GameplayConstants.hpp"

struct BossFightMetric {
    std::string bossName;
    sf::Time killTime;
    unsigned int attemptsCount { 0 };
    bool didKill { false };
    float accuracy { 0.f };
};

struct PlayerMetrics {
    unsigned int totalShotsFired { 0 };
    unsigned int totalShotsOnTarget { 0 };
    unsigned int totalDeaths { 0 };
};

struct TrackedMetrics {
    std::array<BossFightMetric, gc::BOSS_COUNT> bossFightsData;
    PlayerMetrics playerMetrics;
};

class GameMetrics {
public:
    static void LoadOrInitDefault();
    static void WriteToDisk();
    [[nodiscard]] static auto HasFoundExistingSave() -> bool { return m_foundExistingSaveFile; }

    /* For setting boss metrics */

    static void SetBossKillTime(size_t bossIndex, const sf::Time& time);
    static void SetBossAttemptsCount(size_t bossIndex, unsigned int attemptsCount);
    static void SetBossCompleted(size_t bossIndex);
    static void SetPlayerAccuracyOnBoss(size_t bossIndex, float accuracy);

    /* For setting player metrics */

    static void AddPlayerTotalShotsFired();
    static void AddPlayerTotalShotsOnTarget();
    static void AddPlayerDeath();

    [[nodiscard]] static auto GetPlayerMetrics() -> const PlayerMetrics& { return m_trackedMetrics.playerMetrics; }
    [[nodiscard]] static auto GetBossFightMetrics(size_t index) -> const BossFightMetric&
    {
        assert(index >= 0 && index <= m_trackedMetrics.bossFightsData.size());
        return m_trackedMetrics.bossFightsData[index];
    }

private:
    static void ConfigureDefault();

    static bool m_foundExistingSaveFile;
    static TrackedMetrics m_trackedMetrics;
};

/* Operator overloads for writing BossFightMetric to disk */

inline std::ostream& operator<<(std::ostream& lhs, const BossFightMetric& rhs)
{
    // clang-format off
    lhs << rhs.bossName << "\n" 
        << rhs.killTime.asMilliseconds() << "\n" 
        << rhs.attemptsCount << "\n" 
        << rhs.didKill << "\n" 
        << rhs.accuracy << "\n";
    // clang-format on
    return lhs;
}

inline std::istream& operator>>(std::istream& lhs, BossFightMetric& rhs)
{
    static std::array<char, 100> buffer;

    int32_t killTime { 0 };
    memset(buffer.data(), '\0', buffer.size());
    lhs.getline(buffer.data(), buffer.size(), '\n');
    rhs.bossName = buffer.data();
    lhs >> killTime;
    lhs >> rhs.attemptsCount;
    lhs >> rhs.didKill;
    lhs >> rhs.accuracy;

    rhs.killTime = sf::milliseconds(killTime);

    // We append a new line between the bosses, we'll read this in before progressing
    lhs.getline(buffer.data(), buffer.size(), '\n');
    return lhs;
}

/* Operator overloads for writing PlayerMetrics to disk */

inline std::ostream& operator<<(std::ostream& lhs, const PlayerMetrics& rhs)
{
    // clang-format off
    lhs << rhs.totalShotsFired << "\n" 
        << rhs.totalShotsOnTarget << "\n"
        << rhs.totalDeaths << "\n";
    // clang-format on
    return lhs;
}

inline std::istream& operator>>(std::istream& lhs, PlayerMetrics& rhs)
{
    lhs >> rhs.totalShotsFired >> rhs.totalShotsOnTarget >> rhs.totalDeaths;
    return lhs;
}

/* Operator overloads for writing TrackedMetrics to disk */

inline std::ostream& operator<<(std::ostream& lhs, const TrackedMetrics& rhs)
{
    for (const auto& boss : rhs.bossFightsData) {
        lhs << boss;
    }

    lhs << "\n";
    lhs << rhs.playerMetrics;
    return lhs;
}

inline std::istream& operator>>(std::istream& lhs, TrackedMetrics& rhs)
{
    for (auto& boss : rhs.bossFightsData) {
        lhs >> boss;
    }

    lhs >> rhs.playerMetrics;
    return lhs;
}
