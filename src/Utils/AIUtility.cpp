#include "AIUtility.hpp"

#include <algorithm>
#include <nce/RNG.hpp>

sf::Vector2f getLinearShotPredictedEndPoint(nce::TimerController& tc,
                                            float projectileSpeed,
                                            const sf::Vector2f& spawnLocation,
                                            const sf::Vector2f& targetLocation,
                                            float targetXDirection,
                                            float targetSpeed,
                                            std::uint32_t directionChangeAmount,
                                            std::pair<float, float> playAreaMinMax)
{
    // How we predict the location of the player in the future
    // for projectile firing:
    // - We figure out the line to the player
    // - Then we calculate the time that projectile would be alive for
    // - Then with this time we figure out how far the player would
    //	 move if they were just moving normally
    // - Then we fire the projectile towards this new position

    // TODO: Maybe we should incorporate the ability to specify a custom
    //  degree of inaccuracy with this function

    // Uh... if we don't have this timer and try use this function
    // we've really screwed up. The direction change is fairly crucial
    assert(tc.exists("directionChangeTimer"));

    sf::Vector2f endPoint;

    auto directionChangedElapsed { tc.getElapsed("directionChangeTimer").asSeconds() };
    // Prevent divide by 0
    if (directionChangedElapsed == 0.f)
        directionChangedElapsed = 1.f;

    const auto directionChangeFrequency { static_cast<float>(directionChangeAmount) / directionChangedElapsed };

    // If the player isn't side dodging too much we
    // will fire with a prediction of where they'll
    // be otherwise we'll fire directly at them
    if (directionChangeFrequency < 2.5f) {
        const auto distanceToTarget { (targetLocation - spawnLocation).length() };
        const auto predictedAliveTime { distanceToTarget / projectileSpeed };
        auto newLocation = targetLocation + (sf::Vector2f { targetXDirection, 0.f } * predictedAliveTime * targetSpeed);

        newLocation.x = std::clamp(newLocation.x, playAreaMinMax.first, playAreaMinMax.second);
        auto direction = (newLocation - spawnLocation).normalized();

        const auto inaccuracyAngle = nce::RNG::realWithinRange(0.1f, 2.f);
        direction = direction.rotatedBy(sf::degrees(inaccuracyAngle));

        endPoint = spawnLocation + (direction * distanceToTarget);
    } else {
        endPoint = targetLocation;
    }

    return endPoint;
}
